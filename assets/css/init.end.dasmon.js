 	$( "#protocol" ).change(function() {
		if ($('#protocol').val()=="MQTT"){
			$('#topic').show();
		}
		else{
		$('#topic').hide();
		}
	});
	
	
  
  function logoutx() {
        if (confirm("Apakah anda akan logout?")) {
            window.location = "logout.php";
        }
    }
	
	$('#largeModal').on('shown.bs.modal', function () {
		
		$('#largeModal').trigger('show')
		
			$('#wg_1').css({ opacity: 1 });
			$('#wg_2').css({ opacity: 1 });
			$('#wg_3').css({ opacity: 1 });
			$('#wg_4').css({ opacity: 1 });
			$('#wg_5').css({ opacity: 1 });
			$('#wg_6').css({ opacity: 1 });
			$.ajax({method: "GET",url:'pr_wg.php'})
			var dtwidget=''
				 $.get( "pr_wg.php" )
						.done(function( data ) {
							if (data== null){
							}
							else{
							$.each(JSON.parse(data), function(idx, obj) {
							$('.widget-checked-wg'+obj.kd_widget).removeClass('fa fa-check-square-o text-success')
							$('#wg_'+obj.kd_widget).css({ opacity: 0.5 });
								dtwidget+=obj.kd_widget;
							})
							ls_save('wg_pr',dtwidget);
							}
						});		
					
		})
	function selectwg(){
			$('.modal').modal('hide')
			if(ls_read('addwidget')=="1"||ls_read('addwidget')=="2"||ls_read('addwidget')=="3")
			{
				$('#editwidget').modal('toggle');
				$('#editwidget').modal('show');
			ls_save('uact','ins');
			}else{
			ins_widget();
			}
							 }
	function widget_add(x){
		$('.widget-checked').removeClass('fa fa-check-square-o text-success');
	}
	function modify_widget(x){
        $('.widget_title').html(x);
        $('#pan	el_name').html();
        $('#topic').html();
        $('#payload_min').html();
        $('#payload_max').html();
        $('#unit').html();
        $('#qos').html();
        $('#description').html();
		ls_save('uact','edit');				
		load_widget(x);	
		ls_save('objid',x);		
    }
    function ins_widget(){
		var design_id = Date.now();
        var title=$('.widget_title').html();
        var panel_name=$('#panel_name').val();
        var topic=$('#topic').val();
        var pmin=$('#payload_min').val();
        var pmax=$('#payload_max').val();
        var unit=$('#unit').val();
        var qos=$('#qos').val();
        var desc=$('#description').val();
        var kd_widget = ls_read('addwidget');
		var protocol='MQTT'
		var url ='URL'
		var phase = 'R' 
		var getdata= 'getdata'
		var code='phase'
		if(kd_widget=="4"){
			var panel_name='Map'
				topic='das/widget/prefix/map'
				pmin=0;
				pmax=1000
				unit=0
				qos=1
				desc='Coverage Area'					
		}
		if(kd_widget=="5"){
			var panel_name='Summary Chart'
				topic='das/widget/prefix/summary'
				pmin=0;
				pmax=1000
				unit=0
				qos=1
				desc='Summary Chart'					
		}
		if(kd_widget=="6"){
			var panel_name='Summary Report'
				topic='das/widget/prefix/report'
				pmin=0;
				pmax=1000
				unit=0
				qos=1
				desc='Coverage Area'					
		}
       $.post( "process/ins_widget.php", { design_id: design_id ,title: title, panel_name: panel_name , topic: topic, payload_max: pmax, payload_min:pmin, unit: unit, qos: qos, description: desc,kd_widget:kd_widget,protocol:protocol})
        //$.post( "process/ins_widget.php", { design_id: design_id ,title: title, panel_name: panel_name , topic: topic, payload_max: pmax, payload_min:pmin, unit: unit, qos: qos, description: desc,kd_widget:kd_widget,active:'y',protocol:'MQTT',url:'das.pt-mgi.co.id',phase:'R',getdata:'phase',code:'ok'})
        .done(function( data ) {
			$('#loading').show()
			var plotgauge =[];
			var reportchart=[];
			var binddata = [];
			var plotpie=[];
			$('#widgetdata').empty();
            $('#widgetmap').empty();
            readsqlite();
			 toastr.options.timeOut = "false";
             toastr.options.closeButton = true;
             toastr.options.positionClass = 'toast-top-center';
             toastr['info']('Data successfully inserted!');
			 $('#editwidget').modal('hide');
        })
		.fail(function( ms )
         {
			 $('#editwidget').modal('hide');
	
				plotgauge.push();
				var icon ='icon icon-energy text-danger';
				addwggauge(design_id,topic,panel_name,icon,unit);
				rendergauge();
			alert('tambah coy')
			alert('♥♥♥ Cek file "process/ins_widget.php" ♥♥♥');
         });
	}
	function upd_widget(){
        var title=$('.widget_title').html();
        var panel_name=$('#panel_name').val();
        var topic=$('#topic').val();
        var pmin=$('#payload_min').val();
        var pmax=$('#payload_max').val();
        var unit=$('#unit').val();
        var qos=$('#qos').val();
        var desc=$('#description').val();
		var design_id=$('#design_id').val();  
        $.post( "process/upd_widget.php", { title: title, panel_name: panel_name , topic: topic, payload_max: pmax, payload_min:pmin, unit: unit, qos: qos, description: desc, design_id: design_id})
        .done(function( data ) { 
        	var plotgauge =[];
			var reportchart=[];
			var binddata = [];
			var plotpie=[];
            $('#widgetmap').empty();
            $('#widgetdata').empty();
			try{			
			readsqlite();
			}
			catch(err){
				alert(err.message)
			}
            toastr.options.timeOut = "false";
             toastr.options.closeButton = true;
             toastr.options.positionClass = 'toast-top-center';
             toastr['info']('Data successfully updated!');
			 $('#editwidget').modal('hide');
        
		})
        .fail(function( ms )
         {
          alert('♥♥♥ Cek file "process/upd_widget.php" ♥♥♥');
         });
    }
	
    function load_widget(x){
        $.ajax({method: "GET",url:'process/load_widget.php?design_id='+x})
         .done(function( ms )
         {
            var dtx=ms.split('#');
            $('.widget_title').html(dtx[0]);
            $('#panel_name').val(dtx[1]);
            $('#topic').val(dtx[2]);
            $('#payload_min').val(dtx[3]);
            $('#payload_max').val(dtx[4]);
            $('#unit').val(dtx[5]);
            $('#qos').val(dtx[6]);
            $('#description').val(dtx[7]);
            $('#design_id').val(dtx[8]);
         })
         .fail(function( ms )
         {
          alert('♥♥♥ Cek file "process/load_widget.php" ♥♥♥');
         });
    }
	
	function save_widget(){
		var uact=ls_read('uact');
		if(uact=='edit'){
			upd_widget()
		}else{
			ins_widget()
		}
		$('.panel_name_'+ls_read('objid')).html($('#panel_name').val());
	}
	
	$(document).ready(function () {
        csys2();
        csys();
    });
