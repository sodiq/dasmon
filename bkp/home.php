﻿<?php
error_reporting(0);
session_start();
require_once 'module/login/class.user.php';
$user_home = new USER();

if (!$user_home->is_logged_in()) {
    $user_home->redirect('login.php');
}

$stmt = $user_home->runQuery("SELECT * FROM tbl_im_users WHERE userID=:uid");
$stmt->execute(array(":uid" => $_SESSION['userSession']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);
require 'class/class.select.php';
$select = new select;
$userID = $_SESSION['userSession'];
?>

<html lang="en"> 

<head> 
<title>Device Monitoring Systems</title>
<link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon"/>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Lucid Bootstrap 4.1.1 Admin Template">
<meta name="author" content="WrapTheme, design by: ThemeMakker.com">
<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/vendor/jvectormap/jquery-jvectormap-2.0.3.min.css"/>
<link rel="stylesheet" href="assets/vendor/morrisjs/morris.min.css" />
<link rel="stylesheet" href="assets/vendor/sweetalert/sweetalert.css"/>
<!-- MAIN CSS -->
<link rel="stylesheet" href="theme/assets/css/main.css">
<link rel="stylesheet" href="theme/assets/css/color_skins.css">
<link rel="stylesheet" href="assets/css/widget.css"> 
<script src="assets/js/jquery-3.4.1.js"></script>
<script src="module/mqtt/mqttws31.min.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/js/loader.js"></script>
<script type="text/javascript" src="assets/js/jquery.sparkline.js"></script>
<script src="assets/vendor/raphael/raphael-min.js"></script>
<script src="assets/vendor/justgage-toorshia/justgage.js"></script>

<script src ="module/mqtt/widget.js" type = "text/javascript"></script>
<script src ="module/mqtt/addwidget.js" type = "text/javascript"></script>

<style>
      html, body {
      max-width: 100%;
      overflow-x: hidden;
      }
      #sortable { list-style-type: none; margin: 0; padding: 0; width: 20px; }
</style>
<script>
	ls_save('kode-trafo','008'); //simpan secara global kode trafo
</script>
</head>
<body class="theme-dark">
<div hidden id ="setting_gui"></div>
<div id="wrapper">

    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
            </div>
            <div class="navbar-brand">              
            </div>
            <div class="navbar-right">
                <div id="navbar-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                <i class="icon-bell"></i>
                                <span class="notification-dot"></span>                              
                                <span class="nodot"></span>
                            </a>
                            <ul class="dropdown-menu notifications">
                                <li class="header"><strong>You have <span id="status"></span> new Notifications</strong></li>
                                <div id="notif-body"></div>                            
                                <li class="footer"><a href="#" class="more">See all notifications</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" onClick="logoutx()" class="icon-menu"><i class="icon-login"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div id="left-sidebar" class="sidebar">
        <div class="sidebar-scroll">
            <div class="user-account">
                 <img src="<?php echo 'assets/images/';
                if ($select->user($userID, 'photo') == '') {
                    echo 'f_avatar.png';
                } else {
                    echo $select->user($userID, 'photo');
                }; ?>" class="rounded-circle user-photo">

                <div class="dropdown">
                    <span>Welcome,</span>
                     <a href="" class="user-name"><strong> <?php echo $select->user($userID, 'role_role_id') . ' <br> ' .
                $select->user($userID, 'nama_depan'). ' '.$select->user($userID, 'nama_belakang') ; ?></strong></a>
                </div>
            </div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#menu">Menu</a></li>
            </ul>
                
            <!-- Tab panes -->
            <div class="tab-content p-l-0 p-r-0">
                <div class="tab-pane active" id="menu">
                    <nav id="left-sidebar-nav" class="sidebar-nav">
                        <?php
                           if ($select->user($userID, 'role_role_id') == 'SUPERADMIN') {                           
                        ?>  
                        <ul id="main-menu" class="metismenu">                          
                            <li class="active">
                                <a href="#Dashboard"><i class="icon-home"></i> <span>Dashboard</span></a>
                            </li>
                            <li>
                                <a href="main/user_list.php"><i class="icon-users"></i> <span>User List</span></a>
                            </li>
                            <li>
                                <a href="main/list_wilayah2.php"><i class="icon-map"></i> <span>Region List</span></a>
                            </li>
                            <li>
                                <a href="main/mesin_list.php"><i class="icon-speedometer"></i> <span>Device List</span></a>
                            </li>
                            <li>
                                <a href="det_alat.php"><i class="icon-grid"></i> <span>Device Details</span></a>
                            </li>
                            <li>
                                <a href="main/notif_list.php"><i class="icon-notebook"></i> <span>Summary Report</span></a>
                            </li>
                            <li>
                                <a href="#" onClick="logoutx()"><i class="icon-power"></i> <span>Logout</span></a>
                            </li>
                        </ul>
                        <?php
                            }else{
                        ?>
                        <ul id="main-menu" class="metismenu">                          
                            <li class="active">
                                <a href="home.php"><i class="icon-home"></i> <span>Dashboard</span></a>
                            </li>
                            <li>
                                <a href="main/notif_list.php"><i class="icon-notebook"></i> <span>Summary Report</span></a>
                            </li>
                            <li>
                                <a href="#" onClick="logoutx()"><i class="icon-power"></i> <span>Logout</span></a>
                            </li>
                        </ul>
                        <?php
                            }
                        ?> 
                    </nav>
                </div>   
            </div>          
        </div>
    </div>

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-6 col-sm-12">                        
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-bars"></i></a></h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="home.php"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item ">Home</li>
                            <li class="breadcrumb-item active"><span id="message"></span></li>
                        </ul>
                    </div>            
                     <div class="col-lg-7 col-md-4 col-sm-12 text-right">                        
                        <div class="inlineblock text-center m-r-15 m-l-15 hidden-sm">
                            <div class="icon" ><a href="#largeModal" data-toggle="modal" data-target="#largeModal"><i class="fa fa-plus-circle fa-2x text-success"></i></a></div>                           
                            <span>Add Widget</span>
                        </div>
						<div class="inlineblock text-center m-r-15 m-l-15 hidden-sm">
                            <div class="icon" ><a href="javascript:ls_save('kode-trafo','009')"><i class="fa fa-edit fa-2x text-success"></i></a></div>                           
                            <span>kode 009</span>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Large Size -->
            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-dark">
                            <h4 class="title" id="largeModalLabel">Select Widget</h4>
                        </div>
                        <div class="modal-body">                        
                            <div class="row clearfix"> 
                                <div class="col-lg-4 col-md-4">
                                    <div class="card info-box-2">
                                        <a href="#">
                                            <div class="body" onClick="widget_checked('addwidget','1');">
                                                <div class="text-center"><h5><i class="fa fa-bolt text-danger"></i> Gauge <span class="widget-checked widget-checked-wg1"></span></h5></div>
                                                <div class="icon">                                                  
                                                    <img class="widget-gauge-icon" src="assets/images/w_gauge.png"></img>
                                                </div>
                                                <div class="content">
                                                    <div class="text">Average Value</div>
                                                    <div class="number"><span id="">R,S,T </span></div>
                                                </div>
                                            </div>                                          
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="card info-box-2">
                                        <a href="#">
                                            <div class="body" onClick="widget_checked('addwidget','2')">
                                                <div class="text-center"><h5><i class="fa fa-bolt text-danger"></i> Chart <span class="widget-checked widget-checked-wg2"></span></h5></div>
                                                <div class="icon">                                                  
                                                    <img class="widget-gauge-icon" src="assets/images/w_grafik.png"></img>
                                                </div>
                                                <div class="content">
                                                    <div class="text">Average Value</div>
                                                    <div class="number"><span id="">R,S,T </span></div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="card info-box-2">
                                        <a href="#">
                                            <div class="body" onClick="widget_checked('addwidget','3')">
                                                <div class="text-center"><h5><i class="fa fa-pie-chart-o text-danger"></i> Power Usage <span class="widget-checked widget-checked-wg3"></span></h5></div>
                                                <div class="icon">                                                  
                                                    <img class="widget-gauge-icon" src="assets/images/w_pie.png"></img>
                                                </div>
                                                <div class="content">
                                                    <div class="text">Average Value</div>
                                                    <div class="number"><span id="">R,S,T </span></div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                                            
                            </div>
                             <div class="row clearfix"> 
							
                                <div class="col-lg-4 col-md-4">
                                    <div class="card info-box-2">
                                        <a href="#">
                                            <div class="body"  onClick="widget_checked('addwidget','4')">
                                                <div class="text-center"><h5><i class="fas fa-map-marker-alt text-danger"></i> Coverage Area <span class="widget-checked widget-checked-wg4"></span></h5></div>
                                                <div class="icon">                                                  
                                                    <img class="widget-gauge-icon" style="margin-top:5px" src="assets/images/w_map.png"></img>
                                                </div>
                                                <div class="content">
                                                    <div class="text"> </div>
                                                    <div class="number"><span id="">	</span></div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>  

                                <div class="col-lg-4 col-md-4">
                                    <div class="card info-box-2">
                                        <a href="#">
                                            <div class="body" onClick="widget_checked('addwidget','5')">
                                                <div class="text-center"><h5><i class="icon icon-graph text-danger"></i> Summary Chart <span class="widget-checked widget-checked-wg5"></span></h5></div>
                                                <div class="icon">                                                  
                                                    <img class="widget-gauge-icon" src="assets/images/w_grafik.png"></img>
                                                </div>
                                                <div class="content">
                                                    <div class="text">Summary Value</div>
                                                    <div class="number"><span id="">R,S,T </span></div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="card info-box-2">
                                        <a href="#">
                                            <div class="body" onClick="widget_checked('addwidget','6')">
                                                <div class="text-center"><h5><i class="fas fa-book text-danger"></i> Summary Report<span class="widget-checked widget-checked-wg6"></span></h5></div>
                                                <div class="icon">                                                  
                                                    <img class="widget-gauge-icon" src="assets/images/w_table	.png"></img>
                                                </div>
                                                <div class="content">
                                                    <div class="text">Average Value</div>
                                                    <div class="number"><span id="">R,S,T </span></div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div> 
                            
                        </div>
                        <div class="modal-footer bg-dark">
						
							
                            
							<button type="button" class="btn btn-primary" onClick="selectwg()">Add widget</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal" onClick="$('.widget-checked').removeClass('fa fa-check-square-o text-success');">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
			<div id="widgetmap" class="row clearfix"></div>
            <div id="widgetdata" class="row clearfix"></div>
			<img id="loading" src="assets/images/loading.gif" alt="Girl in a jacket" width="100" height="100" style="position:fixed;top:350px;left:800px"; >
            <!-- Edit Task -->
            <div class="modal fade" id="editwidget" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content bg-dark">
                        <div class="modal-header">
                            <h6 class="title" id="defaultModalLabel">Edit Parameter <span class="widget_title"> </span></h6>
						</div>
                        <div class="modal-body bg-white">
                            <div class="row clearfix">
                                <div class="col-12">
                                    <div class="form-group">	
										<input type="text" class="hiddenx" value="" id="design_id" style="display: none;">
                                             <input type="text" class="form-control text-danger" placeholder="Panel name" id="panel_name" name="panel_name">
											 <p class="text-danger" hidden style ="margin-left:5px" id= "alert_panel_name">Data is empty </p>
                                    </div>
                                </div>
								<div class="col-6">
                                    <div class="form-group">                                   
                                        <select class="form-control show-tick m-b-10" id="protocol" name="protocol">
										<option>Select Protocol</option>
                                        <option>MQTT</option>
                                        <option>WEBAPI</option>
                                    </select>
									</div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">                                   
                                        <input type="text" class="form-control" placeholder="Topic" id="topic" name="topic">
										<p class="text-danger"  hidden style ="margin-left:5px" id= "alert_topic">Data is empty</p>
									</div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">                                   
                                        <input type="text" class="form-control" placeholder="Payload min" id="payload_min" name="payload_min">
										<p class="text-danger" hidden style ="margin-left:5px" id= "alert_payload_min">Data is empty</p>
									</div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">                                   
                                        <input type="text" class="form-control" placeholder="Payload max" id="payload_max" name="payload_max">
										<p class="text-danger" hidden style ="margin-left:5px" id= "alert_payload_max">Data is empty</p>
									</div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">                                   
                                        <input type="text" class="form-control" placeholder="Unit" id="unit" name="unit">
										<p class="text-danger" hidden style ="margin-left:5px" id= "alert_unit">Data is empty</p>
								   </div>
                                </div>
                                <div class="col-12">
                                    <select class="form-control show-tick m-b-10" id="qos" name="qos">
                                        <option>Qos</option>
                                        <option>0</option>
                                        <option>1</option>
                                        <option>2</option>
                                    </select>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">                                    
                                        <textarea type="text" class="form-control" placeholder="Description" id="description" name="description"></textarea>

								   </div>
                                </div>                   
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onClick="save_widget()">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
    
</div>

<!-- Javascript -->
<script src="theme/assets/bundles/libscripts.bundle.js"></script>    
<script src="theme/assets/bundles/vendorscripts.bundle.js"></script>
<script src="theme/assets/js/pages/charts/el_realtime.js"></script>
<script src="theme/assets/bundles/flotscripts.bundle.js"></script> 
<script src="theme/assets/bundles/morrisscripts.bundle.js"></script><!-- Morris Plugin Js -->
<script src="theme/assets/bundles/jvectormap.bundle.js"></script> <!-- JVectorMap Plugin Js -->
<script src="theme/assets/bundles/mainscripts.bundle.js"></script>
<script src="theme/assets/js/widgets/infobox/infobox-1.js"></script>
<script src="theme/assets/js/index2.js"></script>
<script src="assets/vendor/sweetalert/sweetalert.min.js"></script> <!-- SweetAlert Plugin Js --> 
<script src="theme/assets/js/pages/ui/dialogs.js"></script>
<script src="assets/js/jquery-ui.js"></script>
<script>
	$( "#protocol" ).change(function() {
		if ($('#protocol').val()=="MQTT"){
			$('#topic').show();
		}
		else{
		$('#topic').hide();
		}
	});
	function selectwg(){
			$('.modal').modal('hide');
			if(ls_read('addwidget')=="1"||ls_read('addwidget')=="2"||ls_read('addwidget')=="3")
			{
				$('#editwidget').modal('toggle');
				$('#editwidget').modal('show');
			ls_save('uact','ins');
			}else{
			ins_widget();
			}
							 }
	function widget_add(x){
		$('.widget-checked').removeClass('fa fa-check-square-o text-success');
	}
	function modify_widget(x){
        $('.widget_title').html(x);
        $('#pan	el_name').html();
        $('#topic').html();
        $('#payload_min').html();
        $('#payload_max').html();
        $('#unit').html();
        $('#qos').html();
        $('#description').html();
		ls_save('uact','edit');				
		load_widget(x);	
		ls_save('objid',x);		
    }
    function ins_widget(){
		var design_id = Date.now();
        var title=$('.widget_title').html();
        var panel_name=$('#panel_name').val();
        var topic=$('#topic').val();
        var pmin=$('#payload_min').val();
        var pmax=$('#payload_max').val();
        var unit=$('#unit').val();
        var qos=$('#qos').val();
        var desc=$('#description').val();
        var kd_widget = ls_read('addwidget');
		if(kd_widget=="4"){
			var panel_name='Map'
				topic='das/widget/prefix/map'
				pmin=0;
				pmax=1000
				unit=0
				qos=1
				desc='Coverage Area'					
		}
		if(kd_widget=="5"){
			var panel_name='Summary Chart'
				topic='das/widget/prefix/map'
				pmin=0;
				pmax=1000
				unit=0
				qos=1
				desc='Coverage Area'					
		}
		if(kd_widget=="6"){
			var panel_name='Summary Report'
				topic='das/widget/prefix/map'
				pmin=0;
				pmax=1000
				unit=0
				qos=1
				desc='Coverage Area'					
		}
        $.post( "process/ins_widget.php", { design_id: design_id ,title: title, panel_name: panel_name , topic: topic, payload_max: pmax, payload_min:pmin, unit: unit, qos: qos, description: desc,kd_widget:kd_widget})
        .done(function( data ) {
			$('#loading').show()
			var plotgauge =[];
			var reportchart=[];
			var binddata = [];
			var plotpie=[];
			$('#widgetdata').empty();
			try{			
			readsqlite();
			}
			catch(err){
				alert(err.message)
			}
			 $('#editwidget').modal('hide');
        })
		.fail(function( ms )
         {
			 $('#editwidget').modal('hide');
	
				plotgauge.push();
				var icon ='icon icon-energy text-danger';
				addwggauge(design_id,topic,panel_name,icon,unit);
				rendergauge();
			alert('tambah coy')
			alert('♥♥♥ Cek file "process/ins_widget.php" ♥♥♥');
         });
	}
	function upd_widget(){
        var title=$('.widget_title').html();
        var panel_name=$('#panel_name').val();
        var topic=$('#topic').val();
        var pmin=$('#payload_min').val();
        var pmax=$('#payload_max').val();
        var unit=$('#unit').val();
        var qos=$('#qos').val();
        var desc=$('#description').val();
		var design_id=$('#design_id').val();  
        $.post( "process/upd_widget.php", { title: title, panel_name: panel_name , topic: topic, payload_max: pmax, payload_min:pmin, unit: unit, qos: qos, description: desc, design_id: design_id})
        .done(function( data ) { 
        	var plotgauge =[];
			var reportchart=[];
			var binddata = [];
			var plotpie=[];
			$('#widgetdata').empty();
			try{			
			readsqlite();
			}
			catch(err){
				alert(err.message)
			}
			 $('#editwidget').modal('hide');
        
		})
        .fail(function( ms )
         {
          alert('♥♥♥ Cek file "process/upd_widget.php" ♥♥♥');
         });
    }
    function load_widget(x){
        $.ajax({method: "GET",url:'process/load_widget.php?design_id='+x})
         .done(function( ms )
         {
            var dtx=ms.split('#');
            $('.widget_title').html(dtx[0]);
            $('#panel_name').val(dtx[1]);
            $('#topic').val(dtx[2]);
            $('#payload_min').val(dtx[3]);
            $('#payload_max').val(dtx[4]);
            $('#unit').val(dtx[5]);
            $('#qos').val(dtx[6]);
            $('#description').val(dtx[7]);
            $('#design_id').val(dtx[8]);
         })
         .fail(function( ms )
         {
          alert('♥♥♥ Cek file "process/load_widget.php" ♥♥♥');
         });
    }
	
	function save_widget(){
		var uact=ls_read('uact');
		if(uact=='edit'){
			upd_widget()
		}else{
			ins_widget()
		}
		$('.panel_name_'+ls_read('objid')).html($('#panel_name').val());
	}
</script>
<script>
    
     $( function() {
    $( "#widgetdata" ).sortable(
    {
        update: function(event, ui) { 
                
                 var ids = $(this).children().get().map(function(el) {
                 distance: 5
			
                    return el.id
                        }).join(",");
                        $.post( "upd_position.php", { data:ids})
                            .done(function( data ) {  
                            });
                                console.log(ui)
        },
        start: function(event, ui) { 
            console.log(ui)
        }
    }
    );
    $( "#widgetdata" ).disableSelection();
  } );
</script>



<script>
    document.getElementById('cls_ctrl').focus();
    $(document).ready(function () {
        csys2();
        csys();
    });
    function hide_nav() {
        $('#cls_ctrl').attr('checked', false);
    }

    function logoutx() {
        if (confirm("Apakah anda akan logout?")) {
            window.location = "logout.php";
        }
    }

</script>
</html>
