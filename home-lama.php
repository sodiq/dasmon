<?php
error_reporting(0);
session_start();
require_once 'module/login/class.user.php';
$user_home = new USER();

if (!$user_home->is_logged_in()) {
    $user_home->redirect('login.php');
}

$stmt = $user_home->runQuery("SELECT * FROM tbl_im_users WHERE userID=:uid");
$stmt->execute(array(":uid" => $_SESSION['userSession']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);
require 'class/class.select.php';
$select = new select;
$userID = $_SESSION['userSession'];
$role = $select->user($userID,'role_role_id');
$area = $select->user($userID, 'area');

if($area == 'PUSAT'){
    $area_login = '%';
}else{
    $area_login = $area;
}

?>

<html lang="en"> 

<head> 
<title>Device Monitoring Systems</title>
<link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon"/>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Lucid Bootstrap 4.1.1 Admin Template">
<meta name="author" content="WrapTheme, design by: ThemeMakker.com">
<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/vendor/jvectormap/jquery-jvectormap-2.0.3.min.css"/>
<link rel="stylesheet" href="assets/vendor/toastr/toastr.min.css">
<link rel="stylesheet" href="assets/vendor/morrisjs/morris.min.css" />
<link rel="stylesheet" href="assets/vendor/sweetalert/sweetalert.css"/>
<link rel="stylesheet" href="assets/vendor/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="assets/vendor/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css">
<!-- MAIN CSS -->
<link rel="stylesheet" href="theme/assets/css/main.css">
<link rel="stylesheet" href="theme/assets/css/color_skins.css">
<link rel="stylesheet" href="assets/css/widget.css">
<script src="assets/js/jquery-3.4.1.js"></script>
<script type="text/javascript" src="assets/css/init.start.dasmon.js"></script>
<!-- <script type="text/javascript" src="assets/css/init.end.dasmon.js"></script> -->
<script src="module/mqtt/mqttws31.min.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/js/loader.js"></script>
<script type="text/javascript" src="assets/js/jquery.sparkline.js"></script>
<script src="assets/vendor/raphael/raphael-min.js"></script>
<script src="assets/vendor/justgage-toorshia/justgage.js"></script>
<script src ="module/mqtt/widget.js" type = "text/javascript"></script>
<script src ="module/mqtt/notif.js" type = "text/javascript"></script>
<script src ="module/mqtt/addwidget.js" type = "text/javascript"></script>
<style>
      html, body {
      max-width: 100%;
      overflow-x: hidden;
      }
      #sortable { list-style-type: none; margin: 0; padding: 0; width: 20px; }
</style>
<script>
    var ruser = '<?php echo $role; ?>';
    ls_save('kode-trafo','0001'); //simpan secara global kode trafo
    ls_save('role', ruser);
</script>

</head>
<body class="theme-dark">
<div hidden id ="setting_gui"></div>
<div id="wrapper">
    <nav class="navbar navbar-fixed-top wg-thumb">
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
            </div>
            <div class="navbar-brand">              
            </div>
            <div class="navbar-right">
                <div id="navbar-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                <i class="icon-bell"></i>
                                <span id="notif-dot" class="notification-dot" style="display:none"></span>
                            </a>
                            <ul class="dropdown-menu notifications">
                                <li class="header"><strong>You have <span id="status_notif"></span> new Notifications</strong></li>
                                <div id="notif-body"></div>                            
                                <li class="footer"><a href="notifikasi_list.php" class="more">See all notifications</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" onClick="logoutx()" class="icon-menu"><i class="icon-login"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div id="left-sidebar" class="sidebar">
        <div class="sidebar-scroll">
            <div class="user-account">
                 <img src="<?php echo 'assets/images/';
                if ($select->user($userID, 'photo') == '') {
                    echo 'f_avatar.png';
                } else {
                    echo $select->user($userID, 'photo');
                }; ?>" class="rounded-circle user-photo">

                <div class="dropdown">
                    <span>Welcome,</span>
                   <a href="" class="user-name"><strong> <?php echo $select->user($userID, 'role_role_id') . ' <br> ' .
                $select->user($userID, 'nama_depan'). ' '.$select->user($userID, 'nama_belakang'). ' '.$select->user($userID, 'area') ; ?></strong></a>
                </div>
            </div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#menu">Menu</a></li>
            </ul>
                
            <!-- Tab panes -->
            <div class="tab-content p-l-0 p-r-0">
                <div class="tab-pane active" id="menu">
                    <nav id="left-sidebar-nav" class="sidebar-nav">
                        <?php
                           if ($select->user($userID, 'role_role_id') == 'SUPERADMIN') {                           
                        ?>  
                        <ul id="main-menu" class="metismenu">                          
                            <li class="active">
                                <a href="home.php" onclick="removeLs()"><i class="icon-home"></i> <span>Dashboard</span></a>
                            </li>
                            <li>
                                <a href="main/user_list.php"><i class="icon-users"></i> <span>User List</span></a>
                            </li>
                            <li>
                                <a href="main/list_wilayah2.php"><i class="icon-map"></i> <span>Region List</span></a>
                            </li>
                            <li>
                                <a href="main/mesin_list.php"><i class="icon-speedometer"></i> <span>Device List</span></a>
                            </li>
                            <li>
                                <a href="main/det_alat.php"><i class="icon-grid"></i> <span>Device Details</span></a>
                            </li>
                            <li>
                                <a href="main/event_history.php"><i class="fa fa-bar-chart-o"></i> <span>Event History</span></a>
                            </li>
                            <li>
                                <a href="main/notif_list.php"><i class="icon-notebook"></i> <span>Summary Report</span></a>
                            </li>
                            <li>
                                <a href="main/list_task.php"><i class="icon-briefcase"></i> <span>Task List</span></a>
                            </li>
                            <li>
                                <a href="#" onClick="logoutx()"><i class="icon-power"></i> <span>Logout</span></a>
                            </li>
                        </ul>
                        <?php
                            }else{
                        ?>
                        <ul id="main-menu" class="metismenu">                          
                            <li class="active">
                                <a href="home.php" onclick="removeLs()"><i class="icon-home"></i> <span>Dashboard</span></a>
                            </li>
                            <li>
                                <a href="main/notif_list.php"><i class="icon-notebook"></i> <span>Summary Report</span></a>
                            </li>
                            <li>
                                <a href="#" onClick="logoutx()"><i class="icon-power"></i> <span>Logout</span></a>
                            </li>
                        </ul>
                        <?php
                            }
                        ?> 
                    </nav>
                </div>   
            </div>          
        </div>
    </div>

    <div id="main-content" >
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-6 col-sm-12">                        
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-bars"></i></a></h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a onclick="removeLs()" href="home.php"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item"><a onclick="removeLs()" href="home.php">Home</a></li>
                            <li class="breadcrumb-item active"><span id="message"></span></li>
                        </ul>
                    </div> 
                    <?php
                      if ($select->user($userID, 'role_role_id') == 'SUPERADMIN') {                           
                    ?>             
                      <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                             <div id="devicereg" class="inlineblock text-center m-r-15 m-l-15">
                                <div class="icon"><a href="#"  data-toggle="modal" data-target="#register"><i class="fa fa-edit (alias) fa-2x text-primary"></i></a></div>               
                                <span>Device Reg.</span>
                            </div>

                            <div id="addwg" class="inlineblock text-center m-r-15 m-l-15 hidden-sm">
                                <div class="icon" id="addwidgetw"><a href="#largeModal"  data-toggle="modal" data-target="#largeModal"><i class="fa fa-plus-circle fa-2x text-success"></i></a></div>                           
                                <span id="fontaddwidgetw">Add Widget</span>
                            </div>
                        </div>
                    <?php
                      }else{
                    ?>
                        <div class="col-lg-7 col-md-4 col-sm-12 text-right" style="display: none;">                        
                            <div style="display: none;" class="inlineblock text-center m-r-15 m-l-15 hidden-sm">
                                <div class="icon" id="addwidgetw"><a href="#largeModal"  data-toggle="modal" data-target="#largeModal"><i class="fa fa-plus-circle fa-2x text-success"></i></a></div>                           
                                <span id="fontaddwidgetw">Add Widget</span>
                            </div>
                        </div>
                    <?php
                       }
                    ?> 
                </div>
            </div>

            <!-- Large Size -->
            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-dark">
                            <h4 class="title" id="largeModalLabel">Select Widget</h4>
                        </div>
                        <div class="modal-body">                        
                            <div class="row clearfix"> 
                                <div class="col-lg-4 col-md-4" id="wg_1">
                                    <div class="card info-box-2">
                                        <a href="#">
                                            <div class="body" onClick="widget_checked('addwidget','1');">
                                                <div class="text-center"><h5><i class="fa fa-bolt text-danger"></i> Gauge <span class="widget-checked widget-checked-wg1"></span></h5></div>
                                                <div class="icon">                                                  
                                                    <img class="widget-gauge-icon" src="assets/images/w_gauge.png"></img>
                                                </div>
                                                <div class="content">
                                                    <div class="text">Average Value</div>
                                                    <div class="number"><span id="">R,S,T </span></div>
                                                </div>
                                            </div>                                          
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4" id="wg_2">
                                    <div class="card info-box-2">
                                        <a href="#">
                                            <div class="body" onClick="widget_checked('addwidget','2')">
                                                <div class="text-center"><h5><i class="fa fa-bolt text-danger"></i> Chart <span class="widget-checked widget-checked-wg2"></span></h5></div>
                                                <div class="icon">                                                  
                                                    <img class="widget-gauge-icon" src="assets/images/w_grafik.png"></img>
                                                </div>
                                                <div class="content">
                                                    <div class="text">Average Value</div>
                                                    <div class="number"><span id="">R,S,T </span></div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4" id="wg_3">
                                    <div class="card info-box-2">
                                        <a href="#">
                                            <div class="body" onClick="widget_checked('addwidget','3')">
                                                <div class="text-center"><h5><i class="fa fa-pie-chart-o text-danger"></i> Power Usage <span class="widget-checked widget-checked-wg3"></span></h5></div>
                                                <div class="icon">                                                  
                                                    <img class="widget-gauge-icon" src="assets/images/w_pie.png"></img>
                                                </div>
                                                <div class="content">
                                                    <div class="text">Average Value</div>
                                                    <div class="number"><span id="">R,S,T </span></div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                                            
                            </div>
                             <div class="row clearfix"> 
                            
                                <div class="col-lg-4 col-md-4" id="wg_4">
                                    <div class="card info-box-2">
                                        <a href="#">
                                            <div class="body"  onClick="widget_checked('addwidget','4')">
                                                <div class="text-center"><h5><i class="fas fa-map-marker-alt text-danger"></i> Coverage Area <span class="widget-checked widget-checked-wg4"></span></h5></div>
                                                <div class="icon">                                                  
                                                    <img class="widget-gauge-icon" style="margin-top:5px" src="assets/images/w_map.png"></img>
                                                </div>
                                                <div class="content">
                                                    <div class="text"> </div>
                                                    <div class="number"><span id="">    </span></div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>  

                                <div class="col-lg-4 col-md-4" id="wg_5">
                                    <div class="card info-box-2">
                                        <a href="#">
                                            <div class="body" onClick="widget_checked('addwidget','5')">
                                                <div class="text-center"><h5><i class="icon icon-graph text-danger"></i> Summary Chart <span class="widget-checked widget-checked-wg5"></span></h5></div>
                                                <div class="icon">                                                  
                                                    <img class="widget-gauge-icon" src="assets/images/w_grafik.png"></img>
                                                </div>
                                                <div class="content">
                                                    <div class="text">Summary Value</div>
                                                    <div class="number"><span id="">R,S,T </span></div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4" id="wg_6">
                                    <div class="card info-box-2">
                                        <a href="#">
                                            <div class="body" onClick="widget_checked('addwidget','6')">
                                                <div class="text-center"><h5><i class="fas fa-book text-danger"></i> Summary Report<span class="widget-checked widget-checked-wg6"></span></h5></div>
                                                <div class="icon">                                                  
                                                    <img class="widget-gauge-icon" src="assets/images/w_table.png"></img>
                                                </div>
                                                <div class="content">
                                                    <div class="text">Average Value</div>
                                                    <div class="number"><span id="">R,S,T </span></div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4 col-md-4" id="wg_7">
                                    <div class="card info-box-2">
                                        <a href="#">
                                            <div class="body" onClick="widget_checked('addwidget','7')">
                                                <div class="text-center"><h5><i class="icon icon-bar-chart text-danger"></i> Compare Bar <span class="widget-checked widget-checked-wg7"></span></h5></div>
                                                <div class="icon">                                                  
                                                    <img class="widget-gauge-icon" src="assets/images/bar-chart.JPG"></img>
                                                </div>
                                                <div class="content">
                                                    <div class="text">Summary Value</div>
                                                    <div class="number"><span id="">R,S,T </span></div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4 col-md-4" id="wg_8">
                                    <div class="card info-box-2">
                                        <a href="#">
                                            <div class="body" onClick="widget_checked('addwidget','1a')">
                                                <div class="text-center"><h5><i class="icon icon-drop text-danger"></i> Oil Level<span class="widget-checked widget-checked-wg8"></span></h5></div>
                                                <div class="icon">                                                  
                                                <img class="widget-gauge-icon" src="assets/images/oillevel.JPG"></img>
                                                </div>
                                                <div class="content">
                                                    <div class="text">Summary Value</div>
                                                    <div class="number"><span id="">Oil </span></div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                                
                            </div> 
                            
                        </div>
                        <div class="modal-footer bg-dark">                            
                            <button type="button" class="btn btn-primary" onClick="selectwg()">Add widget</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal" onClick="$('.widget-checked').removeClass('fa fa-check-square-o text-success');">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
            <div id="widgetmapx"></div>
            <div id="widgetdata" class="row clearfix" style="margin-left: -15px"></div>
            <img id="loading" src="assets/images/loading.gif" width="100" height="100" style="position:fixed;top:350px;left:800px"; >
            <!-- Edit Task -->
           
           <div class="modal fade" id="editwidget" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content bg-dark">
                        <div class="modal-header">
                            <h6 class="title" id="defaultModalLabel">Edit Parameter <span class="widget_title"> </span></h6>
                        </div>
                        <div class="modal-body bg-white">
                             <div class="row clearfix">
                                <div class="col-12">
                                    <div class="form-group">    
                         <input type="text" class="hiddenx" value="" id="design_id" style="display: none;">
                                             <input type="text" class="form-control" placeholder="Panel name" id="panel_name" name="panel_name">
                                    </div>
                                </div>
                                <div class="input-group col-12">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><select id="protokol" onchange="changeProtokol()"><option value="MQTT">MQTT</option><option value="HTTP">API</option></select></span>
                                    </div>
                                    <input type="text" name="topic" id="topic" class="form-control" placeholder="Topic">
                                </div>
                                 <div id="webapi" class="col-12 mt-3" style="display:none">
                                    <div class="form-group">
                                    <div class="input-group">                               
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Parameter</span>
                                    </div>
                                    <input type="text" id="trafo_code" class="form-control" placeholder="Trafo Code">
                                    <input type="text" id="phase" class="form-control" placeholder="phase">
                                    <input type="text" id="getdata" class="form-control" placeholder="getdata">
                                    </div>
                                </div>
                                </div>
                                <div class="col-12 mt-3">
                                    <div class="form-group">                                   
                                        <input type="number" min="0" class="form-control" placeholder="Payload min" id="payload_min" name="payload_min">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">                                   
                                        <input type="number" min="0" class="form-control" placeholder="Payload max" id="payload_max" name="payload_max">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">                                   
                                        <input type="text" class="form-control" placeholder="Unit" id="unit" name="unit">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <select class="form-control show-tick m-b-10" id="qos" name="qos">
                                        <option value ="" selected>Qos</option>
                                        <option value ="0">0</option>
                                        <option value ="1">1</option>
                                        <option value ="2">2</option>
                                    </select>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">                                    
                                        <textarea type="text" class="form-control" placeholder="Description" id="description" name="description"></textarea>
                                    </div>
                                </div>                   
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onClick="save_widget()">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            
                        
            <div class="modal fade" id="compareModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content bg-dark">
                        <div class="modal-header">
                            <h6 class="title" id="judul_modal"> Compare Bar<span class="widget_title"> </span></h6>
                        </div>
                        <div class="modal-body bg-white testing">
                             <div class="row clearfix">
                                <div class="col-12">
                                    <div class="form-group">    
                         <input type="text" class="hiddenx" value="" id="design_id" style="display: none;">
                                             <input type="text" class="form-control" placeholder="Panel name" id="panel_name_alat" >
                                             <p class="text text-danger" id="warningPanel" style="display:none"><i class="fa fa-warning"></i> Silahkan masukkan nama panel</p>
                                    </div>
                                </div>
                                <div class="col-12">
                                <!--  -->
                                    <div class="form-group">                                   
                                    <select style="width:465px" class="form-control select2 kode_alat" multiple="multiple" id="kode_alat" placeholder="Kode Alat">
                                    <?php $qry = $select->get_kode_alat($area_login); 
                                    while($row = mysql_fetch_array($qry)){
                                        echo '<option value="'.$row['kode_mesin'].'">'.$row['kode_mesin'].'-'.$row['nama'].'</option>';
                                    }
                                    ?>
                                    </select>
                                    <p class="text text-danger" id="warningAlat" style="display:none"><i class="fa fa-warning"></i> Silahkan masukkan kode alat</p>
                                    </div>
                                </div>
                                <input type="hidden" id="id_alat"> 
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
            
        </div>
    </div>
    
</div>
<!-- Modal Popup Device Register--> 
<div id="register" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content bg-dark">
 <div class="modal-header">
   
                            <h6 class="title" id="defaultModalLabel">Device Register<span class="widget_title"> </span></h6>
                        </div>
                        <div class="modal-body bg-white">
                             <div class="row clearfix">

                                   <div class="col-6">
                                        <div class="input-group">
                                               <span class="input-group-addon" id="">Province </span>
                                        <div class="row">
                                              <input class="form-control" style="margin-left: 15px;" id="nama_provinsi" name="nama_provinsi" type="text"   value="<?php echo $area;?>" readonly>
                                        </div>
                                        </div>
                                    </div>


                                    <div class="input col-6">
                                       <div class="input-group">
                                        <span class="input-group-addon" id="">City </span>
                                        <div class="row">
                                        <input onchange="placee()" class="form-control" style="margin-left: 15px; width: 210px;" id="nama_kota" name="nama_kota" type="text" onkeypress="suggest(this.value);" value="<?php echo $kota; ?>"><div id="suggest"></div>
                                         </div>
                                        </div>
                                    </div>
                                    <br/><br/><br/>

                                    <div class="col-md-6">
                                        <div class="input-group">
                                        <span class="input-group-addon" id="">District </span>
                                        <div class="row">
                                        <input onchange="placee()" class="form-control" style="margin-left: 15px; width: 210px;" id="nama_kecamatan" name="nama_kecamatan" type="text" onkeypress="suggest_kec(this.value);" value="<?php echo $kec; ?>"><div id="suggest_kec"></div>
                                         </div>
                                        </div>
                                    </div>

                                     <br/><br/><br/>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                        <span class="input-group-addon" id="">Village </span>
                                        <div class="row">
                                        <input onchange="placee()" class="form-control" style="margin-left: 15px; width: 210px;" id="nama_desa" name="nama_desa" type="text" onkeypress="suggest_desa(this.value);"  value="<?php echo $desa; ?>"><div id="suggest_desa"></div>
                                         </div>
                                        </div>
                                    </div>
                                    

                                    <div class="input col-6">
                                        <div class="form-group">                                   
                                            <select class="form-control" style="margin-top: 10px; width: 210px;" id="teknisi_id" name="teknisi_id" type="text" placeholder="Technician ID">
                                                  <?php $qry = $select->teknisi($area_login); 
                                                    while($row = mysql_fetch_array($qry)){
                                                        echo '<option value="'.$row['userCode'].'">'.$row['userCode'].'-'.$row['userName'].'</option>';
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>

                                   <div class="input col-6">
                                        <div class="form-group">                                   
                                            <input onChange="addRow(this.value)" class="form-control" style="margin-top: 10px; height: 60px; font-size:36px; width: 210px;" id="scan" name="scan" type="text">
                                        </div>
                                    </div>
                                    <br/><br/><br/>


                            <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <table id="idRow" name="idRow" class="table table-bordered table-striped mb-0"  style="margin-left: 15px; width: 460px; height: 50px; overflow-y: scroll;">
                             <thead>
                                <tr>
                                  <th scope="col">Device Code</th>
                                  <th scope="col">Location</th>
                                  <th scope="col">Action</th>
                                </tr>
                              </thead>

                            </table>
                        </div>

                            </div> <!-- tutup row cleaefix -->
                        </div>

                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-reg" onClick="save_reg()">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                  
        </div>
    </div>
</div>
</div>

<?php include "modal_notif.php";?>

<audio id="status10">
  <source src="assets/sound/alarm.mp4" type="audio/mp4">
</audio>
<audio id="status11">
  <source src="assets/sound/train_low.mp3" type="audio/mp3">
</audio>



<!-- Javascript -->
<script src="sse/receive_sse.js"></script>
<script src="theme/assets/bundles/libscripts.bundle.js"></script>    
<script src="theme/assets/bundles/vendorscripts.bundle.js"></script>
<script src="assets/vendor/toastr/toastr.js"></script>
<script src="theme/assets/js/pages/charts/el_realtime.js"></script>
<script src="theme/assets/bundles/flotscripts.bundle.js"></script> 
<script src="theme/assets/bundles/morrisscripts.bundle.js"></script><!-- Morris Plugin Js -->
<script src="theme/assets/bundles/jvectormap.bundle.js"></script> <!-- JVectorMap Plugin Js -->
<script src="theme/assets/bundles/mainscripts.bundle.js"></script>
<script src="theme/assets/js/widgets/infobox/infobox-1.js"></script>
<script src="theme/assets/js/index2.js"></script>
<script src="assets/vendor/sweetalert/sweetalert.min.js"></script> <!-- SweetAlert Plugin Js --> 
<script src="theme/assets/js/pages/ui/dialogs.js"></script>
<script src="assets/js/jquery-ui.js"></script>
<script src="assets/vendor/select2/dist/js/select2.full.min.js"></script>
<script src="assets/vendor/bootstrap-progressbar/js/bootstrap-progressbar.min.js"></script>

<script>

function changeProtokol(){
    var select = $('#protokol').val();
    if(select == 'MQTT'){
        $('#webapi').hide();
        $('#topic').attr('placeholder', 'Topic');
    }else{
        $('#webapi').show();
        $('#topic').attr('placeholder', 'URL');
    }
}


$(function() {
    $('#progress-format1 .progress-bar').progressbar({
        display_text: 'fill'
    });

    $('#progress-format2 .progress-bar').progressbar({
        display_text: 'fill',
        use_percentage: false
    });

    $('#progress-custom-format .progress-bar').progressbar({
        display_text: 'fill',
        use_percentage: false,
        amount_format: function(p, t) {
            return p + ' of ' + t;
        }
    });

    $('#progress-striped .progress-bar, #progress-striped-active .progress-bar, #progress-stacked .progress-bar').progressbar({
        display_text: 'fill'
    });

    $('.progress.vertical .progress-bar').progressbar();
    $('.progress.vertical.wide .progress-bar').progressbar({
        display_text: 'fill'
    });

});


$( document ).ready(function() {
    var nama = ls_read('nama_trafo');
    if (nama != null){
        $('#message').html(nama)
    }
});

 $(function () {
    $('.select2').select2();
});
    $( "#protocol" ).change(function() {
        if ($('#protocol').val()=="MQTT"){
            $('#topic').show();
        }
        else{
        $('#topic').hide();
        }
    });
    
    
    $('#largeModal').on('shown.bs.modal', function () {
        
        $('#largeModal').trigger('show')
        
            $('#wg_1').css({ opacity: 1 });
            $('#wg_2').css({ opacity: 1 });
            $('#wg_3').css({ opacity: 1 });
            $('#wg_4').css({ opacity: 1 });
            $('#wg_5').css({ opacity: 1 });
            $('#wg_6').css({ opacity: 1 });
            $.ajax({method: "GET",url:'pr_wg.php'})
            var dtwidget=''
                 $.get( "pr_wg.php" )
                        .done(function( data ) {
                            if (data== null){
                            }
                            else{
                            $.each(JSON.parse(data), function(idx, obj) {
                            $('.widget-checked-wg'+obj.kd_widget).removeClass('fa fa-check-square-o text-success')
                            $('#wg_'+obj.kd_widget).css({ opacity: 0.5 });
                                dtwidget+=obj.kd_widget;
                            })
                            ls_save('wg_pr',dtwidget);
                            }
                        });     
                    
        })
    function selectwg(){
		$('.widget_title').html('');
		$('#panel_name').val('');
        $('#payload_min').val('');
        $('#payload_max').val('');
        $('#unit').val('');
        $('#qos').val('');
        $('#description').val('');
        $('#phase').val('')
		$('#getdata').val('');
        $('#trafo_code').val('');
        $('#topic').val('');
		
            $('.modal').modal('hide')
            if(ls_read('addwidget')=="1"||ls_read('addwidget')=="2"||ls_read('addwidget')=="3" ||ls_read('addwidget')=="1a")
            {
                $('#editwidget').modal('toggle');
                $('#editwidget').modal('show');
            ls_save('uact','ins');
            }else if(ls_read('addwidget')=="7"){
				 $('#judul_modal').html('Add Compare Bar');
                $('#panel_name_alat').val('');
                $('#kode_alat').val(null).trigger('change');
                $('#compareModal').modal('toggle');
                $('#compareModal').modal('show');
                $('#btn_save').attr('onclick','save_widget()');
                ls_save('uact','ins');   
            }else{
            ins_widget();
            }
                             }
    function widget_add(x){
        $('.widget-checked').removeClass('fa fa-check-square-o text-success');
    }
    function modify_widget(x){
        $('.widget_title').html(x);
        $('#pan el_name').html();
        $('#topic').html();
        $('#payload_min').html();
        $('#payload_max').html();
        $('#unit').html();
        $('#qos').html();
        $('#description').html();
        ls_save('uact','edit');             
        load_widget(x); 
        ls_save('objid',x);     
    }
    function ins_widget(){
        var design_id = Date.now();
        var title=$('.widget_title').html();
        var panel_name=$('#panel_name').val();
        var pmin=$('#payload_min').val();
        var pmax=$('#payload_max').val();
        var unit=$('#unit').val();
        var qos=$('#qos').val();
        var desc=$('#description').val();
        var kd_widget = ls_read('addwidget');
        var protocol= $('#protokol').val();
        var topic;
        var url;

        if (protocol == 'MQTT'){
            topic   = $('#topic').val();
        }else{
            url     = $('#topic').val();
            topic   = $('#trafo_code').val()+"/phase";
        }
        
        var phase = $('#phase').val()
        var getdata= $('#getdata').val();
        var code= $('#trafo_code').val();
		
		if(kd_widget == "1"){
			if(panel_name == '' || pmin == '' || pmax == '' || unit == '' || qos == '' || topic == ''){
				alert('Please check your form, column cannot empty');
				return false;
			}
		}
        
        if(kd_widget=="4"){
            var panel_name='Map'
                topic='das/widget/prefix/map'
                pmin=0;
                pmax=1000
                unit=0
                qos=1
                desc='Coverage Area'                    
        }
        if(kd_widget=="5"){
            var panel_name='Summary Chart'
                topic='das/widget/prefix/summary'
                pmin=0;
                pmax=1000
                unit=0
                qos=1
                desc='Summary Chart'                    
        }
        if(kd_widget=="6"){
            var panel_name='Summary Report'
                topic='das/widget/prefix/report'
                pmin=0;
                pmax=1000
                unit=0
                qos=1
                desc='Coverage Area'                    
        }
        if(kd_widget=='7'){
            var kode   = $('#kode_alat').val();
            panel_name = $('#panel_name_alat').val();
                topic='das/widget/prefix/compare'
                pmin=0;
                pmax=1000
                unit=0
                qos=1
                desc='Compare Bar Chart'
			if(kode == '' || panel_name == ''){
				alert('Please check your form, column cannot empty');
				return false;
			}
        }
       $.post( "process/ins_widget.php", { 
       design_id: design_id ,
       title: title, 
       panel_name: panel_name , 
       topic: topic, 
       payload_max: pmax, 
       payload_min:pmin, 
       unit: unit, 
       qos: qos, 
       description: desc,
       kd_widget:kd_widget,
       protocol:protocol, 
       kode: kode,
       url: url,
       phase: phase,
       getdata: getdata,
       trafo_code: code})
        //$.post( "process/ins_widget.php", { design_id: design_id ,title: title, panel_name: panel_name , topic: topic, payload_max: pmax, payload_min:pmin, unit: unit, qos: qos, description: desc,kd_widget:kd_widget,active:'y',protocol:'MQTT',url:'das.pt-mgi.co.id',phase:'R',getdata:'phase',code:'ok'})
        .done(function( data ) {
            $('#loading').show()
            var plotgauge =[];
            var reportchart=[];
            var binddata = [];
            var plotpie=[];
            $('#widgetdata').empty();
            $('#widgetmapx').empty();
            readsqlite();
       client.subscribe(prefix+'#');
             toastr.options.timeOut = "false";
             toastr.options.closeButton = true;
             toastr.options.positionClass = 'toast-top-center';
             toastr['info']('Data successfully inserted!');
             $('#editwidget, #compareModal').modal('hide');
        })
        .fail(function( ms )
         {
             $('#editwidget, #compareModal').modal('hide');
    
                plotgauge.push();
                var icon ='icon icon-energy text-danger';
                addwggauge(design_id,topic,panel_name,icon,unit);
                rendergauge();
            alert('tambah coy')
            alert('♥♥♥ Cek file "process/ins_widget.php" ♥♥♥');
         });
    }
    function upd_widget(){
        var title=$('.widget_title').html();
        var panel_name=$('#panel_name').val();
        var topic=$('#topic').val();
        var pmin=$('#payload_min').val();
        var pmax=$('#payload_max').val();
        var unit=$('#unit').val();
        var qos=$('#qos').val();
        var desc=$('#description').val();
        var design_id=$('#design_id').val();  
        $.post( "process/upd_widget.php", { title: title, panel_name: panel_name , topic: topic, payload_max: pmax, payload_min:pmin, unit: unit, qos: qos, description: desc, design_id: design_id})
        .done(function( data ) { 
            var plotgauge =[];
            var reportchart=[];
            var binddata = [];
            var plotpie=[];
            $('#widgetmapx').empty();
            $('#widgetdata').empty();
            try{            
            readsqlite();
            }
            catch(err){
                alert(err.message)
            }
       client.subscribe(prefix+'#');
            toastr.options.timeOut = "false";
             toastr.options.closeButton = true;
             toastr.options.positionClass = 'toast-top-center';
             toastr['info']('Data successfully updated!');
             $('#editwidget').modal('hide');
        
        })
        .fail(function( ms )
         {
          alert('♥♥♥ Cek file "process/upd_widget.php" ♥♥♥');
         });
    }
    function load_widget(x){
        $.ajax({method: "GET",url:'process/load_widget.php?design_id='+x})
         .done(function( ms )
         {
            var dtx=ms.split('#');
            $('.widget_title').html(dtx[0]);
            $('#panel_name').val(dtx[1]);
            $('#topic').val(dtx[2]);
            $('#payload_min').val(dtx[3]);
            $('#payload_max').val(dtx[4]);
            $('#unit').val(dtx[5]);
            $('#qos').val(dtx[6]);
            $('#description').val(dtx[7]);
            $('#design_id').val(dtx[8]);
         })
         .fail(function( ms )
         {
          alert('♥♥♥ Cek file "process/load_widget.php" ♥♥♥');
         });
    }
    
    function save_widget(){
        var uact=ls_read('uact');
        if(uact=='edit'){
            upd_widget()
        }else{
            ins_widget()
        }
        $('.panel_name_'+ls_read('objid')).html($('#panel_name').val());
    }

     function save_reg(){
        $.post (
            "mesin_reg.php", 
            { 
            nama_provinsi:$('#nama_provinsi').val(),
            nama_kota:$('#nama_kota').val(),
            nama_kecamatan:$('#nama_kecamatan').val(),
            nama_desa:$('#nama_desa').val(),
            device_code:$('#device_code').val(),
            teknisi_id: $('#teknisi_id').val(),
            active:$("input[name=active]:checked").val(),
            max_volt: $('#max_volt').val(),
            max_temp: $('#max_temp').val()
            }
            )
        
        .done(function(data) {
            $('#register').modal('hide');
           toastr.info(
              'Data successfully inserted!',
              '',
              {
                timeOut: 1000,
                fadeOut: 1000,
                positionClass: 'toast-top-center',
               
              }
            );
     });    
}
</script>
<script>
        
     $( function() {
    $( "#widgetdata" ).sortable(
    {
        update: function(event, ui) { 
                
                 var ids = $(this).children().get().map(function(el) {
                 distance: 5
            
                    return el.id
                        }).join(",");
                        $.post( "upd_position.php", { data:ids})
                            .done(function( data ) {  
                            });
                                console.log(ui)
        },
        start: function(event, ui) { 
            console.log(ui)
        }
    }
    );
    $( "#widgetdata" ).disableSelection();
  } );
</script>



<script>
    function hide_nav() {
        $('#cls_ctrl').attr('checked', false);
    }

    function logoutx() {
        if (confirm("Apakah anda akan logout?")) {
            window.location = "logout.php";
            localStorage.removeItem('role');
        }
    }
    function removeLs(){
        localStorage.removeItem('kode_trafo');
        localStorage.removeItem('nama_trafo');
    }
    
    function update_compare(id,panel){
        ls_save('uact','edit'); 
        $('#compareModal').modal('show');
        $.ajax({
            type: "POST",
            url: "process/search_kode.php",
            dataType: "json",
            data:{design:id, jenis:1},
            success: function(response){
                $('#judul_modal').html('Modify Compare Bar');
                $('#id_alat').val(id);
                $('#panel_name_alat').val(panel);
                $('#kode_alat').val(response).trigger('change');
                $('#btn_save').attr('onclick','update_save()');
                }
            });
    }

    function update_save(){
        var id      = $('#id_alat').val();
        var title   = $('#panel_name_alat').val();
        var kode    = $('#kode_alat').val();
        $.post( "process/update_compare.php", { id:id, panel_name:title, kode:kode })
        .done(function( data ) {
            var plotgauge =[];
            var reportchart=[];
            var binddata = [];
            var plotpie=[];
            $('#widgetmapx').empty();
            $('#widgetdata').empty();
            try{            
            readsqlite();
            }
            catch(err){
                alert(err.message)
            }
            toastr.options.timeOut = "false";
            toastr.options.closeButton = true;
            toastr.options.positionClass = 'toast-top-center';
            toastr['info']('Data successfully updated!');
            $('#compareModal').modal('hide');
        });
    }
    
     function fullwidht(id){
        $('#wg'+id).removeClass('col-lg-6').addClass('col-lg-12');
        $('#icon'+id).addClass('icon-size-actual');
        $('#lbl'+id).html('Restore');
        $('#full'+id).removeAttr('onclick');
        $('#full'+id).attr('onclick','restore('+id+')');
    }

    function restore(id){
        $('#wg'+id).removeClass('col-lg-12').addClass('col-lg-6');
        $('#icon'+id).removeClass('icon-size-actual').addClass('icon-size-fullscreen');
        $('#lbl'+id).html('Full Screen');
        $('#full'+id).removeAttr('onclick');
        $('#full'+id).attr('onclick','fullwidht('+id+')');
    }

</script>


</html>
