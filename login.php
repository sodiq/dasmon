<?php
session_start();
require_once 'module/login/class.user.php';
$user_login = new USER();

if($user_login->is_logged_in()!="")
{
	$user_login->redirect('index.php');
}

if(isset($_POST['btn-login']))
{
	$email = trim($_POST['txtemail']);
	$upass = trim($_POST['txtupass']);
	
	if($user_login->login($email,$upass))
	{
		$user_login->redirect('index.php');
	}
}
?>

<!DOCTYPE html>
<html>
<head>
<title>HMI > Login</title>
<link rel="icon" href="assets/images/electrict.png" type="image/x-icon"/>
<link href="assets/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="assets/css/login_css.css" />
<script type="text/javascript" src="assets/js/login_js.js"></script>
</head>
<body>

<!------ Include the above in your HEAD tag ---------->
<?php 
	if(isset($_GET['inactive']))
	{
?>
	<div class='alert alert-error'>
		<button class='close' data-dismiss='alert'>&times;</button>
			<strong>Sorry!</strong> This Account is not Activated Go to your Inbox and Activate it. 
	</div>
<?php
	}
?> 

<div class="container">
	<div id="login-box">
		<div class="logo">
			<img src="assets/images/logologin2.png" style="margin-left: 60px; padding-top: 20px;" />
			<h1 class="logo-caption">Login</h1>
		</div><!-- /.logo -->

		<form class="form-signin" method="post">
		    <?php
			    if(isset($_GET['error']))
				{
			?>
				<div class="alert alert-danger" style="margin-bottom: -15px; margin-top: 15px;">
					<button class='close' data-dismiss='alert'>&times;</button>
						<strong>Wrong Details!</strong> 
				</div>
			<?php
				}
			?>
				<div class="controls">
					<input type="text" name="txtemail" placeholder="Username" class="form-control" />
					<input type="password" name="txtupass" placeholder="Password" class="form-control" />
					<button type="submit" class="btn btn-default btn-block btn-custom" name="btn-login">Login</button>
				</div><!-- /.controls -->
		</form>
	</div><!-- /#login-box -->
</div><!-- /.container -->
<div id="particles-js"></div>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/particles.js/2.0.0/particles.min.js"></script>-->
</body>
</html>
<script>
document.getElementById('user').focus();
</script>