<!doctype html>
<html lang="en">

<?php
include '../session.php';
include '../class/class.select.php';
$select=new select;
$userID=$_SESSION['userSession'];
$area = $select->user($userID, 'area');
$role = $select->user($userID,'role_role_id');
 if($area == 'PUSAT'){
     $area_login = '%';
 }else{
    $area_login = $area;
 }

?>

<head>
<title>Device Monitoring System</title>
<link rel="shortcut icon" href="../assets/images/favicon.ico" type="image/x-icon"/>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.min.css">

<link rel="stylesheet" href="../assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/sweetalert/sweetalert.css"/>

<!-- MAIN CSS -->
<link rel="stylesheet" href="../theme/assets/css/main.css">
<link rel="stylesheet" href="../theme/assets/css/color_skins.css">

<!-- <link rel="stylesheet" type="text/css" href="../assets/datatables/css/jquery.dataTables.css"> -->
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/datatables/js/jquery.dataTables.js"></script>
<script src="../assets/js/init_notif_main.js" type="text/javascript"></script>

<style>
    td.details-control {
		background: url('../assets/images/detail.png') no-repeat center center;
		cursor: pointer;
	}
    tr.shown td.details-control {
        background: url('../assets/images/close.png') no-repeat center center;
    }
	@-webkit-keyframes blinker {
		from {opacity: 1.0;}
		to {opacity: 0.0;}
	}
	.blink{
		text-decoration: blink;
		-webkit-animation-name: blinker;
		-webkit-animation-duration: 0.6s;
		-webkit-animation-iteration-count:infinite;
		-webkit-animation-timing-function:ease-in-out;
		-webkit-animation-direction: alternate;
	}
</style>
</head>
<body class="theme-dark">


<div id="wrapper">

    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
            </div>

            <div class="navbar-brand">
            </div>
            
            <div class="navbar-right">
                <div id="navbar-menu">
                    <ul class="nav navbar-nav">
                       
                       
                       
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                <i class="icon-bell"></i>
                                <span id="notif-dot" class="notification-dot" style="display:none"></span>
                            </a>
                            <ul class="dropdown-menu notifications">
                                <li class="header"><strong>You have <span id="status_notif"></span> new Notifications</strong></li>
                                <div id="notif-body" style="overflow-x: hidden; height: auto; max-height: 200px;"></div>                          
                                <li class="footer"><a href="../notifikasi_list.php" class="more">See all notifications</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" onClick="logoutx()" class="icon-menu"><i class="icon-login"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div id="left-sidebar" class="sidebar">
        <div class="sidebar-scroll">
            <div class="user-account">
                 <img src="<?php echo '../assets/images/';
                if ($select->user($userID, 'photo') == '') {
                    echo 'f_avatar.png';
                } else {
                    echo $select->user($userID, 'photo');
                }; ?>" class="rounded-circle user-photo">

                <div class="dropdown">
                    <span>Welcome,</span>
                     <a href="" class="user-name"><strong> <?php echo ucwords($role) . ' <br> ' .
                $select->user($userID, 'nama_depan'). ' '.$select->user($userID, 'nama_belakang'). ' '.$select->user($userID, 'area') ; ?></strong></a>
                </div>
            </div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#menu">Menu</a></li>
            </ul>
                
            <!-- Tab panes -->
            <div class="tab-content p-l-0 p-r-0">
                <div class="tab-pane active" id="menu">
                    <nav id="left-sidebar-nav" class="sidebar-nav">
                        <?php
                           if ($select->user($userID, 'role_role_id') == 'SUPERADMIN') {                           
                        ?>  
                        <ul id="main-menu" class="metismenu">                          
                            <li >
                                <a href="../home.php"><i class="icon-home"></i> <span>Dashboard</span></a>
                            </li>
                            <li>
                                <a href="user_list.php"><i class="icon-users"></i> <span>User List</span></a>
                            </li>
                            <li>
                                <a href="list_wilayah2.php"><i class="icon-map"></i> <span>Region List</span></a>
                            </li>
                            <li>
                                <a href="mesin_list.php"><i class="icon-speedometer"></i> <span>Device List</span></a>
                            </li>
                            <li>
                                <a href="det_alat.php"><i class="icon-grid"></i> <span>Device Details</span></a>
                            </li>
                            <li >
                                <a href="notif_list.php"><i class="icon-notebook"></i> <span>Summary Report</span></a>
                            </li>
                            <li class="active">
                                <a href="list_task.php"><i class="icon-briefcase"></i> <span>Task List</span></a>
                            </li>
                            <li>
                                <a href="#" onClick="logoutx()"><i class="icon-power"></i> <span>Logout</span></a>
                            </li>
                        </ul>
                        <?php
                            }else{
                        ?>
                        <ul id="main-menu" class="metismenu">                          
                            <li>
                                <a href="../home.php"><i class="icon-home"></i> <span>Dashboard</span></a>
                            </li>
                            <li class="active">
                                <a href="notif_list.php"><i class="icon-notebook"></i> <span>Summary Report</span></a>
                            </li>
                            <li>
                                <a href="#" onClick="logoutx()"><i class="icon-power"></i> <span>Logout</span></a>
                            </li>
                        </ul>
                        <?php
                            }
                        ?> 
                    </nav>
                </div>              
            </div>          
        </div>
    </div>

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-bars"></i></a>Task List</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../home.php"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item"><a href="../home.php"> Home</a></li>
                            <li class="breadcrumb-item active"><a href="notif_list.php"> Task List</a></li>
                        </ul>
                    </div>            
                </div>
            </div>

            <input type="hidden" id="temp_teknisi">
			<input type="hidden" id="jml_plan">
			<input type="hidden" id="jml_real">
            
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Task List</h2>                       
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="myTable" class="table table-hover">
        
                <thead>
               <th><center>Detail</center></th>
               <th><center>Name</center></th>
               <th><center>Amount Plan Task</center></th>
               <th><center>Amount Realization Task</center></th>
               </thead>

            <tbody>
            <?php
            $no=1;
            $qry = $select->get_list_task($area_login);

            if ($qry === FALSE) {
                die(mysql_error());
            }
            
            while($row=mysql_fetch_array($qry)){
            ?>
            <tr>
                <?php echo '<td class="details-control" onclick="setKode(\''.$row['teknisi_id'].'\', '.$row['plan'].', '.$row['realisasi'].')"></td>'?>
                <td><center><?php echo $row['nama_depan'].' '.$row['nama_belakang']; ?></center></td>
                <td><center><span class="badge badge-warning"><b><?php echo $row['plan']?></b> Task</center></span></td>
                <td><center><span class="badge badge-warning"><b><?php echo $row['realisasi']?></b> Realization</center></span></td>                      
            </tr>

            <?php
            }
            ?>   
            </tbody>       
        </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    
</div>

<div class="modal fade" id="modalAdd" name="modalAdd" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" id="ukuran" role="document">
        <div class="modal-content bg-dark">
            <!-- <div class="panel panel-default"> -->
                <div class="modal-header">
                    <h6 class="panel-title"><b id="cap_add">Caption title</b> 
                        <b id="kode_gi"></b><b id="mx_kode_child"></b><b id="kode_group"></b></h6>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                            onClick="dcook('addols','f')">&times;
                    </button>
                </div>
                <div class="modal-body bg-white">
                   <div class="row clearfix">
                       <div class="col-12">
                          <div class="panel-body content-body">Content body</div>
                       </div>
                  </div>
                  </div>
                <div class="modal-footer">
                <button type="button" name="btnAdd" id="btnModify" class="btn btn-warning" onclick="modify()" style="display: none">Modify</button>
                <button type="button" name="btnAdd" id="btnApprove" class="btn btn-primary" onclick="approval()" style="display: none">Approve</button>
                       <button type="button" name="btnAdd" id="btnAdd" class="btn btn-primary" onClick="simpan()">Save</button>
                       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
    </div>
</div>
</div>


<script>
    function detail_task(id){
        $('#btnAdd, #btnApprove, #btnModify').hide()
        $('#cap_add').html('Detail List Task')
        $('#modalAdd').modal('show')
        $('.content-body').load('../process/detail_task.php?id='+id)
    }

    function logoutx() {
        if (confirm("Apakah anda akan logout?")) {
            window.location = "../logout.php";
        }
    }
</script>

<audio id="status10">
  <source src="../assets/sound/alarm.mp4" type="audio/mp4">
</audio>
<audio id="status11">
  <source src="../assets/sound/train_low.mp3" type="audio/mp3">
</audio>

<!-- Javascript -->
<script src="../module/sse/receive_sse.js"></script>
<script src="../theme/assets/bundles/libscripts.bundle.js"></script>    
<script src="../theme/assets/bundles/vendorscripts.bundle.js"></script>
<script src="../theme/assets/bundles/datatablescripts.bundle.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.print.min.js"></script>
<script src="../assets/vendor/sweetalert/sweetalert.min.js"></script> <!-- SweetAlert Plugin Js --> 
<script src="../theme/assets/bundles/mainscripts.bundle.js"></script>
<script src="../theme/assets/js/pages/tables/jquery-datatable.js"></script>

<script type="text/javascript">

$(document).ready(function() {
	
    var table = $('#myTable').DataTable();

    $('#myTable tbody').on('click', 'td.details-control', function () {
        // console.log( table.row( this ).data() );

        var tr = $(this).closest('tr');
        var row = table.row(tr);

        console.log(row);
        // console.log(data['nivel_puesto']);
            
        // var tr = $(this).closest('tr');
        // var closestTable = tr.closest("table");
        // var row = closestTable.DataTable().row(tr);

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            // row.child( format()).show();
            format(row.child);
            tr.addClass('shown');
        }
    } );

    
function format(callback,id) {
        var id = $('#temp_teknisi').val();
		var plan = $('#jml_plan').val();
		var real = $('#jml_real').val();
        $.ajax({
        type: "POST",
        url:'../process/detail_list_task.php',
        dataType: "json",
        data:{kode:id},
        complete: function (response) {
            var data = response.responseText;
            var temp = '';
            $.each(JSON.parse(data), function(idx, obj) {
                var alamat, status, task = '';
                if (obj.provinsi_name != null && obj.kota_name != null && obj.kecamatan_name != null && obj.desa_name != null &&
					obj.provinsi_name != '' && obj.kota_name != '' && obj.kecamatan_name != '' && obj.desa_name != ''){
                    alamat = obj.provinsi_name+', '+obj.kota_name+', '+obj.kecamatan_name+', '+obj.desa_name;
                }else if(obj.provinsi_name != null && obj.kota_name != null && obj.kecamatan_name != null &&
						 obj.provinsi_name != '' && obj.kota_name != '' && obj.kecamatan_name != ''){
                    alamat = obj.provinsi_name+', '+obj.kota_name+', '+obj.kecamatan_name;
                }else if(obj.provinsi_name != null && obj.kota_name != null && obj.provinsi_name != '' && obj.kota_name != ''){
                    alamat = obj.provinsi_name+', '+obj.kota_name;
                }else if(obj.provinsi_name != null && obj.provinsi_name != ''){
                    alamat = obj.provinsi_name;
                }else{
					alamat = '-';
				}
				
				if(obj.flag_rusak == 'Y'){
					status = '<span class="blink badge badge-danger">Broken</span>';
				}else{
					if(obj.active == 'N' && obj.flag_reg == 'Y'){
						status = '<span class="badge badge-warning">On Progress</span>';
					}else if(obj.active == 'N' && obj.flag_reg == 'N'){
						status = '<span class="badge badge-primary">Waiting For Activition</span>';
					}else if(obj.active == 'Y' && obj.flag_reg == 'N'){
						status = '<span class="badge badge-success">Complete</span>';
						if(obj.teknisi_id == obj.realisasi_teknisi_id){
							task = '<span class="badge badge-success">Own</span>';
						}else{
							if(plan > real){
								task = '<span class="badge badge-warning">Other</span>';
							}else{
								task = '<span class="badge badge-warning">Task Other</span>';
							}
						}
					}else{
						status = '<span class="badge badge-danger">Flag Is Undefined</span>';
					}
				}
				
				var kap = '-';
				var ols = '-';
				var cod = '-';
				
				if(obj.trafo_name != null && obj.trafo_name != ''){
					cod = obj.trafo_name;
				}
				
				if(obj.kapasitas != null){
					kap = obj.kapasitas;
				}
				
				if(obj.set_ols != null){
					ols = obj.set_ols;
				}
				
                temp += '<tr style="background: transparent;"><td>'+cod+'</td>'+
                             '<td>'+kap+'</td>'+
                             '<td>'+ols+'</td>'+
                             '<td>'+alamat+'</td>'+
                             '<td>'+status+'</td>'+
                             '<td>'+task+'</td></tr>';
            });

            callback($('<table width=100%>'+
            '<thead><th>Code</th>'+
            '<th>Capacity</th>'+
            '<th>Set Ols</th>'+
            '<th>Address</th>'+
            '<th>Status</th>'+
            '<th>Task Complete</th></thead>'+
            '<tbody>'+temp+'</tbody></table>')).show();
        },
        error: function () {
            $('#output').html('Bummer: there was an error!');
        }
    });
    }
});

function setKode(id, plan, real){
    $('#temp_teknisi').val(id);
	$('#jml_plan').val(plan);
	$('#jml_real').val(real);
	
}


// function format (d) {

//     var temp = "";
//     var temp2 = "tes";

//     $.ajax({
//          type: "POST",
//          url: "../process/detail_list_task.php",
//          dataType: "json",
//          success: function(data){
//                 setContent(data);

//                 alert('succes');

//                 // temp = '<table style="padding-left:50px;">';

//                 // alert(data.length);
                

//                 // temp = temp + '</table>';
//             }
//         });
        
    
// }

// function setContent(){

//     var temp2 = "";

//     for (var i = 0; i < 5; i++) {
//         temp2 += '<tr><td>sodiq</td></tr>';
//     }

//     return temp;
// }

</script>

</html>
