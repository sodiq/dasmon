﻿<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
</head>
<body>

<!-- MAP[START] -->
<div id="main" style="width: 100%; height: 100%;">
    <div id="myMap" style='width:100%;height:91%;'>
        <input type="text" id="from" style="width: 200px; height: 25px; margin-left: 10px; margin-top: 10px; z-index: 1; position: relative; " placeholder=" Search here...">
        <button id="get" style="width: 50px; height: 25px; margin-left: 1px; margin-top: 10px; z-index: 1; position: relative; ">
            <i class="fa fa-search"></i>
        </button>
    </div>
</div>

<!-- MAP[END] -->

<script src='https://www.bing.com/api/maps/mapcontrol?callback=GetMap&key=AklReKlg6U-7ie54NEcZ3yAuP4WzeZfeMjDWf_jR4ARCbJZXE_bCubuInqzP0E-Q' async defer></script>
<script>
var infobox;
var inter;
let map,searchManager;

function GetMap() {

    map = new Microsoft.Maps.Map('#myMap', {
        zoom: 5,
        mapTypeId: Microsoft.Maps.MapTypeId.canvasDark,
        showMapTypeSelector: false,
        enableClickableLogo: false,
        center: new Microsoft.Maps.Location(-0.789275, 113.921326)
    });

    infobox = new Microsoft.Maps.Infobox(map.getCenter(), {
            visible: false
        });

    setInterval(function(){
    $.ajax({
			type: "POST",
			url: "process/map_data.php",
			dataType: "json",
			success: function(data){
                
                map.entities.clear();

                for (var i = data.length - 1; i >= 0; i--) {

                var icon    =  "assets/images/trf_grey.png";
                var url_img = "assets/images/calloutBlankGrey.png";
                var status  = "HEALTY";

                if (data[i].evt==0 && data[i].dc==1){
                    icon =  "assets/images/trf_green2.png";
                    url_img = "assets/images/calloutBlankGreen.png";
                    status = "HEALTY";
                }else if (data[i].evt==8 && data[i].dc==1){
                    icon ="assets/images/trf_green2.png";
                    url_img = "assets/images/calloutBlankGreen.png";
                    status = "HEALTY";
                }else if (data[i].evt==10 && data[i].dc==1) {
                    icon ="assets/images/trf_yellow2.png";
                    url_img = "assets/images/calloutBlankYellow.png";
                    status = "ALARM";
                }else if (data[i].evt==11 && data[i].dc==1){
                    icon ="assets/images/trf_red2.png";
                    url_img = "assets/images/calloutBlankRed.png";
                    status = "TRIP & ALARM";
                }else if (data[i].evt==26 && data[i].dc==1){
                    icon =test;
                }else if (data[i].evt==0 && data[i].dc==0){
                    icon = "assets/images/disconnect2.png";
                    status = "DISCONNECT";
                }else if (data[i].evt==8 && data[i].dc==0){
                    icon = "assets/images/disconnect2.png";
                    status = "DISCONNECT";
                }else if (data[i].evt==10 && data[i].dc==0){
                    icon ="assets/images/disconnect2.png";
                    status = "DISCONNECT";
                }else if (data[i].evt==11 && data[i].dc==0){
                    icon = "assets/images/disconnect2.png";
                    status = "DISCONNECT";
                }

                var location = new Microsoft.Maps.Location(parseFloat(data[i].latitude), parseFloat(data[i].longitude))
                var pin = new Microsoft.Maps.Pushpin(location,{
                        icon: icon,
                        anchor: new Microsoft.Maps.Point(13,15)
                });
                pin.metadata = {
                    nama: data[i].kota_name,
                    latitude: data[i].latitude,
                    longitude: data[i].longitude,
                    evt: data[i].evt,
                    dc: data[i].dc,
                    currR: (data[i].arusia*data[i].ratio).toFixed(2),
                    currS: (data[i].arusib*data[i].ratio).toFixed(2),
                    currT: (data[i].arusic*data[i].ratio).toFixed(2),
                    voltR: data[i].vola,
                    voltS: data[i].volb,
                    voltT: data[i].volc,
                    tempR: data[i].tempa,
                    tempS: data[i].tempb,
                    tempT: data[i].tempc,
                    freqR: data[i].freqa,
                    freqS: data[i].freqb,
                    freqT: data[i].freqa,
                    kode_mesin: data[i].kode_mesin,
                    url_collut: url_img,
                    nama_trafo: data[i].nama,
                    status: status
                };

                Microsoft.Maps.Events.addHandler(pin, 'click', function(e){
                    startInfoBox(e);
                });
                
                //Microsoft.Maps.Events.addHandler(pin, 'mouseout', function (e) {
                    // $('.infobox').addClass('d-none');
                //    stopInfoBox();
                //});

                //Microsoft.Maps.Events.addHandler(pin, 'click', function (e) {
                //    det_trafo(e);
                //});

                map.entities.push(pin);
                
                }
                
                infobox.setMap(map);
            }
        });
    },1000);
    
}

function det_trafo(e){
    ls_save('kode_trafo', e.target.metadata.kode_mesin);
    ls_save('nama_trafo', e.target.metadata.nama_trafo);
    window.location.reload(); 
}

function startInfoBox(e){
	stopInfoBox();
	infobox.setOptions({
        location: e.target.getLocation(),
        visible: true,
        showCloseButton:false,
        htmlContent:'<img style="width:30px; height:30px;" src="assets/images/loader.gif"></img>'
    });
    var kode_mesin = e.target.metadata.kode_mesin;
    inter = setInterval(function(){
        $.ajax({
			type: "POST",
			url: "process/infobox.php",
            dataType: "json",
            data: {kode : kode_mesin},
			success: function(respone){

                for (var i = respone.length - 1; i >= 0; i--) {

                var url_img = "assets/images/calloutBlankGrey.png";
                var status  = "HEALTY";
				var color 	= 'black';

                var nama  = respone[i].kota_name;
                var currR = (respone[i].arusia*respone[i].ratio).toFixed(2);
                var currS = (respone[i].arusib*respone[i].ratio).toFixed(2);
                var currT = (respone[i].arusic*respone[i].ratio).toFixed(2);
                var voltR = respone[i].vola;
                var voltS = respone[i].volb;
                var voltT = respone[i].volc;
                var tempR = respone[i].tempa;
                var tempS = respone[i].tempb;
                var tempT = respone[i].tempc;
                var freqR = respone[i].freqa;
                var freqS = respone[i].freqb;
                var freqT = respone[i].freqa;
                var kode_mesin = respone[i].kode_mesin;
                var nama_trafo = respone[i].nama;
				
				if(tempR == null){
					tempR = '-';
				}
				
				if(tempS == null){
					tempS = '-';
				}
				
				if(tempT == null){
					tempT = '-';
				}
				
				if(voltR == null){
					voltR = '-';
				}
				
				if(voltS == null){
					voltS = '-';
				}
				
				if(voltT == null){
					voltT = '-';
				}
				
				if(freqR == null){
					freqR = '-';
				}
				
				if(freqS == null){
					freqS = '-';
				}
				
				if(freqT == null){
					freqT = '-';
				}

                if (respone[i].evt==0 && respone[i].dc==1){
                    url_img = "assets/images/calloutBlankGreen.png";
                    status = "HEALTY";
                }else if (respone[i].evt==8 && respone[i].dc==1){
                    url_img = "assets/images/calloutBlankGreen.png";
                    status = "HEALTY";
                }else if (respone[i].evt==10 && respone[i].dc==1) {
                    url_img = "assets/images/calloutBlankYellow.png";
                    status = "ALARM";
                }else if (respone[i].evt==11 && respone[i].dc==1){
                    url_img = "assets/images/calloutBlankRed.png";
                    status = "TRIP & ALARM";
					color 	= 'white';
                }else if (respone[i].evt==26 && respone[i].dc==1){
                    icon =test;
                }else if (respone[i].evt==0 && respone[i].dc==0){
                    status = "DISCONNECT";
                }else if (respone[i].evt==8 && respone[i].dc==0){
                    status = "DISCONNECT";
                }else if (respone[i].evt==10 && respone[i].dc==0){
                    status = "DISCONNECT";
                }else if (respone[i].evt==11 && respone[i].dc==0){
                    status = "DISCONNECT";
                }
                
                infobox.setOptions({
                            location: e.target.getLocation(),
                            visible: true,
                            showCloseButton:false,
                            htmlContent:'\
                            <div class="row clearfix infobox" style="background: transparent; border: transparent;">\
                                    <div class="col-lg-6 col-md-4">\
                                        <div class="dataDetail"><table border="1px;" width="180px;" height="80px;" style="position: absolute; z-index:2; margin-left:10px; top: 10px; color: '+color+'; border-color: '+color+'; font-size:10px;"><tr><td colspan="4"><center>'+ nama_trafo  +'</center></td></tr>\
                                        <tr><td><center>'+status+'</center></td><td><center>R</center></td><td><center>S</center></td><td><center>T</center></td></tr><tr><td><center>Curr (A)<center></td><td><center>'+ currR + '<center></td><td><center>' +currS + '</center></td><td><center>' + currT + '</center></td></tr>\
                                        <tr><td><center>Volt (V)<center></td><td><center>'+ voltR + '<center></td><td><center>' +voltS + '</center></td><td><center>' + voltT + '</center></td></tr><tr><td><center>Temp (C) <center></td><td><center>'+ tempR + '<center></td><td><center>' +tempS + '</center></td><td><center>' + tempT + '</center></td></tr>\
                                        <tr><td><center>Freq (Hz) <center></td><td><center>'+ freqR + '<center></td><td><center>' +freqS + '</center></td><td><center>' + freqT + '</center></td></tr></table><img style="position:relative; z-index:1; border:solid 0px #000; width:200px; height:130px; border-radius: 8px;" src="'+url_img+'" class="callout"></img>\
                                        </div>\
                                    </div>\
                                    <span class=""></span>\
                                    <div class="col-lg-4 col-md-4">\
                                        <button class="btn btn-danger" onClick="$(\'.infobox\').addClass(\'d-none\'); stopInfoBox()"><i class="fa fa-close"></i> Close</button>\
                                        <button class="btn btn-primary" onclick="$(\'.infobox\').addClass(\'d-none\'); window.location.reload(); ls_save(\'kode_trafo\',\''+e.target.metadata.kode_mesin+'\'); ls_save(\'nama_trafo\',\''+e.target.metadata.nama_trafo+'\')" style="margin-top:4px;"><i class="fa fa-home"></i> Detail</button>\
                                    </div>\
                            </div>'
                        });
                    }
                }
            });
     }, 1000);
}
function stopInfoBox(){
    clearInterval(inter);
    infobox.setOptions({
        visible: false
    });
}

function pushpinClicked(e) {
        //Make sure the infobox has metadata to display.
            // var status = "HEALTY";
            // if (e.target.metadata.evt==0 && e.target.metadata.dc==1){
            //     status = "HEALTY";
            // }else if (e.target.metadata.evt==8 && e.target.metadata.dc==1){
            //     status = "HEALTY";
            // }else if (e.target.metadata.evt==10 && e.target.metadata.dc==1){
            //     status = "ALARM";
            // }else if (e.target.metadata.evt==11 && e.target.metadata.dc==1){
            //     status = "TRIP & ALARM"; 
            // }else if (e.target.metadata.evt==0 && e.target.metadata.dc==0){
            //     status = "DISCONNECT";
            // }else if (e.target.metadata.evt==8 && e.target.metadata.dc==0){
            //     status = "DISCONNECT";
            // }else if (e.target.metadata.evt==10 && e.target.metadata.dc==0){
            //     status = "DISCONNECT";
            // }else if (e.target.metadata.evt==11 && e.target.metadata.dc==0){
            //     status = "DISCONNECT";
            // }

        if (e.target.metadata) {
            //Set the infobox options with the metadata of the pushpin.
            infobox.setOptions({
                location: e.target.getLocation(),
                visible: true,
                showCloseButton:false,
                htmlContent:'\
                <div class="row clearfix infobox" style="background: transparent; border: transparent;">\
                        <div class="col-lg-6 col-md-4">\
                            <div class="dataDetail"><table border="1px;" width="180px;" height="80px;" style="position: absolute; z-index:2; margin-left:10px; top: 10px; color: black; border-color: black; font-size:10px;"><tr><td colspan="4"><center>'+ e.target.metadata.nama_trafo  +'</center></td></tr><tr><td><center>'+e.target.metadata.status+'</center></td><td><center>R</center></td><td><center>S</center></td><td><center>T</center></td></tr><tr><td><center>Curr (A)<center></td><td><center>'+ e.target.metadata.currR + '<center></td><td><center>' +e.target.metadata.currS + '</center></td><td><center>' + e.target.metadata.currT + '</center></td></tr><tr><td><center>Volt (V)<center></td><td><center>'+ e.target.metadata.voltR + '<center></td><td><center>' +e.target.metadata.voltS + '</center></td><td><center>' + e.target.metadata.voltT + '</center></td></tr><tr><td><center>Temp (C) <center></td><td><center>'+ e.target.metadata.tempR + '<center></td><td><center>' +e.target.metadata.tempS + '</center></td><td><center>' + e.target.metadata.tempT + '</center></td></tr><tr><td><center>Freq (Hz) <center></td><td><center>'+ e.target.metadata.freqR + '<center></td><td><center>' +e.target.metadata.freqS + '</center></td><td><center>' + e.target.metadata.freqT + '</center></td></tr></table><img style="position:relative; z-index:1; border:solid 0px #000; width:200px; height:130px; border-radius: 8px;" src="'+e.target.metadata.url_collut+'" class="callout"></img>\
                            </div>\
                        </div>\
                        <span class=""></span>\
                        <div class="col-lg-4 col-md-4">\
                            <button class="btn btn-danger" onClick="$(\'.infobox\').addClass(\'d-none\')"><i class="fa fa-close"></i> Close</button>\
                            <button class="btn btn-primary" onclick="$(\'.infobox\').addClass(\'d-none\'); window.location.reload(); ls_save(\'kode_trafo\',\''+e.target.metadata.kode_mesin+'\'); ls_save(\'nama_trafo\',\''+e.target.metadata.nama_trafo+'\')" style="margin-top:4px;"><i class="fa fa-home"></i> Detail</button>\
                        </div>\
                </div>'
            });
        }
    }
//Geocode：Location

function geocodeQuery(query) {
    //If search manager is not defined, load the search module.
    if (!searchManager) {
        //Create an instance of the search manager and call the geocodeQuery function again.
        //map.entities.clear();
        Microsoft.Maps.loadModule('Microsoft.Maps.Search', function () {
            searchManager = new Microsoft.Maps.Search.SearchManager(map);
            geocodeQuery(query);
        });
    } else {
        var searchRequest = {
            where: query,
            callback: function (r) {
                //Add the first result to the map and zoom into it.
                if (r && r.results && r.results.length > 0) {
                    var pin = new Microsoft.Maps.Pushpin(r.results[0].location);
                    map.entities.push(pin);
                    map.setView({ bounds: r.results[0].bestView });
                }
            },
            errorCallback: function (e) {
                //If there is an error, alert the user about it.
                alert("No results found.");
            }
        };
        //Make the geocode request.
        searchManager.geocode(searchRequest);
    }
}
//SearchButton
document.getElementById("get").onclick = function(){
    geocodeQuery(document.getElementById("from").value);
};

var input = document.getElementById("from");
input.addEventListener("keyup", function(event) {
  if (event.keyCode === 13) {
   event.preventDefault();
   document.getElementById("get").click();
  }
});

</script>
</body>
</html>