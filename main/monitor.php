<html lang="en">
<head>
<meta content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" name="viewport"/>
<link href="http://localhost/dasmon/css/bootstrap.css" rel="stylesheet">
<script src="http://localhost/dasmon/js/jquery.min.js"></script>
</head>
 

<?php
session_start();
error_reporting(0);

include '../class/class.select.php';
if(!empty($_REQUEST['kode'])){
  $kode=$_REQUEST['kode'];
}else{
  $kode='008';
  $kode_gi='008';
}
$select = new select;
$jenismesin =$select->get_jenismesin($kode);
$namamesin= $select->get_namamesin('001');
$id = $select->get_jenismesin('id');
?>

<?php
 $id = $_GET['id'];
?>

<body>

<div class="container-fluid">
<div class="card-header" style="border-bottom:solid 1px #ededed; padding-bottom:2px; margin-bottom:5px; padding-top: 10px">
         <span style="font-size:13pt; font-family: Arial,Helvetica,sans-serif; color: #222222"><b><center>PORT MONITOR</center></b></span>
        <span class="pull-right"></span><br/><br/>
</div>
<br/>

 
          <script type="text/javascript">
              var auto_refresh = setInterval(
              function () {
                 $('#load_content').load('monitor_ir.php?id=<?php echo $id; ?>').fadeIn("slow");
                 $('#load_content2').load('monitor_is.php?id=<?php echo $id; ?>').fadeIn("slow");
                 $('#load_content3').load('monitor_it.php?id=<?php echo $id; ?>').fadeIn("slow");
                 $('#load_content4').load('monitor_vr.php?id=<?php echo $id; ?>').fadeIn("slow");
                 $('#load_content5').load('monitor_vs.php?id=<?php echo $id; ?>').fadeIn("slow");
                 $('#load_content6').load('monitor_vt.php?id=<?php echo $id; ?>').fadeIn("slow");
                 $('#load_content7').load('monitor_tr.php?id=<?php echo $id; ?>').fadeIn("slow");
                 $('#load_content8').load('monitor_ts.php?id=<?php echo $id; ?>').fadeIn("slow");
                 $('#load_content9').load('monitor_tt.php?id=<?php echo $id; ?>').fadeIn("slow");
                 $('#load_content10').load('monitor_tr_f.php?id=<?php echo $id; ?>').fadeIn("slow");
                 $('#load_content11').load('monitor_ts_f.php?id=<?php echo $id; ?>').fadeIn("slow");
                 $('#load_content12').load('monitor_tt_f.php?id=<?php echo $id; ?>').fadeIn("slow");
              }, 20000); // refresh setiap 10000 milliseconds
          </script>


          <div class="row">
          <div class="col-md-4">
          <div class=" panel panel-primary">
            <div class="panel-heading"><center><h4>Current</h4></center></div>
            <div class="panel-body"></div>
            <div id="load_content" style="font-size: 16px; font-family: Arial, Helvetica, sans-serif;"></div>
            <div id="load_content2" style="padding-top: 8px; font-size: 16px; font-family: Arial, Helvetica, sans-serif;"></div>
            <div id="load_content3" style="padding-top: 8px; padding-bottom: 25px; font-size: 16px; font-family: Arial, Helvetica, sans-serif;"></div>
          </div>
          </div>
          
        
          <div class="col-md-4">
          <div class=" panel panel-primary">
            <div class="panel-heading"><center><h4>Voltage</h4></center></div>
            <div class="panel-body"></div>
            <div id="load_content4" style="font-size: 16px; font-family: Arial, Helvetica, sans-serif;"></div>
            <div id="load_content5" style="padding-top: 8px; font-size: 16px; font-family: Arial, Helvetica, sans-serif;"></div>
            <div id="load_content6" style="padding-top: 8px; padding-bottom: 25px; font-size: 16px; font-family: Arial, Helvetica, sans-serif;"></div>
          </div>
          </div>
         

          <div class="col-md-4">
          <div class=" panel panel-primary">
            <div class="panel-heading"><center><h4>Temperature</h4></center></div>
            <div class="panel-body"></div>
            <div id="load_content7" style="font-size: 16px; font-family: Arial, Helvetica, sans-serif;"></div>
            <div id="load_content8" style="padding-top: 8px; font-size: 16px; font-family: Arial, Helvetica, sans-serif;"></div>
            <div id="load_content9" style="padding-top: 8px; font-size: 16px; font-family: Arial, Helvetica, sans-serif;"></div>
            <hr>
            <div id="load_content10" style="font-size: 16px; font-family: Arial, Helvetica, sans-serif;"></div>
            <div id="load_content11" style="padding-top: 8px; font-size: 16px; font-family: Arial, Helvetica, sans-serif;"></div>
            <div id="load_content12" style="padding-top: 8px; padding-bottom: 25px; font-size: 16px; font-family: Arial, Helvetica, sans-serif;"></div>
          </div>
          </div>
       

        </div> <!-- tutup row -->
 </center>
</body>
</html>