<!doctype html>
<html lang="en">

<?php
error_reporting(0);
include '../session.php';
include '../class/class.select.php';
$select=new select;
$userID=$_SESSION['userSession'];
$max_kode=$select->zf(($select->max_kode()+1),4);
$role = $select->user($userID,'role_role_id');

?>

<head>
<title>   </title>
<link rel="shortcut icon" href="../assets/images/favicon.ico" type="image/x-icon"/>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.min.css">

<link rel="stylesheet" href="../assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/sweetalert/sweetalert.css"/>

<!-- MAIN CSS -->
<link rel="stylesheet" href="../theme/assets/css/main.css">
<link rel="stylesheet" href="../theme/assets/css/color_skins.css">
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/datatables/js/jquery.dataTables.js"></script>
<script src="../assets/js/init_notif_main.js" type="text/javascript"></script>
<script src="../assets/js/pdfmake.min.js" type="text/javascript"></script>
<script src="../assets/js/vfs_fonts.js" type="text/javascript"></script>
<script src="../assets/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="../assets/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="../assets/js/jszip.min.js" type="text/javascript"></script>
<script src ="../module/mqtt/widget.js" type = "text/javascript"></script>


<script type="text/javascript">
var kodetf = ls_read('kode_trafo');
var nama_trafo = ls_read('nama_trafo');
//alert(kodetf);
document.cookie = "kodeterafo="+kodetf;
document.cookie = "namaterafo="+nama_trafo;
var x = document.cookie;
//alert(x);

$(document).ready(function() {
    $('#datatable').DataTable({
        dom: 'Bfrtip',
       buttons: [
            {
                extend: 'pdfHtml5',
                messageTop: nama_trafo+', '+kodetf,  
                customize: function ( doc ) {
                    doc.content.splice( 1, 0, {
                        margin: [ 0, 0, 0, 12 ],
                        alignment: 'center',
                        image: 'data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAggAAAAoCAYAAACbzwJ8AAAKN2lDQ1BzUkdCIElFQzYxOTY2LTIuMQAAeJydlndUU9kWh8+9N71QkhCKlNBraFICSA29SJEuKjEJEErAkAAiNkRUcERRkaYIMijggKNDkbEiioUBUbHrBBlE1HFwFBuWSWStGd+8ee/Nm98f935rn73P3Wfvfda6AJD8gwXCTFgJgAyhWBTh58WIjYtnYAcBDPAAA2wA4HCzs0IW+EYCmQJ82IxsmRP4F726DiD5+yrTP4zBAP+flLlZIjEAUJiM5/L42VwZF8k4PVecJbdPyZi2NE3OMErOIlmCMlaTc/IsW3z2mWUPOfMyhDwZy3PO4mXw5Nwn4405Er6MkWAZF+cI+LkyviZjg3RJhkDGb+SxGXxONgAoktwu5nNTZGwtY5IoMoIt43kA4EjJX/DSL1jMzxPLD8XOzFouEiSniBkmXFOGjZMTi+HPz03ni8XMMA43jSPiMdiZGVkc4XIAZs/8WRR5bRmyIjvYODk4MG0tbb4o1H9d/JuS93aWXoR/7hlEH/jD9ld+mQ0AsKZltdn6h21pFQBd6wFQu/2HzWAvAIqyvnUOfXEeunxeUsTiLGcrq9zcXEsBn2spL+jv+p8Of0NffM9Svt3v5WF485M4knQxQ143bmZ6pkTEyM7icPkM5p+H+B8H/nUeFhH8JL6IL5RFRMumTCBMlrVbyBOIBZlChkD4n5r4D8P+pNm5lona+BHQllgCpSEaQH4eACgqESAJe2Qr0O99C8ZHA/nNi9GZmJ37z4L+fVe4TP7IFiR/jmNHRDK4ElHO7Jr8WgI0IABFQAPqQBvoAxPABLbAEbgAD+ADAkEoiARxYDHgghSQAUQgFxSAtaAYlIKtYCeoBnWgETSDNnAYdIFj4DQ4By6By2AE3AFSMA6egCnwCsxAEISFyBAVUod0IEPIHLKFWJAb5AMFQxFQHJQIJUNCSAIVQOugUqgcqobqoWboW+godBq6AA1Dt6BRaBL6FXoHIzAJpsFasBFsBbNgTzgIjoQXwcnwMjgfLoK3wJVwA3wQ7oRPw5fgEVgKP4GnEYAQETqiizARFsJGQpF4JAkRIauQEqQCaUDakB6kH7mKSJGnyFsUBkVFMVBMlAvKHxWF4qKWoVahNqOqUQdQnag+1FXUKGoK9RFNRmuizdHO6AB0LDoZnYsuRlegm9Ad6LPoEfQ4+hUGg6FjjDGOGH9MHCYVswKzGbMb0445hRnGjGGmsVisOtYc64oNxXKwYmwxtgp7EHsSewU7jn2DI+J0cLY4X1w8TogrxFXgWnAncFdwE7gZvBLeEO+MD8Xz8MvxZfhGfA9+CD+OnyEoE4wJroRIQiphLaGS0EY4S7hLeEEkEvWITsRwooC4hlhJPEQ8TxwlviVRSGYkNimBJCFtIe0nnSLdIr0gk8lGZA9yPFlM3kJuJp8h3ye/UaAqWCoEKPAUVivUKHQqXFF4pohXNFT0VFysmK9YoXhEcUjxqRJeyUiJrcRRWqVUo3RU6YbStDJV2UY5VDlDebNyi/IF5UcULMWI4kPhUYoo+yhnKGNUhKpPZVO51HXURupZ6jgNQzOmBdBSaaW0b2iDtCkVioqdSrRKnkqNynEVKR2hG9ED6On0Mvph+nX6O1UtVU9Vvuom1TbVK6qv1eaoeajx1UrU2tVG1N6pM9R91NPUt6l3qd/TQGmYaYRr5Grs0Tir8XQObY7LHO6ckjmH59zWhDXNNCM0V2ju0xzQnNbS1vLTytKq0jqj9VSbru2hnaq9Q/uE9qQOVcdNR6CzQ+ekzmOGCsOTkc6oZPQxpnQ1df11Jbr1uoO6M3rGelF6hXrtevf0Cfos/ST9Hfq9+lMGOgYhBgUGrQa3DfGGLMMUw12G/YavjYyNYow2GHUZPTJWMw4wzjduNb5rQjZxN1lm0mByzRRjyjJNM91tetkMNrM3SzGrMRsyh80dzAXmu82HLdAWThZCiwaLG0wS05OZw2xljlrSLYMtCy27LJ9ZGVjFW22z6rf6aG1vnW7daH3HhmITaFNo02Pzq62ZLde2xvbaXPJc37mr53bPfW5nbse322N3055qH2K/wb7X/oODo4PIoc1h0tHAMdGx1vEGi8YKY21mnXdCO3k5rXY65vTW2cFZ7HzY+RcXpkuaS4vLo3nG8/jzGueNueq5clzrXaVuDLdEt71uUnddd457g/sDD30PnkeTx4SnqWeq50HPZ17WXiKvDq/XbGf2SvYpb8Tbz7vEe9CH4hPlU+1z31fPN9m31XfKz95vhd8pf7R/kP82/xsBWgHcgOaAqUDHwJWBfUGkoAVB1UEPgs2CRcE9IXBIYMj2kLvzDecL53eFgtCA0O2h98KMw5aFfR+OCQ8Lrwl/GGETURDRv4C6YMmClgWvIr0iyyLvRJlESaJ6oxWjE6Kbo1/HeMeUx0hjrWJXxl6K04gTxHXHY+Oj45vipxf6LNy5cDzBPqE44foi40V5iy4s1licvvj4EsUlnCVHEtGJMYktie85oZwGzvTSgKW1S6e4bO4u7hOeB28Hb5Lvyi/nTyS5JpUnPUp2Td6ePJninlKR8lTAFlQLnqf6p9alvk4LTduf9ik9Jr09A5eRmHFUSBGmCfsytTPzMoezzLOKs6TLnJftXDYlChI1ZUPZi7K7xTTZz9SAxESyXjKa45ZTk/MmNzr3SJ5ynjBvYLnZ8k3LJ/J9879egVrBXdFboFuwtmB0pefK+lXQqqWrelfrry5aPb7Gb82BtYS1aWt/KLQuLC98uS5mXU+RVtGaorH1futbixWKRcU3NrhsqNuI2ijYOLhp7qaqTR9LeCUXS61LK0rfb+ZuvviVzVeVX33akrRlsMyhbM9WzFbh1uvb3LcdKFcuzy8f2x6yvXMHY0fJjpc7l+y8UGFXUbeLsEuyS1oZXNldZVC1tep9dUr1SI1XTXutZu2m2te7ebuv7PHY01anVVda926vYO/Ner/6zgajhop9mH05+x42Rjf2f836urlJo6m06cN+4X7pgYgDfc2Ozc0tmi1lrXCrpHXyYMLBy994f9Pdxmyrb6e3lx4ChySHHn+b+O31w0GHe4+wjrR9Z/hdbQe1o6QT6lzeOdWV0iXtjusePhp4tLfHpafje8vv9x/TPVZzXOV42QnCiaITn07mn5w+lXXq6enk02O9S3rvnIk9c60vvG/wbNDZ8+d8z53p9+w/ed71/LELzheOXmRd7LrkcKlzwH6g4wf7HzoGHQY7hxyHui87Xe4Znjd84or7ldNXva+euxZw7dLI/JHh61HXb95IuCG9ybv56Fb6ree3c27P3FlzF3235J7SvYr7mvcbfjT9sV3qID0+6j068GDBgztj3LEnP2X/9H686CH5YcWEzkTzI9tHxyZ9Jy8/Xvh4/EnWk5mnxT8r/1z7zOTZd794/DIwFTs1/lz0/NOvm1+ov9j/0u5l73TY9P1XGa9mXpe8UX9z4C3rbf+7mHcTM7nvse8rP5h+6PkY9PHup4xPn34D94Tz+49wZioAAAAJcEhZcwAALiMAAC4jAXilP3YAACAASURBVHic7V0HfFRV9j73vZnJTCoQSqgRVgWFSJIpieiKba1gF7CBHdtfwYINLIgNFFGxLjbsq4uu64qubVl1TWYmoUlRegkgLZA+mZl3/9958yYOKTQDrO79fr/kvXff7fPeO9+55RyblJIUFBQUFBQUFBJhO9AVUFBQUFBQUPjvgyIICgoKCgoKCk2gCIKCgoKCgoJCE+xAEGSNqBdE9tYsQBLViGSZ0pp5Kij81uDL9/1J6HKIIHEwXgqdhFyO4LeLAoHPDnTd/hugaZrw5Xtuw/fisMRwfI/qcVgTFfLtQCCw7ABVr1lwnb35+TeRlD8Wl5TMPND1UVBobRyIEYT9siqSF1/Wb91KNe9/SNGvZpGxfDVJu0YiN4dc115Fqf36khCicZpZCDt2f9RP4X8HBV7veE0X4yDutuAp+wxSz05SnIfjiEKP7/6ioP++A13HAw23290Lh0etN3IhPhIGzh0474y/NDCqu5hk+Uv93x3Aau4Ab05OTyG0J6QgJnt/OND1UVBobfzuphiYGETDYdo+/Q0KfTmLkgu85Lj8EhI2G0UWL6Xwm+9S7bBLSZ/5Abl6dCex6ywVFPYaOTk5jpQk5+38oNWF63PnzJmzlsM9Hk+hjbRvIFyux+V9HNarVy9nZmamBk25Jp4eWqoG4ZmGsO183bt37yRd1x0LFy6sxC29f//+mXPnzt1qGEaE73u93mScp8yePXszjg1kHOHtkcdmPs/Pz29TXV1t/PjjjxXxMvLy8jKrqqoqEBZq3IbDDz88Lc1mayOi0ZqiBQu2JN7j+iQnJ9tQXjXq0nbbtm11q1atqs3OznZ17NhRJLaFUdi3b6Z/0aKtiXVj4L11kEXYK6qrCrl98fzbpmX8Df13sqbLWxHUQBCsvukUiUSiKH9jo35PraioiHJdEM2GurWz2WzlqE+4ud+J6+5wOPSSkpItjevG/YXfjftKoJ/aoSyOY6C+5mirRWQUFH532GOCUP5KOm15Lh38nij1+FrKmrhl14n2E5gchDZuoi3PvEApJxxLnS4bQULTfiEBxx9LkYuGUPnQi6n2rXfJOeZm876Cwr7CggULDJ/bw0LX4bQ7boagfoaHyoPBYFGBxzNYGlo0HrdDZvs5eFYzINC6Nwh8t3uqIHEVCIUbaeZBWH6OB7of0o5DvuMRpZ0v37OtwOsdTVIM0IlG6JruwL2fkOZ8TuPz+IbpQrxd6PGNR9oTHLrtKEd6hizw+D4QQr6LuI8jn25t0zNqEefBoqD/QbNsr/doaO5PpKekuolir1Gh17cSonGUv8T/N75Gmm9JUheUP93lSLrd1bFTPcq7NatjpxuQwAXS03v58uV1ZlqP5xhKTvna53aPxuVTu9N/TFgKvd6fUPzJaJ/Nqpcdb+14tPsaXLbRbXau12J8AUbFp2xSnK4FKUku7pepaN/DCMrEXyXaN9VfGhyH/o3G2uMdgbzHoe7mCADibkScpxDnUf4NfHm+PIfNFkD4NLCGo9Cmfjhfif6/Q9gdL1rV7Iryq1H+TSh/2q94XBQU/quwZwQBb8j2D1JI8koFvKHtrq7YR9XaczA5qPrxJ6r0B6jjnbeQzeki1kgajxDYMtIp6f6xFHryeSI1fqCwj2EKGY/vek0QC5PRELijIUzK8Lh+K4T4a2BO8IN4XBEbprbl5uam4rgtFiYO5jCbED1wnIeA7ji2FaRdLaVxFZ7xAcjnFsR7BeFfGVIMQT7X4NE/xSY0JhBnCTK6my+soLvxptxuGGIKePHziHMOAk9APmMkaQ7U8WnEmQCN+dXS0tIyXYoZsYrJcygSWWjo9gLEmY60L4DEfGRq2pIORpo2iDQE53dKIbMhyNegDksQPqhjZua5yOFNzgZljIp9OeiHnfVZenLqOAj2ENrEmnlv5D0IL3g96m2SCtRrIvIehc/RuyIaeUAK0VloOtov/g7yUAgCVopo3bivkMfN0ohebmas6RPR5jtBuvjD9QiE/MVCaK/ifBn64FyUUYE4YxFnAuK0Q/gtQov2QongXTQSdX8fDX6Lf6ewYXzr0MUtKPPPuLc1ShJEg75thUdGQeG/BntEEGrnJFF4dSxJ2ok15Di42dG6XSISCVNNTR2lp6ftVfrG4NG+ymXLKLJhA2VdfCEJjYlB88Kf1x2kuvOpxuVssgZBQWFfwB/0vw6h/3WS3T4Mz+XxCCrEozcUT+NQaMFFEGonNB6K3yWEfLQ4EJyBtN9Det3CQVEhrw4EA8sg+BCknQKR3L1RqiJouDxaQAUe34Wow9kQeC8VB4OmJgziMhaHTrqud8OxDOL/Vo2iC/2ls2eDNByq8QJmCGq8OJ3QngxiEmO9aILkrUXBQAPZQb0qdBbsJK7E5Zu47qwLMRjnC/wlJV/vonUXCSkykGlscbOkV2Qk/Kh/zpwfzSmH9IyRCDXCRuSWcDjMUy+rkp3OZ1CVh0EeLsF1abyXILgvDZSUFJvt83hQX20Wgq/C5SMgBzxlQUaUrvWXBj/nc7fbPdeu6euQ1/V5eXn3OOIjjJI2Ic4F8ZEdsw9zc78RdnN2oRa/3z9285dTUPjNYI8IQsXfrM0IPHowcu9HDzQI8Pff/4yGDz8LqtGvWwYh8RWrWreeIlWV1G7gMbsl9IVuI3FIT1L8QGFfgwVam7S0G/Gcf1YcCDyGoMd47YDP7T4VT+K7EIKFkOZnk6VlM6LRaMKTKW3NjXRFiVbzsa6uLpTCo2XAxo0b1/ER70B9LJZoNH8mV8fP8Oybaw2Q87qECGaYZhh6LI4Rwssyxef25OOyEkLSj0BzaB4kYocXFxr1DqMCEJhfF3p8JShgIITuIXahD8W5DZr6U43n+Bujoqaqz+LFi2tBnsYizb0IGiSF42m+53K5OvCBzx26bakjsRoQ4gbR/MS8qqurFzbUUcpFdmG2uasV1JP/RUV0QTxOSUnJJtR7IyJ1QRs70y+WZhcnkgMFhf8F7LZ0NmoEVf0z2TxPO7mGHH/Yu9EDBj6Q1L1HV3rhhQ/p2mvPMa/3BvydqauooPptFdS2f//dHhHgaFp2d+Lx0ZZGGhQUWgPp6em5eMYm4kW7MgeYP39+vTX//TE09h9xzDOkSLWiswCyQTCxdl4eCxJdm8tXStlEWKGsaHNxf4Fokgb5NJumwO0eIDT9HbwiS2WkPq8Y2jvP/eskTM1Ar6nZQciD1NQ2zgOv5+PQBd4COWCNfQj+thhCvLHzOlppY8L4vkKv91C8sBdoNuLpg9yKiorNzsz2vKbBXlFd1TG+mNGsc35+rhYKrUnMJzk5uS8ORXyO70y/WKPJiiNXoU9yEN6bLKKEn6gdCFcHHikJhUIbkh2O/lZWOxvhUR8Rhd8lWiQIUxcOp5eXnEfLK3tQu6TtNGr1S3Rqzddg2xqd0XUarXunE+VlLqSBWUVUF02iz9cdTUu296RuKevphsNfp+EHz9hJsYKOP85L946fTr6CPuRxN91yuDuIhENUuWoNdcjpa45K7B4kDzuQaNPWXLegphkU9iWgkZZAA5+N07yUJNdHBR6PtbBNOxuPXh5OtopQ7d+s6LzPv69D1x/y5fteAW/mrZCHHJCKC5EZO5IR0fVKt9vdwSb0u3HtNMOdTta+G1YoQ8g2GRUIzg6+h7Y/wov3kJ8Dr97DgeCeTaXU1tdf73Ik/RGn3aBGPLx8+fKrQKx4IeANacmpzxbk5NxWh7KdDscoodvGGC7bdbj3Qjy9TWivoc/v5HOdtEdZlEshX+JrVHgyLl/RpXgGRGS0NIzKFKeTRyzsqO9UkLkqECWy4jZpn2G3V+ix00yQl5M4HX7umeYOBwWF3wFaJAgz1w6knLY/0dCe/6BlIAlZr8V2Ea0o6EGDjv6KXHodfVZ2DI0J3EF2LUIny1l0feZ0WpPZhb5aV7hLgsC7B66+/DS6a+w0+uyTyXskqFmwG/grX7KMMvv13gNyEHvLQ+Vbyda9mxo9UNjnYE0YwvVkCNfJeMTPFaSdbN6Ahiql+BT6+23++fM3cFCU5M3Q0Kfj/bhA03mNgnw3JsTEzchoQywd77mXXfT6+jK+dDqdrEGzoK5dsGBBOJYPrYLgiuIVWRErSlthvl7WdSwbuZKff76XUN3lCMiKClFWWlrq9+Z73kS6C+yazmVVIdUr0rRVIG6nmNYdREarkE22ruvlzbW90ON7Ei83r3sI10Xqn22pn8Lh8AabIwllUOXixYur4+Fz584tt3YafCJkzEZJdV3tLdDya1C3keR0XRybYKHtaNPEYGnJSw2ZSnOh5wvo8+nWeoYQ4jweKCmZxLeLA4FXC7xeO9pzH/L/VGimuOf+nIjfYmys7/Q1JqcQaGcjBAKB9QUe399xbzB+N949UY7fmkcs1rfUTgWF3xJaJAgfnjjSFPyM8BobrSzrTALvz4l3fk+nZn9jTjlct+UNWr6iG8liItemOsqesYEcPcMUMfRdFswfpyFDjiePtze+H02nJHem3fO9bWvWUnp2Nr5Te7iGwZC0aNlaysnrp0YPFPYLeF4bh0tycnKuSLHZzCmD9Vu3buA9+onxIHD+CU28G4RMF1xuZ9sH1l5/Fmqm0Fm/6edB7du3Ty2ZN2+TlSacn59/MNs1iM/tI2w+NOtu2+rrTaHtL/G/n5ub233evHkN6w2Q39gjjjjimbhdBjPvjT+fZuYdqy/jYtT5RtQ5ozoSKePpEQ5EfZ5CHDOv6lDtUTg4cK+lkQHLMqt8P7GsxmAi0L9//x51dXXRxhp4USDwFcrsXltba26XtOpxO7rmLrS9G6ILzru5NQJFQf/k7Ozs57LatOlsOBwbGi8GBUn4M/KZ5u3Xr1vYZtMb5+Mv9fu9Xm/XhD7ZAcVB/xmoQ1d7NJpUYxhr432koPB7QIvStTKcSv8sO5qWVhxER/09QN3kOnLmhKjqaxfVfOc0dzTIsDBHEngWc23vzvRR9cnUZfnPdFLXbygzaVuLhYbq6+nNtz+lmpp65uZUXLyQLh0x2BwJ4G9cILgIH4ylVFsbMdchp6Y46PLLziCmFWXrNtF//vMDrViwhFyZGeaCQ5fLQYf+oRsNGJBDNhuvKpY065s59MMPy8jr4SmMw3hRVcyIEv7W/lxOeXa7eb29ooqqq+vI4dCpfSZP/e6a3Cgo7A0s4bFiZ3Es4bQ64ZqFZYNGapGKHYgFtP0mL1uxNSoRR2PhbK2D2CGsubxR5604bE0Mi5MD635V47LZsFJ6cup4KSTvJ7qCeA1jRDzUpLGNwCShpXvNCWirDU00+/htnh7hE6tdy1vK2yJWa1q6zyMFLd1j8JbQnd1XUPitokWCcOG/ppgEob1jKx3z2fdmGJMC/uOR+bLsLHrpoKG0tk1nevHDO2hSr5FUMi+Hyusz6ITO/6GZJ1/WbL5SGvT8CzPo5rtf5X3RHEADPL3osksHUXl5JZ095B76tmgJ8wY6dkAfeu2lMdQ5qwMPQdItY56lP7/2BUUiBo0ZNZhGjjiNOnQ9lwzhIDaH9NyUq+mKywbRe+9/RcOvfoIiUXMQlfKP6Eb3j7uUjh2YT09PfZuGDDmJqmtqqGJ7BR05cBStWVdOfxp4OL0+fRx1yGy3b3paQeF/BCDjPGpwMN4+3i75Ld75Kf7ZwZ3aPmht4LPyLg6b92eZCgq/N7RIEK4/bDq98scxlDG3gsqqOphh9u4RSju9htJPq6ZDeqyh9uXbac7TfUhvH6Vnb7+XDmqzlrbXp1HRprwWC9xeWUUTp8zguU+KL/4dftFJJIRGjzz2Fn1bvBTkgMV9lB584DLq1jXLjDP1+Rn0/MufQyXQqGtWGxp7x2W0YlWZ6ffG3IWNO9k9OtLKVevpmpueISNqmHsUmIQE562lwedPIFeSRsMvPJ66detAg84YQ9/7l1BVyDB3gwVmL6NzQU7+/eXU1utdBYX/QVijDoMPZB2Kg/6LD2T5Cgq/B7RIEAb3+Mo8ln3VgTKGVFP66VXkPKK+YUMPC99+bX+itmXl5DynnjLbmKbiKcNRSSd3/XeLBT773Ee0YaO1Mwk0P8Ul6PzzjjOH+7/7dj4JKc2FhFJIatc2w9xxwNdffllqHlnkF3oPJXuSTg8/+pYp3DVh0MjLT6Djj82niy99iM496yg66QQP/eOT7+mdGd9RJBqrb3pGCo27+xKy6Tb65KPHaMSVE+it92ZRl07taMGc1yglxdUafaqgoKCgoPCbR4sEYVzpaHrlp/NpfVYHSkuqpuM2f0/HLSqmRdv+QH9ZcTrVRJw0IKOUnp17Nz1y2nX0+V+PNrdE9kgto9tzXqSrer/TbL6bNm2lM0/zxowRQGwfP7A/tclIM88nPXoNvfHOl4hTQVFD0oQJ08nj7kU3XD+M7hs7gnr1+pjWb9hOToeNHn5kOoUjBt0++mwadGoBFfj6UlHRD9QzuxM9OGEkaZpO553zRxp94zK69Y7nacWqn+nVabdSp468ewukQtfolWljEWYuVjZHMNSixf8NWCZ2j5PmHJf5KPKmmK1CyEB1Xd2HrbnQLOYS2PMEnqxt+9Jro+V6+FZB2mHgwtVRkg8lzp0XeL23gnXzCvvthpCTcW/1TrLbI+Tl5aU4dPtUIuP7uFVG9uOAzj16W0XFE805gDoQ4AWfnjzPOZpGZ0DZYNPVrHAsikj5GvvGaK1yCnJyskSS61qKhl8tmj17p2tOFBT+m9EiQZi3tQ9d+IePqE/GMvpxey96Y9mZ9O8NPhqY5acH3Y+RQwvTyq+70tddjqS3qs6gYRDet7d/gX7a3pMWbju4xQInT/o/2nFLsbAEs6ABR/Y3/6RpvYz/NPPI9/Pye9Phh2wjR3JfCPfUhrTmfyHMNF9+HaB777ncJAcM3uGQm9ubPv14EtWHfqYkV5ZJBBoav6MxOKtchd878Aw8h0MqHhse9uJFgSnC3N8vTCc/BUcccWrxvHktLlrbE/Tt29fO2xvxdPE+4ftaI8/mkJub2wXtmhi/1sxtiWTu/y/Mz+8rdNuk+OifLgUTh0dbq2xHNNpO2OznSKnxEJxJEPAGjkd/Htc2Pd2Py69aq6xfA5/b8yoObIp5Db4YX6N+Gfi8XG4T2kifx3elP+h/uVUKcjp5euMeqdvZstxtrZKngsIBwE62OV5jDufH8ai36fdk8ydtSBst6aqj3yNd/GKQTe7EvkCcDOwMO8YRVp7INbSKRJpnByFv3oNgX7pkGQ0+fQDezaQd73EboptBFurVCIFCHOZDYkTpdH+p/zvWvt1ud2+dBBvgOUo4nGz2+JjEBGxFEIfO9fX1lYmr7Ztz0cxg98Hz58/fZhhGfU5OzmFI18T0KLuCttvtvKVxW3M7EeJxnE5nVjQarWgpDsMeiWgUI7y8FdAG2n0WWQRBatqZ1pPPpCGVdxc0U067FMMQjd05x+9Z6wpMN8qojzPuOprBZCo/Pz97zpw5iTsa7I2ODUhwLx2Ku5xOvAeykx5vK7uHjiYnR+Lurhv3DaKzm+atvN2TzVojOKlxngzU7yCHbruEbSOEouHD2D01h/vcvvM0Qe/h71pcvty4/Dh4Z4au66H46BKby0b57XFa38zui1ibhWzWDbTVppSSkpKyuFfJOBLdcnO/22y2ULyu8TKRplxtp1TYH2iRIAjatTYdXqtT5xu3NJH3u5N2T6Gxohf5GXk3tw1RUM+e2fg+2mjHyhgUjVRSuH41OVObEgsFBYa1zW0xhMjFECLL8Qj90ePxHBYMBhfhg50BjfsJXYhhxK4AHEmy0Ov146t+HXsN7NiuPTsCageicEjcrTEPryPeLK/bfTs+6E+lJLmWpzhdPIJgmvpl8uByOJ5A2FBcsmVC5OmbQ+zwKBAwtW0WDslJrqcQh70hOnXdxuV+Z5U7v9mGxMBb+vj+MWjDwWjDUkFafMHgl/g7MzEy8ryWpLgd5WTHrn3rQaonB0pKJvMWS9w/C/c+KPB6bwPpOA7npyCaxvWNSIPdSS9Fe3ug33705ntm4t45BV7fd4hbaBYg6aNCj6+4KOg/xnS0lJbxADR5duDUtm16Bpe3AJHGot0fcnRfvmcC+v82lHsp3uVbKTklF288u6b+uCZUeyFvq2Qhzq6uUZebkISHE6tQv0lt0jKOhA6AU28nJgyJ7WThTrGtjxkOm+3/QAin83ZNthFR4PEMM4RYaf52+Z7n8f0agTw88X7m3ys9JXUpTtnJ1HmIfxfKZ0dPba0+Wy2l8SBPr6Ct90IRuZvD2eET7l0kQ3V5TKIK3O5zhaY/hDYdyl8x5FHOhqT8pcEJTBRQ8ZvYvTbC7kTiYWhfLqJF0Rev4gH9Fn3DXix5xXhlods3pqjE//xuPN4KCnuNvfaUFC3XYosW95PMleH1pJmO5JoWyAMDNntjRUVSNFpF9ZXF5GpzrCIHCrsEtMaV7IoZp93wAT8Cx0WaFO/go3wKL5GRkfqXhN2OcPEy7n8BwXEYBP0CtnzYvl378xH/dc7H8q6oCSlnQytPsz7qDa5LnY4k9no4EM/oexD403TTzbN4hK0FgqT04XokO10z8FgPhLCeQOHwGyjXjfvTrHL77MxmANLMhHA6xkbaGYj7JsiKD8ELEb4q0XqoSQ5IPIughdKInik0LQSycB8E3CQII7Y8eD81zNULnr6YgtyfwxWbMz4V+T+B42Bdyq54wZxCSHNuMWJEr7ML/W0kOgxxHw7L6F84HAL8eYRditOZJI3HY+ac2cqiYK+Ug9kjIq8NEOZ3SbyIhoyDFn4/58HWCpOdztFI+4Avz3M9jnezFUcpo5dKqVdoOt2DvI+imIbABk122OJoWj30eseiHQ+w10e7pj+M33oF2vMNSflOSWmpP9ZOWspmoXVJN+BypPl72e3syrkdypuPPjse35IHcf232vrQZWh0R2F3vIuw59GGGcjgRQh5dtt9CfL+VBrGpMAPP6w102n6X9gSZoQMHp3ajv57EPHvA5Fkew0P4K97rKvpPlCZ0Qjcrmk0DUFXoF6D2Lsm+jgbEcajX55EedP32AuogsIeYK8JAttDSD+jetcRWwnR0GoQhN11EMUjBxUUKv+EktqeYZqBV1DYTcSt6CUX5uUdKmx2JgfrKmqqHoAWyqyzLCXJ9R4+4pcl2ZPOipJ8DEL7Agiwa5DmddamISCgscvSomDwSx5OTswcWr3bJjSQA1rtLym5MG61D9rlQhL6tSijCtptfwgwkANaXlNXF5vb43KdLvbZMCzJlsQjAtNbbIBhzGQBCCEyCHXkoXJNSvoEdd7hRZDQ0FmaRqRxRbCkxFykh/ovQv1XIu7NUNQf9Fm+CIBvigL+m/mk0OOpIKGdijjNLjaCZj4XAjE2tCjk9yXBkoV5eXkdk2z24Si0Gn05NO5kyef2GRCC7+k8WkDU4DIZ9X22OOifbPaN18s2FSYht1h5mrzIHKeU0VuKS0r+asbJyZkvnC4md4ItKzZXr+JA4OGC3NwZZLcPFVIMRBYgTmI4BPdwb77nA7T3vNzc3Bcduo29SA5DnW8GEasBWbqaTWNHIfyllNk2YY6RHuV0OEaDBM4KRcIn4XfLDAaDm636LopZYKFlqN8sM8zjux7JwDflxFBdiH1zkHA672KX2IjLRmMeSGj7J8XW6ECB13cDGjMAmU32l/inW6Mn95okJhLuRLswvKWg8Guw1wRBTzNIb7P/fJJIWUdUz0bTuMyWRwPYOV0ktIYi5X8nR4cRpOkp+62OCr9tmBYAU1JNU8j4KK+WmtZDxC6yEL6xUfS1mpBLigKBUnzEv0C8EyH8j9CFuAD3dEOKSc2VAeHSyzpdkGjS1xIkMWHidg+w6nAQSEFjC4JrICyW7awds2fPngchAmEp/shCycxLyJkyYXqB110gzkF8XlNT0+DumHc3QLNm5p+ek5OTQG7kyvhZVIhya6Jvt4flHEJkW/FXJ3pgNISxQDMNpsVcL/8Co6E8YYjymE01axhQikzuHNRjSTwOW44s9Pi2WKM1TcDTBE67/dqozfZmMBAYz2G8hiE5KekiaP/T0E9ne3I9R/KaFOTzEvIZZdftQ335+UutkZDXAwE/L+5cD/JztjAJjbgTHTsuyabVof7Ts7OzRzU2n93QBiJzCkdImorf9KmEW9wXxY3i/uKWm2TIsvNiWq+MTfv4+LnR0RY1LKqwT7HXBMHeY/+6Rte0diSrZkGRqiZdT2s2jmGEKVLxBUUrZ5Ojy41Ik6IWJirsNtKTU3lRnx1f400bt279LjMz8yBLEK71lwQPivs6YLAQ37B5s6kJgpNOFDqdaBPaTUg7GN/zFcHS4PvNlWFExTprk82hrA3G/Q6AXBQi/ZOgDFdIm7aWo6CwJcUBf5/E9Ly+oaK6IrizdnA9ocXyNMOVwpzKoMrqurpvk53OMxPjQBCuYUdL6S5XzPESmYv5ukKDZlZdxYv/fsm1qavo3QE07tgLGA6vId3G65e7ow3J8aFxEKbesex3NJksSWuxPPTLUh5NsKaB5nFYYV5eT7LZM1tKA3JwEk8N2EieiG4/gdvPC/1w/hqIEpO5TE2X5vaoCBlTbKTdoAm6UkptudkAI2oKdfaTYSc9yz87eGyfPn1cKSkpx+hSPIT6X53VoQOvU2gghiJxQZQwzVq7eeSpKOB/Kx6MvuiMPmrTqH3NuOXWduHKW0Gh9bF3BAFPsK3j/n1ehesQc9rAWD+ZRJe7LCdN1g4HaVAESlB443NkS86npC63kaY3WTytoBBDzBMYnhF5fYHHd5Y55y0oF3/H4i6b4LjOWnC4OD464HW7x0N4Pu6Qsi0E0f1C0y/Jat+e48+C1vk5tDomC5fzI4n045tzHMQIzgkWxd0/I88XQAxeggZ9EMjBFITxqnh2GTwbcb5DVkdBiN8dFXIqqtxBJ22CLsTQjJSMAsTz76KNM9HGK2On9AULQ5CGRnHocdT3KXTEnws9nluhkYccmo19JrDK+nQg1vHPrAAABbRJREFUGAgXNk6z2xDmTgK2NwEyJUK6Pj9JEnuHvFiT4g20eyLIeyYvyqOYN+UndjtrGX2OhH4K8p6C/ukghawSun30zpIYQnyqm74nxHE+t/tN1OkdKfRk9DNve2RisWz9xo2mhbdgMLgKvycTvGGorwfH74pKSkwCZdM0duv8nC/f46Pa6tuivHBRyB+Q7xFobGzng5QVMTsvoj+vPZDRaKUk2zMghoPx4N0DkrGgvr5+WXJS0un4TZ9GXM77tD3tYQWFfY3GBCEid7UHkbFnSnmrMAmhp5HIGk2i7F6KVJeQaAtlyJZOFFpNRgXeaz0VxGAcCeehpKlRA4WdQvBq9D/x4kIR30VLtA2C9FMIn0f81rwxIxyNnO/Q9Sk8pGzOTcewAaT0huKEeFGS4yDsPsLpKgishv301ZpWCXV8KwowpwrM1eo5OadRkusZCMgRNkuIowaLIGiu8c8OmKMShX37nknJyU/xEDbynWAKHEnrJMmr/aWBJuSgRsrylNhwtamJ10ejnyXZNNZau6JNr5qttoau48eiEv/TEGAiNlSufWGNlmxDOeP9pUFzGF4axkrTDbL8Zdg7Eon8rNvstegv01YEmNA6fEjCJEXCKIB8DfmegJMxSH+bQ+g3btq6+aoO7dpvQYFXghCdbUXkHQDDi4NBcxcDmrnSPMpoQ3mSoqsEW2+wHCqh3z+ydjncjcY8iuNyJLgH5b/MpiwgfJt8c3ibZEH//gOFPYndT5+POl1gfSVqeLGoDIVu2WF6wIg+DuLEO1egacgnG4KJ3jRHLgRdRckpl+mxZwfp5NM1dXWvmc+MYfzDoWs8GnUM6val0G2fFAf8p6POw4QUD9k1fY7daVptZd9xn4TC9ddZ2cccSgmZsK5A8PlAgT74pWuJd9qwnwvla0Jhn2IHgiCSZfK+LhCa0L/2Jh0v+rF1uomiWipeC3x/y/CttmURZRxHWpd7yJbKo42x5shYQXtTzNy9SaTw20JR0H/K7sa19sNf2rt375Ftk5O71EkZnjdvXlnidAODV+BD8Hc1nM6KxJXlrLn379//YIPnvyxYnhbPzc7OdnXo0KEjiEI1tPUdPvaWPYKLevXqdUXHjIzO9ULUz5kzZ13jchPKqcrJyTmI983zNe+dz8vL66NpWkbc+yKIz2Rvv35/STQCVRQIPIU4T7M7ZZ4OQHvXJu7NZ2Hc2FU08t7IizE3btporlJmjRtldaupqdmekO9fDj/88JnJycmdwuHwloRdF6NQzzHs9jqsaaFEz5CxOgbvQXnPJXpIRB2+QP26odyf+drn9p0GJWBAbThUEM/XWgTJ36/KBQsWNGsvonjuXBbAg7nfLffPkbq6ug3N2hQQIjYEKWmVv7Tkg3iwtX3yZrThDsuGBW+hLEvcVsm7UNA/PfVIpBv/bvG2oE/ew+E9PA+dHA5HKojWhriNA7N+gcCzSPdxopVLEE9e/Do27u6bUR2q5V0p7GK7iW0IBYXWxF6vQdhb4GN43N6ndZDW6Voi/lNQ2I+wzAXvdMV4YxfLcbS0JdHSWFtyV2zCmurYrZXqcWNGcVjCp0EAteTW2Apv0fRyY1fRjERDSVZZjRdxkrUYsbJx+M7cXltrMpq4T04kEhpvCxV0tcuRdHShx2fu5gA5YG1fSJJ/jq/raAk7c/9c4PWy5t9exEc4hJzQ3HSR1YaVLZVhEYZm24jngYnOzy2kW93omvPZwd10cy62FRT2BfY7QVBQUFD4NbCmRlj4Xgit4Ty2FSmlWCOEfAaa9q8ylyyk6Ip/RyLPMrCmJ6HVT2ulaiso/OagCIKCgsJvDkWBwMc4fNzq+Qb9N7R2ngoKv1UogqCgoKCgoKDQBIogKCgoKCgoKDSBIggKCgoKCgoKTaAIgoKCgoKCgkITKIKgoKCgoKCg0ASKICgoKCgoKCg0gSIICgoKCgoKCk3w/+aEInSm8AgOAAAAAElFTkSuQmCC '
                    } );
                }
            },
            'excel', 'copy'
        ]
    });
});
</script>

<style>
    td.details-control {
    background: url('../assets/images/details_open.png') no-repeat center center;
    cursor: pointer;
}
    tr.shown td.details-control {
        background: url('../assets/images/details_close.png') no-repeat center center;
    }
</style>
</head>
<body class="theme-dark">


<div id="wrapper">

    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
            </div>

            <div class="navbar-brand">
            </div>
            
            <div class="navbar-right">
                <div id="navbar-menu">
                    <ul class="nav navbar-nav">
                       
                       
                       
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                <i class="icon-bell"></i>
                                <span id="notif-dot" class="notification-dot" style="display:none"></span>
                            </a>
                            <ul class="dropdown-menu notifications">
                                <li class="header"><strong>You have <span id="status_notif"></span> new Notifications</strong></li>
                                <div id="notif-body" style="overflow-x: hidden; height: auto; max-height: 200px;"></div>                          
                                <li class="footer"><a href="../notifikasi_list.php" class="more">See all notifications</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" onClick="logoutx()" class="icon-menu"><i class="icon-login"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div id="left-sidebar" class="sidebar">
        <div class="sidebar-scroll">
            <div class="user-account">
                 <img src="<?php echo '../assets/images/';
                if ($select->user($userID, 'photo') == '') {
                    echo 'f_avatar.png';
                } else {
                    echo $select->user($userID, 'photo');
                }; ?>" class="rounded-circle user-photo">

                <div class="dropdown">
                    <span>Welcome,</span>
                     <a href="" class="user-name"><strong> <?php echo ucwords($role) . ' <br> ' .
                $select->user($userID, 'nama_depan'). ' '.$select->user($userID, 'nama_belakang'). ' '.$select->user($userID, 'area') ; ?></strong></a>
                </div>
            </div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#menu">Menu</a></li>
            </ul>
                
            <!-- Tab panes -->
            <div class="tab-content p-l-0 p-r-0">
                <div class="tab-pane active" id="menu">
                    <nav id="left-sidebar-nav" class="sidebar-nav">
                        <?php
                           if ($select->user($userID, 'role_role_id') == 'SUPERADMIN') {                           
                        ?>  
                        <ul id="main-menu" class="metismenu">                          
                            <li >
                                <a href="../home.php"><i class="icon-home"></i> <span>Dashboard</span></a>
                            </li>
                            <li>
                                <a href="user_list.php"><i class="icon-users"></i> <span>User List</span></a>
                            </li>
                            <li>
                                <a href="list_wilayah2.php"><i class="icon-map"></i> <span>Region List</span></a>
                            </li>
                            <li>
                                <a href="mesin_list.php"><i class="icon-speedometer"></i> <span>Device List</span></a>
                            </li>
                            <li>
                                <a href="det_alat.php"><i class="icon-grid"></i> <span>Device Details</span></a>
                            </li>
                            <li>
                                <a href="event_history.php"><i class="fa fa-bar-chart-o"></i> <span>Event History</span></a>
                            </li>
                            <li class="active">
                                <a href="notif_list.php"><i class="icon-notebook"></i> <span>Summary Report</span></a>
                            </li>
                            <li>
                                <a href="list_task.php"><i class="icon-briefcase"></i> <span>Task List</span></a>
                            </li>
                            <li>
                                <a href="#" onClick="logoutx()"><i class="icon-power"></i> <span>Logout</span></a>
                            </li>
                        </ul>
                        <?php
                            }else{
                        ?>
                        <ul id="main-menu" class="metismenu">                          
                            <li>
                                <a href="../home.php"><i class="icon-home"></i> <span>Dashboard</span></a>
                            </li>
                            <li class="active">
                                <a href="notif_list.php"><i class="icon-notebook"></i> <span>Summary Report</span></a>
                            </li>
                            <li>
                                <a href="#" onClick="logoutx()"><i class="icon-power"></i> <span>Logout</span></a>
                            </li>
                        </ul>
                        <?php
                            }
                        ?> 
                    </nav>
                </div>              
            </div>          
        </div>
    </div>
    
    <?php echo $_COOKIE['namaterafo'].'/'.$_COOKIE['kodeterafo'];?>
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-bars"></i></a> Summary Report</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../home.php"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item"><a href="../home.php"> Home</a></li> 
                            <li class="breadcrumb-item active"><a href="notif_list.php"> Summary Report</a></li>
                        </ul>
                    </div>            
                </div>
            </div>
            
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Summary Report</h2>                       
                        </div>
                        <div class="body">
                         <!--   <button type="button" onClick="export_to()" class="btn btn-primary m-b-15 float-right" colspan="2" data-toggle="modal"><i class="fa fa-file-excel-o"> Export to Excel</i></button>
                            <button type="button" onClick="export_pdf()" class="btn btn-danger m-b-15 float-right" colspan="2" data-toggle="modal" style="margin-right:15px;"><i class=" fa fa-file-pdf-o"> Export to PDF</i></button> -->
                            <div class="table-responsive">

                                <table width="100%" id="datatable" class="table" style="font-style:">
        
                                  <thead>
               <!-- <th><center>No.</center></th> -->
               <th><center>Date</center></th>
               <th><center>Device Code</center></th>
               <th><center>IR (A)</center></th>
               <th><center>IS (A)</center></th>
               <th><center>IT (A)</center></th>
               <th><center>RN (V)</center></th>
               <th><center>SN (V)</center></th>
               <th><center>TN (V)</center></th>
               <th><center>Temp (C)</center></th>
               <th><center>FR (Hz)</center></th>
               <th><center>FS (Hz)</center></th>
               <th><center>FT (Hz)</center></th>
               </thead>

            
            <?php
			$kode_trafo='0001';
            $no=1;
            $i=0;
            $jumlah=0;
            $jumlahib=0;
            $jumlahic=0;
            $jumlahrn=0;
            $jumlahsn=0;
            $jumlahtn=0;
            $jumlahtr=0;
            $jumlahts=0;
            $jumlahtt=0;
            $idtrf = $kode_trafo;
            // echo $idtrf;
            

            $data2 = mysql_query("SELECT DISTINCT date_format(tanggal, '%Y-%m-%d') FROM report WHERE kode='".$idtrf."'");
            while ($row2 = mysql_fetch_array($data2)) {
            $tgl = $row2[0];

            $data = mysql_query("SELECT report.kode, report.tanggal, report.ia, report.ib, report.ic, report.va, report.vb, report.vc, report.ta, report.tb, report.tc, report.fa, report.fb, report.fc, m_mesin.kode_mesin, m_mesin.ratio
                FROM report
                LEFT JOIN m_mesin
                ON report.kode = m_mesin.kode_mesin 
                WHERE substring(report.tanggal, 1,11) = '".$tgl."' ");
                while ($row= mysql_fetch_array($data)) {

            $i = $i+1;
            $jumlah = $jumlah + ($row['ia']*$row['ratio']);
            $jumlahib = $jumlahib + ($row['ib']*$row['ratio']);
            $jumlahic = $jumlahic + ($row['ic']*$row['ratio']);
            $jumlahrn = $jumlahrn + $row['va'];
            $jumlahsn = $jumlahsn + $row['vb'];
            $jumlahtn = $jumlahtn + $row['vc'];
            $jumlahtr = $jumlahtr + $row['ta'];
            $jumlahts = $jumlahts + $row['tb'];
            $jumlahtt = $jumlahtt + $row['tc'];
            $jumlahfa = $jumlahfa + $row['fa'];
            $jumlahfb = $jumlahfb + $row['fb'];
            $jumlahfc = $jumlahfc + $row['fc'];
            }
            $rata = $jumlah / $i ;
            $rataib = $jumlahib/$i;
            $rataic = $jumlahic/$i;
            $ratarn = $jumlahrn/$i;
            $ratasn = $jumlahsn/$i;
            $ratatn = $jumlahtn/$i;
            $ratatr = $jumlahtr/$i;
            $ratats = $jumlahts/$i;
            $ratatt = $jumlahtt/$i;
            $ratafa = $jumlahfa/$i;
            $ratafb = $jumlahfb/$i;
            $ratafc = $jumlahfc/$i;
            ?>
            <tr style="background: transparent;">
                <!-- <td><center><?php print $no++;?></center></td> -->
                <td><center><?php print ($tgl); ?></center></td>
                <td><center><?php echo $kode_trafo;?></center></td>
                <td><center><?php print number_format($rata,2); ?></center></td>
                <td><center><?php print number_format($rataib,2); ?></center></td>
                <td><center><?php print number_format($rataic,2); ?></center></td>
                <td><center><?php print number_format($ratarn,2); ?></center></td>
                <td><center><?php print number_format($ratasn,2); ?></center></td>
                <td><center><?php print number_format($ratatn,2); ?></center></td>
                <td><center><?php print number_format($ratatr,2); ?></center></td>
                <td><center><?php print number_format($ratafa,2); ?></center></td>
                <td><center><?php print number_format($ratafb,2); ?></center></td>
                <td><center><?php print number_format($ratafc,2); ?></center></td>                          
            </tr>

            <?php
            }
            ?>          
        </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    
</div>

<div class="modal fade" id="modalAdd" name="modalAdd" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" id="ukuran" role="document">
        <div class="modal-content bg-dark">
            <!-- <div class="panel panel-default"> -->
                <div class="modal-header">
                    <h6 class="panel-title"><b id="cap_add">Caption title</b> 
                        <b id="kode_gi"></b><b id="mx_kode_child"></b><b id="kode_group"></b></h6>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                            onClick="dcook('addols','f')">&times;
                    </button>
                </div>
                <div class="modal-body bg-white">
                   <div class="row clearfix">
                       <div class="col-12">
                          <div class="panel-body content-body">Content body</div>
                       </div>
                  </div>
                  </div>
                <div class="modal-footer">
                <button type="button" name="btnAdd" id="btnModify" class="btn btn-warning" onclick="modify()" style="display: none">Modify</button>
                <button type="button" name="btnAdd" id="btnApprove" class="btn btn-primary" onclick="approval()" style="display: none">Approve</button>
                       <button type="button" name="btnAdd" id="btnAdd" class="btn btn-primary" onClick="simpan()">Save</button>
                       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
    </div>
</div>
</div>


<script>
    function export_to() {
        window.location = 'export_summary.php';
    }

    function export_pdf() {
        window.location = 'export_pdf.php';
    }

    function logoutx() {
        if (confirm("Apakah anda akan logout?")) {
            window.location = "../logout.php";
        }
    }
</script>

<audio id="status10">
  <source src="../assets/sound/alarm.mp4" type="audio/mp4">
</audio>
<audio id="status11">
  <source src="../assets/sound/train_low.mp3" type="audio/mp3">
</audio>

<!-- Javascript -->
<script src="../module/sse/receive_sse.js"></script>
<script src="../theme/assets/bundles/libscripts.bundle.js"></script>    
<script src="../theme/assets/bundles/vendorscripts.bundle.js"></script>
<script src="../theme/assets/bundles/datatablescripts.bundle.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.print.min.js"></script>
<script src="../assets/vendor/sweetalert/sweetalert.min.js"></script> <!-- SweetAlert Plugin Js --> 
<script src="../theme/assets/bundles/mainscripts.bundle.js"></script>
<script src="../theme/assets/js/pages/tables/jquery-datatable.js"></script>
</html>
