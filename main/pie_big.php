    <?php 
    include "koneksi.php";
    $id = '001'; 
    $query = mysql_query("SELECT ia.kode,ia.tanggal, ia.arus, m_mesin.kode_mesin, m_mesin.ratio, m_mesin.kapasitas
    FROM ia
    LEFT JOIN m_mesin
    ON ia.kode = m_mesin.kode_mesin
    where ia.kode='$id' ORDER BY tanggal DESC LIMIT 1");
    while ($row = mysql_fetch_array($query)) {
    $arus_ia=$row['arus']*$row['ratio'];
    $kapasitas=$row['kapasitas'];
    }
    ?>

    <?php
    $data2 = mysql_query("
    SELECT va.kode,va.tanggal, va.volt, m_mesin.kode_mesin
    FROM va
    LEFT JOIN m_mesin
    ON va.kode = m_mesin.kode_mesin
    where va.kode='$id' ORDER BY tanggal DESC LIMIT 1
    ");
    while($row2 = mysql_fetch_array($data2)){
        $volt_va=$row2['volt'];
      }
    ?>


    <?php
    $data3 = mysql_query("
    SELECT ib.kode,ib.tanggal, ib.arus, m_mesin.kode_mesin, m_mesin.ratio
    FROM ib
    LEFT JOIN m_mesin
    ON ib.kode = m_mesin.kode_mesin
    where ib.kode='$id' ORDER BY tanggal DESC LIMIT 1
    ");
    while($row3 = mysql_fetch_array($data3)){
        if ($row3['ratio']!=null){
        $arus_ib=($row3['arus']*$row3['ratio']);
        }
    }
   ?>

    <?php
    $data4 = mysql_query("
    SELECT vb.kode,vb.tanggal, vb.volt, m_mesin.kode_mesin
    FROM vb
    LEFT JOIN m_mesin
    ON vb.kode = m_mesin.kode_mesin
    where vb.kode='$id' ORDER BY tanggal DESC LIMIT 1
    ");
    while($row4 = mysql_fetch_array($data4)){
        $volt_vb=$row4['volt'];
      }
    ?>

    <?php
    $data5 = mysql_query("
    SELECT ic.kode,ic.tanggal, ic.arus, m_mesin.kode_mesin, m_mesin.ratio
    FROM ic
    LEFT JOIN m_mesin
    ON ic.kode = m_mesin.kode_mesin
    where ic.kode='$id' ORDER BY tanggal DESC LIMIT 1");
    while($row5 = mysql_fetch_array($data5)){
        if ($row5['ratio']!=null){
        $arus_ic=($row5['arus']*$row5['ratio']);
        }
    }
   ?>

    <?php
    $data6 = mysql_query("
    SELECT vc.kode,vc.tanggal, vc.volt, m_mesin.kode_mesin
    FROM vc
    LEFT JOIN m_mesin
    ON vc.kode = m_mesin.kode_mesin
    where vc.kode='$id' ORDER BY tanggal DESC LIMIT 1");
    while($row6 = mysql_fetch_array($data6)){
        $volt_vc=$row6['volt'];
      }
    ?>



<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

         var data = google.visualization.arrayToDataTable([
          ['Power Usage', 'Realtime'],
          ['IR'+" (<?php echo ($arus_ia*$volt_va/1000); ?>) " ,          <?php echo ($arus_ia*$volt_va/1000); ?>],
          ['IS'+" (<?php echo ($arus_ib*$volt_vb/1000); ?>) ",          <?php echo ($arus_ib*$volt_vb/1000); ?>],
          ['IT'+" (<?php echo ($arus_ic*$volt_vc/1000); ?>) ",          <?php echo ($arus_ic*$volt_vc/1000); ?>],
          ['Available'+" (<?php echo $kapasitas-(($arus_ia*$volt_va/1000)+($arus_ib*$volt_vb/1000)+($arus_ic*$volt_vc/1000)); ?>) ",  <?php echo $kapasitas-(($arus_ia*$volt_va/1000)+($arus_ib*$volt_vb/1000)+($arus_ic*$volt_vc/1000)); ?>]
        ]);

        var options = {
          width: 500, height: 300,
          backgroundColor: 'transparent',
          legend: {textStyle: {color: 'white'}},
          colors: ['red','#ffba00', 'blue', 'green']
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechartbig'));

        chart.draw(data, options);
      }
    </script>

<div id="piechartbig"></div>
 