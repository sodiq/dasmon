<?php

require_once 'dbconfig.php';

class USER
{

	private $conn;

	public function __construct()
	{
		$database = new Database();
		$db = $database->dbConnection();
		$this->conn = $db;
    }

	public function runQuery($sql)
	{
		$stmt = $this->conn->prepare($sql);
		return $stmt;
	}

	public function lasdID()
	{
		$stmt = $this->conn->lastInsertId();
		return $stmt;
	}

	public function register($usrcode,$role,$nama_depan,$nm_belakang,$uname,$email,$upass,$code)
	{
		try
		{
			$password = md5($upass);
		
			$stmt = $this->conn->prepare("INSERT INTO tbl_im_users(userCode,role_role_id,nama_depan,nama_belakang,userName,userEmail,userPass,tokenCode)
			                                             VALUES(:user_code,:role_id,:nama_depan,:nama_belakang,:user_name, :user_mail, :user_pass, :active_code)");
			//$usrcode=$this->lasdID()+1;
			$stmt->bindparam(":user_code",$usrcode);
			$stmt->bindparam(":role_id",$role);
			$stmt->bindparam(":nama_depan",$nama_depan);
			$stmt->bindparam(":nama_belakang",$nm_belakang);
			$stmt->bindparam(":user_name",$uname);
			$stmt->bindparam(":user_mail",$email);
			$stmt->bindparam(":user_pass",$password);
			$stmt->bindparam(":active_code",$code);
			$stmt->execute();
			return $stmt;
		}
		catch(PDOException $ex)
		{
			echo $ex->getMessage();
		}
	}

	public function login($email,$upass)
	{
		try
		{
			//$stmt = $this->conn->prepare("SELECT * FROM tbl_im_users WHERE userEmail=:email_id");
			$stmt = $this->conn->prepare("SELECT * FROM tbl_im_users WHERE userCode=:email_id OR userName=:email_id");
			$stmt->execute(array(":email_id"=>$email));
			$userRow=$stmt->fetch(PDO::FETCH_ASSOC);

			if($stmt->rowCount() == 1)
			{
				if($userRow['userStatus']=="Y")
				{
					if($userRow['userPass']==md5($upass))
					{
						$_SESSION['userSession'] = $userRow['userID'];
						$_SESSION['userRole'] = $userRow['role_role_id'];
						return true;
					}
					else
					{
						header("Location: login.php?error");
						exit;
					}
				}
				else
				{
					header("Location: login.php?inactive");
					exit;
				}
			}
			else
			{
				header("Location: login.php?error");
				exit;
			}
		}
		catch(PDOException $ex)
		{
			echo $ex->getMessage();
		}
	}


	public function is_logged_in()
	{
		if(isset($_SESSION['userSession']))
		{
			return true;
		}
	}

	public function redirect($url)
	{
		header("Location: $url");
	}

	public function logout()
	{
		session_destroy();
		$_SESSION['userSession'] = false;
	}

	function send_mail($email,$message,$subject)
	{
		return true;
		/**
		require_once('mailer/class.phpmailer.php');
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPDebug  = 0;
		$mail->SMTPAuth   = true;
		$mail->SMTPSecure = "ssl";
		$mail->Host       = "smtp.gmail.com";
		$mail->Port       = 465;
		$mail->AddAddress($email);
		$mail->Username="larizkpos@gmail.com";
		$mail->Password="psw";
		$mail->SetFrom('larizk@gmail.com','OLS Larizk');
		$mail->AddReplyTo("larizk@gmail.com","OLS Larizk");
		$mail->Subject    = $subject;
		$mail->MsgHTML($message);
		$mail->Send();
		**/
	}
}
