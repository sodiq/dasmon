<html>
<head>
<title>MiCOMP123</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style>
body {
    /*background: url("images/icons.png");*/
    background-color: #cccccc;
    background-repeat: no-repeat;
}

.red_bullet{
background: url("images/bullets_new_small.png");
background-position: -1px -48px; 
width: 16px;
height: 16px;
}

.green_bullet{
background: url("images/bullets_new_small.png");
background-position: -1px 0px; 
width: 16px;
height: 16px;
}

.blue_bullet{
background: url("images/bullets_new_small.png");
background-position: -1px -32px; 
width: 16px;
height: 16px;
}

.orange_bullet{
background: url("images/bullets_new_small.png");
background-position: -1px -17px; 
width: 16px;
height: 16px;
}
.trip{
z-index:10;
position:absolute;
left:41px;
top:220px;
}
.alarm{
z-index:10;
position:absolute;
left:41px;
top:245px; 
}
.warning{
z-index:10;
position:absolute;
left:41px;
top:270px;
}
.healthy{
z-index:10;
position:absolute;
left:41px;
top:295px;
}
.led5{
z-index:10;
position:absolute;
left:41px;
top:321px;
}
.led6{
z-index:10;
position:absolute;
left:41px;
top:346px;
}
.led7{
z-index:10;
position:absolute;
left:41px;
top:370px;
}
.led8{
z-index:10;
position:absolute;
left:41px;
top:394px;
}
#lcd{
	z-index:10;
	position:absolute;
	left:60px;
	top:142px;
	color:#666;
	font-family:arial;
	font-size:16px;
	font-weight:bold;
	border:solid 0px red;
	height:48px;
	width:179px;
	background-color:yellow;
}
#lcd .fasa_a{
	width:82px;
	height:21px;
	color:#666;
	float:left;
	border:solid 0px green;
	margin:2px;
}
#lcd .fasa_b{
	width:82px;
	height:21px;
	color:#666;
	float:right;
	border:solid 0px green;
	margin:2px;
	text-align:right;
}
#lcd .fasa_c{
	width:82px;
	height:21px;
	color:#666;
	float:left;
	border:solid 0px green;
	margin:2px;
}
#lcd .fasa_n{
	width:82px;
	height:21px;
	color:#666;
	float:right;
	border:solid 0px green;
	margin:2px;
	text-align:right;
}
<!--#097479-->
</style>
<script type="text/javascript">
        var ws;
        var wsUri = "ws://192.168.0.21:1880";   //change with your ip node red
        var loc = window.location;
        console.log(loc);
        if (loc.protocol === "https:") { wsUri = "wss:"; }
        // This needs to point to the web socket in the Node-RED flow
        // ... in this case it's ws/simple
        //wsUri += "//" + loc.host + loc.pathname.replace("simple","ws/simple");
        wsUri += "/"+("simple","ws/simple");
        function wsConnect() {
            console.log("connect",wsUri);
            ws = new WebSocket(wsUri);
            //var line = "";    // either uncomment this for a building list of messages
            ws.onmessage = function(msg) {
                var line = "";  // or uncomment this to overwrite the existing message
                // parse the incoming message as a JSON object
                var data = msg.data;
                //console.log(data);
                // build the output from the topic and payload parts of the object
                line += "<p>"+data+"</p>";
                // replace the messages div with the new "line"
                document.getElementById('messages').innerHTML = line;
                //ws.send(JSON.stringify({data:data}));
            }
            ws.onopen = function() {
                // update the status div with the connection status
                document.getElementById('status').innerHTML = "connected";
                //ws.send("Open for data");
                console.log("connected");
            }
            ws.onclose = function() {
                // update the status div with the connection status
                document.getElementById('status').innerHTML = "not connected";
                // in case of lost connection tries to reconnect every 3 secs
                setTimeout(wsConnect,3000);
            }
        }
        
        function doit(m) {
            if (ws) { ws.send(m); }
        }
		
	function addCurr(num) {
		num = num.toString().replace(/\$|\,/g,'');
		if(isNaN(num))
		num = "";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = Math.floor(num/10).toString();
		if(cents<10)
		cents = "" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+'.'+
		num.substring(num.length-(4*i+3));
		return (((sign)?'':'-') + num );
	}
	function rmCurr(nil){                     
	  var nil=nil.replace('.','');                     
	  return nil;
	}
	function incCounter() {
		setTimeout('incCounter()',1000);
		var nil=parseInt($('#nil').html());
		var count=nil+1;
		var dsp=$('#nil').html(count);
		if(count==2){
		  $('#nil').html(0);
		}

		$.ajax({method: "GET",url:'status.php'})
		 .done(function( msg ) //0 :healthy 2:trip
		 {
			if(msg=='2'){ 
				$('.trip').addClass('red_bullet');
				$('.alarm').addClass('orange_bullet');
				$('.id_alarm').show();
				$('.id_fasa').hide();
				add_blink();
				bcd_sound();
				//alarm_sound(msg);
			}else{
				$('.trip').removeClass('red_bullet');
				$('.alarm').removeClass('orange_bullet');
				$('.id_alarm').hide();
				$('.id_fasa').show();
				
				$.ajax({method: "GET",url:'arus.php'})
				 .done(function( msg )
				 {
				 var dt=msg.split('#');
				$('.fasa_a').html(addCurr(dt[0])+' A');
				$('.fasa_b').html(addCurr(dt[1])+' A');
				$('.fasa_c').html(addCurr(dt[2])+' A');
				$('.fasa_n').html(addCurr(dt[3])+' A');

				 });					
				$('.alarm').removeClass('orange_bullet');
			}
		});
	}
	
	function add_blink(){
		//for(i=0;i<=2;i++) {
	     $('.alarm').effect("pulsate", {}, 350);
		 //}
	}
	
	/* var dt=msg.payload.split('#');
	var ia=dt[0];
	var ib=dt[1];
	var ic=dt[2];
	var iref=dt[3];
	msg.topic='insert into display (ia,ib,ic,iref) values('+ia+','+ib+','+ic+','+iref+')'
	return msg; */
	
	function bcd_sound () {
		document.getElementById('bcd_sound').play();
	}
	function alarm_sound(x) {
		//if(x==2){
			setTimeout('alarm_sound()',100);
			document.getElementById('bcd_sound').play();
		//}
	}
    </script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="wsConnect();incCounter(); add_blink();" onunload="ws.disconnect();">
	<b id="nil" style="display:none;">0</b>	
	<b id="arus" style="display:none;"></b>
	<audio id="bcd_sound" src="addons/train_low.mp3"></audio>
	<i class="trip" onclick="send_socket('trip')"></i>
	<i class="alarm"></i>
	<i class="warning"></i>
	<i class="healthy green_bullet" onclick="alarm_sound()"></i>
	<i class="led5"></i>
	<i class="led6"></i>
	<i class="led7"></i>
	<i class="led8"></i>
	<div id="lcd">
		<div style="width:200p; height:100px; display:none;" class="id_alarm" >ALARMS</div>
		<div class="id_fasa">
			<div class="fasa_a">0.00 A</div>
			<div class="fasa_b">0.00 A</div>
			<div class="fasa_c">0.00 A</div>
			<div class="fasa_n">0.00 A</div>
		</div>
	</div>
	<img style="width:300px; height:500px"src="images/micomp123_small.jpg"/>
</body>
</html>
<script src="js/jquery-1.7.1.min.js"></script>
<script src="js/jquery-ui-1.8.18.custom.min.js"></script>