<div class="modal fade" id="modal_notif" name="modal_notif" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content bg-dark">
                <div class="modal-header">
                    <h6 class="panel-title"><b >Notification Detail</b> 
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                            onClick="dcook('addols','f')">&times;
                    </button>
                </div>
                <div class="modal-body bg-white">
                   <div class="row clearfix">
                       <div class="col-12">
                          <div class="panel-body content-body">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label class="label">Province</label>
                                    <div><input class="form-control" id="provinsi" readonly="true"></div><div id="suggest_prov"></div>
                                    </div>
                                    <div class="col-sm-4">
                                    <label class="label">City</label>
                                    <div><input class="form-control" id="kota" readonly="true" ></div><div id="suggest"></div>
                                    </div>
                                    <div class="col-sm-4">
                                    <label class="label">District</label>
                                    <div><input class="form-control" id="kecamatan" readonly="true" ></div><div id="suggest_kec"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label class="label">Village</label>
                                    <div><input class="form-control" id="desa"  readonly="true" ></div><div id="suggest_desa"></div>
                                    </div>
                                    <div class="col-sm-4">
                                    <label class="label">Device Name</label>
                                    <div><input class="form-control" id="nama_alat" readonly="true"></div>
                                    </div>
                                    <div class="col-sm-4">
                                    <label class="label">Device Type</label>
                                    <div><input class="form-control" id="tipe" readonly="true"></div>
                                </div>
                            </div>
                            <br>
                            <div class="form-group row">
                            <div class="input-group col-md-4" style="height: 20px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">Device IP </span>
                                </div>
                                    <input type="text" id="device_ip" class="form-control" readonly="true">
                                </div>
                                <div class="input-group col-md-4" style="height: 20px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">Capacity </span>
                                </div>
                                    <input type="number" id="capacity" class="form-control" readonly="false">
                                    <div class="input-group-prepend">
			                        <span class="input-group-text"><select style="margin-right: -10px; font-size:12px"><option>Amper</option><option>MVA</option></select></span>
		                            </div>
                                </div>
                                
                                <div class="input-group col-md-4" style="height: 20px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">CT Primer </span>
                                </div>
                                    <input type="number" id="ct_primer" class="form-control" readonly oninput="hit_ratio()" onchange="hit_ratio()">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text">A</span>
		                            </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group row">
                            <div class="input-group col-md-4" style="height: 20px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">CT Sekunder </span>
                                </div>
                                    <input type="number" id="ct_sekunder" class="form-control" readonly oninput="hit_ratio()" onchange="hit_ratio()">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text">A</span>
		                            </div>
                                </div>
                                <div class="input-group col-md-4" style="height: 20px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">Ratio</span>
                                </div>
                                    <input type="number" id="ratio" class="form-control" readonly>
                                </div>
                                <div class="input-group col-md-4" style="height: 20px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">Set OLS </span>
                                </div>
                                    <input type="number" id="set_ols" class="form-control" readonly>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group row">
                            <div class="input-group col-md-4" style="height: 20px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">Max Volt </span>
                                </div>
                                    <input type="number" id="max_volt" class="form-control" readonly>
                                </div>
                                <div class="input-group col-md-4" style="height: 20px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">Max Temp</span>
                                </div>
                                    <input type="number" id="max_temp" class="form-control" readonly>
                                </div>
                                <div class="input-group col-md-4" style="height: 20px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">Keterangan </span>
                                </div>
                                    <input type="text" id="keterangan" class="form-control" readonly>
                                </div>
                            </div>
                            <br>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label class="label">Latitude</label>
                                    <div><input class="form-control" id="latitude"  readonly="true"></div>
                                    </div>
                                    <div class="col-sm-4">
                                    <label class="label">Longitude</label>
                                    <div><input class="form-control" id="longitude" readonly="true"></div>
                                    </div>
                                </div>
                          </div>
                       </div>
                  </div>
                  </div>
                  <input type="hidden" id="kode_mesin_updt">
                  <input type="hidden" id="id_log">
                  <input type="hidden" id="id_notif">
                  <input type="hidden" id="id_teknisi">
				  <input type="hidden" id="realisasi_id_teknisi">
                <div class="modal-footer">
                <button type="button" name="btnAdd" id="btnModify" class="btn btn-warning" onclick="modify()">Modify</button>
                       <button type="button" name="btnAdd" id="btnApprove" class="btn btn-primary" onclick="approval()">Approve</button>
                       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
    </div>
</div>