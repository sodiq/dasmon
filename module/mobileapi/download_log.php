<?php
include 'koneksi.php';

$kode=$_POST['kode'];
$filename = $_POST['name'];

// GET ARUS
$query_arus = "SELECT * FROM arus WHERE kode = '".$kode."' AND flag='Y' ORDER BY tanggal DESC LIMIT 3000";
$result_arus = mysqli_query($connection,$query_arus);
$array_data_arus = array();
while($baris = mysqli_fetch_assoc($result_arus))
{
  $array_data_arus[]=$baris;
}
$data_arus=json_encode($array_data_arus);

$hasil_arus=json_decode($data_arus, true);

//GET VOLTAGE
$query_volt = "SELECT * FROM volt WHERE kode = '".$kode."' AND flag='Y' ORDER BY tanggal DESC LIMIT 3000";
$result_volt = mysqli_query($connection,$query_volt);
$array_data_volt = array();
while($baris = mysqli_fetch_assoc($result_volt))
{
  $array_data_volt[]=$baris;
}
$data_volt=json_encode($array_data_volt);

$hasil_volt=json_decode($data_volt, true);

//GET TEMPERATURE
$query_temp = "SELECT * FROM temp WHERE kode = '".$kode."' AND flag='Y' ORDER BY tanggal DESC LIMIT 3000";
$result_temp = mysqli_query($connection,$query_temp);
$array_data_temp = array();
while($baris = mysqli_fetch_assoc($result_temp))
{
  $array_data_temp[]=$baris;
}
$data_temp=json_encode($array_data_temp);

$hasil_temp=json_decode($data_temp, true);

// CREATE XLS FILE 
$judul = "IO ALERT SYSTEM HISTORY";
$columnHeader = "No." . "\t" . "Tanggal Arus" . "\t" . "Arus R" . "\t" . "Arus S" . "\t" . "Arus T" . "\t" . "Tanggal Volt" . "\t" . "Volt R" . "\t" . "Volt S" . "\t" . "Volt T" . "\t" . "Tanggal Temp" . "\t" . "Temp R" . "\t" . "Temp S" . "\t" . "Temp T" . "\t";
$setData = '';

for ($no = 1; $no <= 3000; $no++) {
    $rowData = '';
    $rowData .= '"' . $no . '"' . "\t";
    $rowData .= '"' . $hasil_arus[$no]['tanggal'] . '"' . "\t";
    $rowData .= '"' . $hasil_arus[$no]['ia'] . '"' . "\t";
    $rowData .= '"' . $hasil_arus[$no]['ib'] . '"' . "\t";
    $rowData .= '"' . $hasil_arus[$no]['ic'] . '"' . "\t";
    $rowData .= '"' . $hasil_volt[$no]['tanggal'] . '"' . "\t";
    $rowData .= '"' . $hasil_volt[$no]['va'] . '"' . "\t";
    $rowData .= '"' . $hasil_volt[$no]['vb'] . '"' . "\t";
    $rowData .= '"' . $hasil_volt[$no]['vc'] . '"' . "\t";
    $rowData .= '"' . $hasil_temp[$no]['tanggal'] . '"' . "\t";
    $rowData .= '"' . $hasil_temp[$no]['ta'] . '"' . "\t";
    $rowData .= '"' . $hasil_temp[$no]['tb'] . '"' . "\t";
    $rowData .= '"' . $hasil_temp[$no]['tc'] . '"' . "\t";
    $setData .= trim($rowData) . "\n";
} 

$data= $judul . "\n" ."\n" ."\n" .$columnHeader . "\n" . $setData . "\n";
$name = 'logs/'.$filename.'.xls';
$fp = fopen($name , 'wb');
fwrite($fp , $data );
chmod($name, 0777);
fclose($fp);
