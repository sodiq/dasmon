<?php
include 'koneksi.php';
$id = $_POST['kode'];


$query = "SELECT 
    mesin.*,
    provinsi.provinsi_id,
    kota.kota_id,
    kecamatan.kecamatan_id,
    desa.desa_id
FROM 
    m_mesin mesin
    LEFT JOIN provinsi ON mesin.provinsi_name = provinsi.provinsi_name
    LEFT JOIN kota On mesin.kota_name = kota.kota_name AND kota.provinsi_id=provinsi.provinsi_id
    LEFT JOIN kecamatan On mesin.kecamatan_name = kecamatan.kecamatan_name AND kecamatan.kota_id=kota.kota_id
    LEFT JOIN desa On mesin.desa_name = desa.desa_name AND desa.kecamatan_id=kecamatan.kecamatan_id
WHERE 
    kode_mesin='".$id."' 
ORDER BY 
    id 
ASC";
$result = mysqli_query($connection,$query);
$array_data = array();
while($baris = mysqli_fetch_assoc($result))
{
  $array_data[]=$baris;
}
echo json_encode($array_data);
?>
