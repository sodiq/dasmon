<?php
include 'koneksi.php';
$kode_mesin = $_POST['kode_mesin'];
$nama = $_POST['nama'];
$nama_trafo = $_POST['nama_trafo'];
$kode_gi = $_POST['kode_gi'];
$jenis_mesin = $_POST['jenis_mesin'];
$ip = $_POST['ip'];
$keterangan = $_POST['keterangan'];
$serial_number = $_POST['serial_number'];
$desa_id = $_POST['desa_id'];
$kecamatan_id = $_POST['kecamatan_id'];
$kota_id = $_POST['kota_id'];
$provinsi_id = $_POST['provinsi_id'];
$kapasitas = $_POST['kapasitas'];
$ct_primer = $_POST['ct_primer'];
$ct_sekunder = $_POST['ct_sekunder'];
$ratio = $_POST['ratio'];
$set_ols = $_POST['set_ols'];
$max_volt = $_POST['max_volt'];
$max_temp = $_POST['max_temp'];
$latitude = $_POST['latitude'];
$longitude = $_POST['longitude'];

$desa_name='';
$kecamatan_name='';
$kota_name='';
$provinsi_name='';

//GET DESA
if($desa_id!=''){
    $queryDesa = "SELECT * FROM desa WHERE desa_id='".$desa_id."'";
    $result_desa = mysqli_query($connection,$queryDesa);
    $array_data_desa = array();
    while($baris = mysqli_fetch_assoc($result_desa))
    {
      $array_data_desa[]=$baris;
    }
    $data_desa=json_encode($array_data_desa);
    $hasil_desa=json_decode($data_desa, true);

    $desa_name=$hasil_desa[0]['desa_name'];
}
//GET KECAMATAN
if($kecamatan_id!=''){
    $queryKecamatan = "SELECT * FROM kecamatan WHERE kecamatan_id='".$kecamatan_id."'";
    $result_kecamatan = mysqli_query($connection,$queryKecamatan);
    $array_data_kecamatan = array();
    while($baris = mysqli_fetch_assoc($result_kecamatan))
    {
      $array_data_kecamatan[]=$baris;
    }
    $data_kecamatan=json_encode($array_data_kecamatan);
    $hasil_kecamatan=json_decode($data_kecamatan, true);

    $kecamatan_name=$hasil_kecamatan[0]['kecamatan_name'];
}
//GET KOTA
if($kota_id!=''){
    $queryKota = "SELECT * FROM kota WHERE kota_id='".$kota_id."'";
    $result_kota = mysqli_query($connection,$queryKota);
    $array_data_kota = array();
    while($baris = mysqli_fetch_assoc($result_kota))
    {
      $array_data_kota[]=$baris;
    }
    $data_kota=json_encode($array_data_kota);
    $hasil_kota=json_decode($data_kota, true);

    $kota_name=$hasil_kota[0]['kota_name'];
}
//GET PROVINSI
if($provinsi_id!=''){
    $queryProvinsi = "SELECT * FROM provinsi WHERE provinsi_id='".$provinsi_id."'";
    $result_provinsi = mysqli_query($connection,$queryProvinsi);
    $array_data_provinsi = array();
    while($baris = mysqli_fetch_assoc($result_provinsi))
    {
      $array_data_provinsi[]=$baris;
    }
    $data_provinsi=json_encode($array_data_provinsi);
    $hasil_provinsi=json_decode($data_provinsi, true);

    $provinsi_name=$hasil_provinsi[0]['provinsi_name'];
}

$tanggal_sekarang = date('Y-m-d H:i:s');
$query ="update m_mesin set tanggal='".$tanggal_sekarang."', kode_mesin='".$kode_mesin."', nama='".$nama_trafo."', active='N', kode_gi='001', jenis_mesin='".$jenis_mesin."', ip='".$ip."', keterangan='".$keterangan."', serial_number='".$serial_number."', desa_name='".$desa_name."', kecamatan_name='".$kecamatan_name."', kota_name='".$kota_name."', provinsi_name='".$provinsi_name."', kapasitas='".$kapasitas."', ct_primer='".$ct_primer."', ct_sekunder='".$ct_sekunder."', ratio='".$ratio."', set_ols='".$set_ols."', max_volt='".$max_volt."', max_temp='".$max_temp."', latitude='".$latitude."', longitude='".$longitude."' where kode_mesin='".$kode_mesin."' ";
$result=mysqli_query($connection,$query);
$respose[]="success";
echo json_encode($respose);
?>

