<?php
include 'koneksi.php';
$id = $_POST['kode'];
$phase= $_POST['phase'];
$getdata= $_POST['getdata'];
$array_data = array();

$query = "";
if ($phase == "all"){
    if ($getdata=="current"){
        $query = "SELECT
        ia.arus AS arusa,
        ib.arus as arusb,
        ic.arus as arusc
        FROM
        (SELECT kode, MAX(tanggal) as tanggal, arus FROM ia GROUP BY kode ) ia
        LEFT JOIN (SELECT kode, MAX(tanggal) as tanggal,arus FROM ib GROUP BY kode) ib ON ib.kode = ia.kode
        LEFT JOIN (SELECT kode, MAX(tanggal) as tanggal,arus FROM ic GROUP BY kode) ic ON ic.kode = ia.kode
        WHERE ia.kode = '".$id."'";
        $result = mysqli_query($connection,$query);
        while($baris = mysqli_fetch_assoc($result))
        {
          $array_data[]=$baris;
        }
        echo json_encode($array_data);
    } else if ($getdata=="volt"){
        $query = "SELECT
        va.volt as vola,
        vb.volt as volb,
        vc.volt as volc
        FROM
        (SELECT kode, MAX(tanggal) as tanggal, volt FROM va GROUP BY kode ) va
        LEFT JOIN (SELECT kode, MAX(tanggal) as tanggal,volt FROM vb GROUP BY kode) vb ON vb.kode = va.kode
        LEFT JOIN (SELECT kode, MAX(tanggal) as tanggal,volt FROM vc GROUP BY kode) vc ON vc.kode = va.kode
        WHERE va.kode = '".$id."'";
        $result = mysqli_query($connection,$query);
        $array_data = array();
        while($baris = mysqli_fetch_assoc($result))
        {
          $array_data[]=$baris;
        }
        echo json_encode($array_data);
    } else if ($getdata=="temp"){
        $query = "SELECT
        ta.temp as tempa,
        tb.temp as tempb,
        tc.temp as tempc
        FROM
        (SELECT kode, MAX(tanggal) as tanggal, temp FROM ta GROUP BY kode ) ta
        LEFT JOIN (SELECT kode, MAX(tanggal) as tanggal,temp FROM tb GROUP BY kode) tb ON tb.kode = ta.kode
        LEFT JOIN (SELECT kode, MAX(tanggal) as tanggal,temp FROM tc GROUP BY kode) tc ON tc.kode = ta.kode
        WHERE ta.kode = '".$id."'";
        
        $result = mysqli_query($connection,$query);
        $array_data = array();
        while($baris = mysqli_fetch_assoc($result))
        {
          $array_data[]=$baris;
        }
        echo json_encode($array_data);
    } else if ($getdata=="freq"){
        $query = "SELECT
        freqa.freq as freqa,
        freqb.freq as freqb,
        freqc.freq as freqc
        FROM
        (SELECT kode, MAX(tanggal) as tanggal, freq FROM freqa GROUP BY kode ) freqa
        LEFT JOIN (SELECT kode, MAX(tanggal) as tanggal,freq FROM freqb GROUP BY kode) freqb ON freqb.kode = freqa.kode
        LEFT JOIN (SELECT kode, MAX(tanggal) as tanggal,freq FROM freqc GROUP BY kode) freqc ON freqc.kode = freqa.kode
        WHERE freqa.kode = '".$id."'";
        
        $result = mysqli_query($connection,$query);
        while($baris = mysqli_fetch_assoc($result))
        {
          $array_data[]=$baris;
        }
        echo json_encode($array_data);
    } else if ($getdata=="all"){
        $query = "SELECT
        ia.arus AS arusa,
        ib.arus as arusb,
        ic.arus as arusc,
        ta.temp As tempa,
        tb.temp As tempb,
        tc.temp As tempc,
        va.volt As vola,
        vb.volt As volb,
        vc.volt As volc,
        freqa.freq AS freqa,
        freqb.freq AS freqb,
        freqc.freq AS freqc
        FROM
        -- GET ARUS
        (SELECT kode, MAX(tanggal) as tanggal, arus FROM ia GROUP BY kode ) ia
        LEFT JOIN (SELECT kode, MAX(tanggal) as tanggal,arus FROM ib GROUP BY kode) ib ON ib.kode = ia.kode
        LEFT JOIN (SELECT kode, MAX(tanggal) as tanggal,arus FROM ic GROUP BY kode) ic ON ic.kode = ia.kode
        
        -- GET VOLT
        LEFT JOIN (SELECT kode, MAX(tanggal) as tanggal,volt FROM va GROUP BY kode) va ON va.kode = ia.kode
        LEFT JOIN (SELECT kode, MAX(tanggal) as tanggal,volt FROM vb GROUP BY kode) vb ON vb.kode = ia.kode
        LEFT JOIN (SELECT kode, MAX(tanggal) as tanggal,volt FROM vc GROUP BY kode) vc ON vc.kode = ia.kode
        
        -- GET TEMP
        LEFT JOIN (SELECT kode, MAX(tanggal) as tanggal,temp FROM ta GROUP BY kode) ta ON ta.kode = ia.kode
        LEFT JOIN (SELECT kode, MAX(tanggal) as tanggal,temp FROM tb GROUP BY kode) tb ON tb.kode = ia.kode
        LEFT JOIN (SELECT kode, MAX(tanggal) as tanggal,temp FROM tc GROUP BY kode) tc ON tc.kode = ia.kode
        
        -- GET FREQUENCY
        LEFT JOIN (SELECT kode, MAX(tanggal) as tanggal,freq FROM freqa GROUP BY kode) freqa ON freqa.kode = ia.kode
        LEFT JOIN (SELECT kode, MAX(tanggal) as tanggal,freq FROM freqb GROUP BY kode) freqb ON freqb.kode = ia.kode
        LEFT JOIN (SELECT kode, MAX(tanggal) as tanggal,freq FROM freqc GROUP BY kode) freqc ON freqc.kode = ia.kode
        
        WHERE ia.kode = '".$id."'";
        $result = mysqli_query($connection,$query);
        while($baris = mysqli_fetch_assoc($result))
        {
          $array_data[]=$baris;
        }
        echo json_encode($array_data);
    } else {
        echo "error";    
    }
}
?>
