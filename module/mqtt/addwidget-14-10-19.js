function addwgchartsum(design_id, topic, panel_name, icon) {
    if (ls_read('role') == 'SUPERADMIN') {
        document.getElementById("widgetdata").innerHTML += '\<div id = wg' + design_id + ' class=" ' + design_id + ' col-lg-6 body team_list realelect wg-thumb">\
                    <div class="card ">\
                        <div class="header p-0">\
                            <ul class="header-dropdown">\
                                <li class="dropdown">\
                                    <a href="javascript:void(0);" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">\
									<i class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i>\
									</a>\
                                    <ul class="dropdown-menu pull-top bg-dark">\
                                        <li><a href="javascript:void(0);" data-type="success" data-toggle="modal" data-target="#editwidget"><i class="icon icon-note text-warning"></i> Modify</a></li>\
                                     	<li><a href="javascript:delwidget(' + design_id + ');" ><i class="icon icon-trash text-danger"></i> Remove</li></a>\
										</li>\
										</ul>\
										</li>\
										</ul></div>\
                        <div class="body ">\
						<div class="text-left"><h5><i class="icon icon-graph text-primary"></i> ' + panel_name + '</h5></div>\
                            <h5>\
                                <span class="m-r-50"  id ="txtchrt1' + design_id + '"><i class="fa fa-bolt text-danger"></i>12</span>\
                                <span class="m-r-50" id ="txtchrt2' + design_id + '" ><i class="fa fa-bolt text-primary"></i>121</span>\
                                <span class="m-r-50" id ="txtchrt3' + design_id + '" ><i class="fa fa-bolt text-success"></i>121</span>\
								<div id= "' + design_id + '" style="height: 250px;">\
							</div>\
                        </div>\
                    </div>\
                </div>\
            </div>';
    } else {
        document.getElementById("widgetdata").innerHTML += '\<div id = wg' + design_id + ' class=" ' + design_id + ' col-lg-6 body team_list realelect wg-thumb">\
                    <div class="card ">\
                        <div class="header p-0">\
                           </div>\
                        <div class="body ">\
						<div class="text-left"><h5><i class="icon icon-graph text-primary"></i> ' + panel_name + '</h5></div>\
                            <h5>\
                                <span class="m-r-50"  id ="txtchrt1' + design_id + '"><i class="fa fa-bolt text-danger"></i>12</span>\
                                <span class="m-r-50" id ="txtchrt2' + design_id + '" ><i class="fa fa-bolt text-primary"></i>121</span>\
                                <span class="m-r-50" id ="txtchrt3' + design_id + '" ><i class="fa fa-bolt text-success"></i>121</span>\
								<div id= "' + design_id + '" style="height: 250px;">\
							</div>\
                        </div>\
                    </div>\
                </div>\
            </div>';
    }
}

function addwgtbsum(design_id, topic, panel_name, icon) {
    if (ls_read('role') == 'SUPERADMIN') {
        document.getElementById("widgetdata").innerHTML += '<div id = wg' + design_id + ' class="row col-md-6 realelect  ' + design_id + ' wg-thumb" >\
															<div class="card" style="margin-left: 15px; ">\
															<div class="header p-0">\
																		<ul class="header-dropdown">\
																			<li class="dropdown">\
																				<a href="javascript:void(0);" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">\
																				<i class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i>\
																				</a>\
																				<ul class="dropdown-menu pull-top bg-dark">\
																				<li><a data-type="success" href="main/notif_list.php"><i class="icon icon-list text-primary"></i> Detail</a></li>\
																				<li><a href="javascript:delwidget(' + design_id + ');" ><i class="icon icon-trash text-danger ' + design_id + '"></i> Remove</li></a>\
																						</ul>\
																			</li>\
																		</ul>\
																	</div>\
																	<div class="body">\
																		<div class="text-left"><h5><i class="icon icon-notebook text-primary"></i> Summary report</h5></div>\
																					  <div id="' + design_id + '"  class="sum_table">ok</div>\
																				</div>\
																			</div>\
																	</div>\
																</div>';
    } else {
        document.getElementById("widgetdata").innerHTML += '<div id = wg' + design_id + ' class="row col-md-6 realelect  ' + design_id + ' wg-thumb" >\
															<div class="card" style="margin-left: 15px; ">\
															<div class="header p-0">\
																	</div>\
																	<div class="body">\
																		<div class="text-left"><h5><i class="icon icon-notebook text-primary"></i> Summary report</h5></div>\
																					  <div id="' + design_id + '"  class="sum_table">ok</div>\
																				</div>\
																			</div>\
																	</div>\
																</div>';
    }
}
// function addwgmap(design_id,topic,panel_name,icon){
// 	document.getElementById("widgetdata").innerHTML+='<div id = wg'+design_id+' class="row  col-lg-12 col-md-12 clearfix '+design_id+' ">\
// 														<div class="col-md-12 covarea">\
// 															<div class="card visitors-map" id="maps" style="width:1050px;">\
// 																<div class="header p-0">\
// 																	<ul class="header-dropdown">\
// 																		<li class="dropdown">\
// 																			<a href="javascript:void(0);" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">\
// 																			<i class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i>\
// 																			</a>\
// 																			<ul class="dropdown-menu pull-top bg-dark">\
// 																			        <li><a class="btn-toggle-fullwidth" onclick="detailmap()"><i class="icon icon-screen-desktop text-primary btn-toggle-fullwidth" onclick="detailmap()"></i> Full Screen</a></li>\
// 																					<li><a href="javascript:void(0);" data-type="success" data-toggle="modal" data-target="#editwidget" onclick="modify_widget("'+design_id+'")"><i class="icon icon-note text-warning"></i> Modify</a></li>\
// 																					<li><a href="javascript:delwidget('+design_id+');" ><i class="icon icon-trash text-danger"></i> Remove</li></a>\
// 																					</ul>\
// 																		</li>\
// 																	</ul>\
// 																</div>\
// 																<div class="body">\
// 																	<div class="text-left"><h5><i class="icon icon-map text-primary"></i> Coverage Area</h5></div>\
// 																	<div class="row">\
// 																		<div class="col-lg-12 col-md-12">\
// 																			      <div class="viewmap" id="'+design_id+'" style="width:100%; height:320px;"></div>\
// 																			</div>\
// 																		</div>\
// 																</div>\
// 															</div>\
// 														</div>\
// 													</div>';


// }

function addwgmap(design_id, topic, panel_name, icon) {
    if (ls_read('role') == 'SUPERADMIN') {
        $('#widgetmapx').prepend('<div id="covarea"></div>');
        document.getElementById("covarea").innerHTML += '\
	<div class="row clearfix wg-map ' + design_id + '" id="wg' + design_id + ' ">\
		<div class="col-md-12">\
			<div class="card visitors-map">\
				<div class="header p-0">\
					<ul class="header-dropdown">\
						<li class="dropdown">\
							<a href="javascript:void(0);" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">\
								<i class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i>\
							</a>\
							<ul class="dropdown-menu pull-top bg-dark">\
								<li><a href="javascript:fullMap()"><i class="icon icon-screen-desktop text-primary icon-fcs"></i> <span class="fcs">Fullscreen</span></a></li>\
								<li><a href="javascript:void(0);" data-type="success" data-toggle="modal" data-target="#editwidget"><i class="icon icon-note text-warning"></i> Modify</a></li>\
								<li><a href="javascript:delwidget(' + design_id + ');"><i class="icon icon-trash text-danger"></i> Remove</a></li>\
							</ul>\
						</li>\
					</ul>\
				</div>\
				<div class="body">\
					<div class="text-left"><h5><i class="icon icon-map text-primary"></i> Coverage Area</h5></div>\
					<div class="row">\
						<div class="col-lg-12 col-md-12">\
							<div class="indonesia-map" id="' + design_id + '"  style="height:420px; position:relative; width:100%;"></div>\
						</div>\
					</div>\
				</div>\
			</div>\
		</div>\
	</div>';
    } else {
        $('#widgetmapx').prepend('<div id="covarea"></div>');
        document.getElementById("covarea").innerHTML += '\
	<div class="row clearfix wg-map ' + design_id + '" id="wg' + design_id + ' ">\
		<div class="col-md-12">\
			<div class="card visitors-map">\
				<div class="header p-0">\
					<ul class="header-dropdown">\
						<li class="dropdown">\
							<a href="javascript:void(0);" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">\
								<i class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i>\
							</a>\
							<ul class="dropdown-menu pull-top bg-dark">\
								<li><a href="javascript:fullMap()"><i class="icon icon-screen-desktop text-primary icon-fcs"></i> <span class="fcs">Fullscreen</span></a></li>\
							</ul>\
						</li>\
					</ul>\
				</div>\
				<div class="body">\
					<div class="text-left"><h5><i class="icon icon-map text-primary"></i> Indonesian Map</h5></div>\
					<div class="row">\
						<div class="col-lg-12 col-md-12">\
							<div class="indonesia-map" id="' + design_id + '"  style="height:420px; position:relative; width:100%;"></div>\
						</div>\
					</div>\
				</div>\
			</div>\
		</div>\
	</div>';
    }
}

function addwgpie(design_id, topic, panel_name, icon, pos) {
    if (ls_read('role') == 'SUPERADMIN') {
        document.getElementById("widgetdata").innerHTML += '<div id = wg' + design_id + ' class="' + design_id + ' col-lg-6 realelect wg-thumb">\
																		<div class="card">\
																			<div class="header p-0">\
																				<ul class="header-dropdown ">\
																						<li class="dropdown">\
																						   <a href="javascript:void(0);" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">\
																						<i class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i>\
																						</a>\
																						<ul class="dropdown-menu pull-top bg-dark">\
																							<li><a href="javascript:void(0);" data-type="success" data-toggle="modal" data-target="#editwidget" onclick="modify_widget("' + design_id + '")"><i class="icon icon-note text-warning"></i> Modify</a></li>\
																							<li><a href="javascript:delwidget(' + design_id + ');" ><i class="icon icon-trash text-danger"></i> Remove</li></a>\
																						</ul>\
																					</li>\
																				</ul>\
																			</div>	\
																			<div class="body gender-overview">\
																				 <div class="text-left"><h5><i class="icon icon-pie-chart text-primary"></i> ' + panel_name + '</h5></div>\
																					<div id="' + design_id + '" class="flot-chart" style="margin-top: -20px; height: 300px; margin-left: 15px;"></div>\
																					<p style="display:none;" id=pos' + design_id + '>' + pos + '</p>\
																				</div>\
																			</div>\
																		</div>\
																</div>';
    } else {
        document.getElementById("widgetdata").innerHTML += '<div id = wg' + design_id + ' class="' + design_id + ' col-lg-6 realelect wg-thumb">\
																		<div class="card">\
																			<div class="header p-0">\
																			</div>	\
																			<div class="body gender-overview">\
																				 <div class="text-left"><h5><i class="icon icon-pie-chart text-primary"></i> ' + panel_name + '</h5></div>\
																					<div id="' + design_id + '" class="flot-chart" style="margin-top: -20px; height: 300px; margin-left: 15px;"></div>\
																					<p style="display:none;" id=pos' + design_id + '>' + pos + '</p>\
																				</div>\
																			</div>\
																		</div>\
																</div>';
    }
}

function addwgpiehttp(design_id, topic, panel_name, icon, pos) {
    if (ls_read('role') == 'SUPERADMIN') {
        document.getElementById("widgetdata").innerHTML += '<div id = wg' + design_id + ' class="' + design_id + ' col-lg-6 realelect wg-thumb">\
																		<div class="card">\
																			<div class="header p-0">\
																				<ul class="header-dropdown ">\
																						<li class="dropdown">\
																						   <a href="javascript:void(0);" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">\
																						<i class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i>\
																						</a>\
																						<ul class="dropdown-menu pull-top bg-dark">\
																							<li><a href="javascript:void(0);" data-type="success" data-toggle="modal" data-target="#editwidget" onclick="modify_widget("' + design_id + '")"><i class="icon icon-note text-warning"></i> Modify</a></li>\
																							<li><a href="javascript:delwidget(' + design_id + ');" ><i class="icon icon-trash text-danger"></i> Remove</li></a>\
																						</ul>\
																					</li>\
																				</ul>\
																			</div>	\
																			<div class="body gender-overview">\
																				 <div class="text-left"><h5><i class="icon icon-pie-chart text-primary"></i>' + panel_name + '</h5></div>\
																					<div id="' + design_id + '" class="flot-chart" style="margin-top: -20px; height: 300px;"></div>\
																					<p id=pos' + design_id + '>' + pos + '</p>\
																				</div>\
																			</div>\
																		</div>\
																	</div>';
    } else {
        document.getElementById("widgetdata").innerHTML += '<div id = wg' + design_id + ' class="' + design_id + ' col-lg-6 realelect wg-thumb">\
																		<div class="card">\
																			<div class="header p-0">\
																			</div>	\
																			<div class="body gender-overview">\
																				 <div class="text-left"><h5><i class="icon icon-pie-chart text-primary"></i>' + panel_name + '</h5></div>\
																					<div id="' + design_id + '" class="flot-chart" style="margin-top: -20px; height: 300px;"></div>\
																					<p id=pos' + design_id + '>' + pos + '</p>\
																				</div>\
																			</div>\
																		</div>\
																	</div>';
    }
}

function addwggauge(design_id, topic, panel_name, icon, unit, pos, kode_widget) {
    if (ls_read('role') == 'SUPERADMIN') {
        document.getElementById("widgetdata").innerHTML += '\<div id = wg' + design_id + ' class="col-lg-3 col-md-3 ' + design_id + ' body team_list wg-thumb">\
																		<div class="card info-box-2 dd" data-plugin="nestable">\
																			<div class="header p-0">\
																				<ul class="header-dropdown m-r--5">\
																					<li class="dropdown"> <a href="javascript:void(0);" class="" data-toggle="dropdown"\
																							role="button" aria-haspopup="true" aria-expanded="false"> <i\
																								class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i> </a>\
																						<ul class="dropdown-menu pull-top bg-dark">\
																							<li><a href="javascript:void(0);" data-type="success" data-toggle="modal" data-target="#editwidget" onclick="modify_widget(' + design_id + ')"><i class="icon icon-note text-warning"></i> Modify</a></li>\
																							<li><a href="javascript:delwidget(' + design_id + ',' + pos + ',' + kode_widget + ');" ><i class="icon icon-trash text-danger"></i> Remove</li></a>\
																						</ul>\
																					</li>\
																				</ul>\
																			</div>	\
																			<div class="body">\
																				<div class="text-center dd"><h5><i class="' + icon + '"></i> ' + panel_name + '</h5></div>\
																				<div class="icon">\
																					<div id="' + design_id + '" class="" style="width:100px; height:55px;"></div>\
																					<p hidden>' + topic + '</p>\
																				</div>\
																				<div class="content">\
																					<div class="text"> Average Value</div>\
																					<div id ="txt' + design_id + '"  style="font-size: 20px;"><span >0</span>' + unit + '</div>\
																					<div style="display: none;" class="pos' + design_id + '">' + pos + '</div>\
																				</div>\
																			</div>\
																		</div>\
																	</div>';
    } else {
        document.getElementById("widgetdata").innerHTML += '\<div id = wg' + design_id + ' class="col-lg-3 col-md-3 ' + design_id + ' body team_list wg-thumb">\
																		<div class="card info-box-2 dd" data-plugin="nestable">\
																			<div class="header p-0">\
																			</div>	\
																			<div class="body">\
																				<div class="text-center dd"><h5><i class="' + icon + '"></i> ' + panel_name + '</h5></div>\
																				<div class="icon">\
																					<div id="' + design_id + '" class="" style="width:100px; height:55px;"></div>\
																					<p hidden>' + topic + '</p>\
																				</div>\
																				<div class="content">\
																					<div class="text"> Average Value</div>\
																					<div id ="txt' + design_id + '"  style="font-size: 20px;"><span >0</span>' + unit + '</div>\
																					<div style="display: none;" class="pos' + design_id + '">' + pos + '</div>\
																				</div>\
																			</div>\
																		</div>\
																	</div>';
    }
}

function addwgreportchart(design_id, name, pos, kode_widget) {
    if (ls_read('role') == 'SUPERADMIN') {
        document.getElementById("widgetdata").innerHTML += '<div id = wg' + design_id + ' class=" col-lg-6 realelect  ' + design_id + ' wg-thumb">\
                    <div class="card ">\
                        <div class="header p-0">\
                            <ul class="header-dropdown">\
                                <li class="dropdown">\
                                    <a href="javascript:void(0);" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">\
									<i class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i>\
									</a>\
                                    <ul class="dropdown-menu pull-top bg-dark">\
                                        <li><a href="javascript:void(0);" data-type="success" data-toggle="modal" data-target="#editwidget"><i class="icon icon-note text-warning"></i> Modify</a></li>\
                                     	<li><a href="javascript:delwidget(' + design_id + ',' + pos + ',' + kode_widget + ');" ><i class="icon icon-trash text-danger"></i> Remove</li></a>\
										</li>\
										</ul>\
										</li>\
										</ul></div>\
                        <div class="body gender-overview">\
						<div class="text-left"><h5><i class="icon icon-fire text-warning"></i> ' + name + '</h5></div>\
                            <h5>\
                                <span class="m-r-50"  id ="txtchrt1' + design_id + '"><i class="icon icon-bar-chart m-r-10 text-danger"></i>12</span>\
                                <span class="m-r-50" id ="txtchrt2' + design_id + '" ><i class="fa fa-bolt m-r-10 text-primary"></i>121</span>\
                                <span class="m-r-50" id ="txtchrt3' + design_id + '" ><i class="fa fa-bolt m-r-10 text-primary"></i>121</span>\
								<div id= "wgchart' + design_id + '" style="height: 250px;">\
							</div>\
                        </div>\
                    </div>\
                </div>\
            </div>';
    } else {
        document.getElementById("widgetdata").innerHTML += '<div id = wg' + design_id + ' class=" col-lg-6 realelect  ' + design_id + ' wg-thumb">\
                    <div class="card ">\
                        <div class="header p-0">\
                            </div>\
                        <div class="body gender-overview">\
						<div class="text-left"><h5><i class="icon icon-fire text-warning"></i> ' + name + '</h5></div>\
                            <h5>\
                                <span class="m-r-50"  id ="txtchrt1' + design_id + '"><i class="icon icon-bar-chart m-r-10 text-danger"></i>12</span>\
                                <span class="m-r-50" id ="txtchrt2' + design_id + '" ><i class="fa fa-bolt m-r-10 text-primary"></i>121</span>\
                                <span class="m-r-50" id ="txtchrt3' + design_id + '" ><i class="fa fa-bolt m-r-10 text-primary"></i>121</span>\
								<div id= "wgchart' + design_id + '" style="height: 250px;">\
							</div>\
                        </div>\
                    </div>\
                </div>\
            </div>';
    }
}

function addwgreportcharthttp(design_id, name, pos, kode_widget) {
    document.getElementById("widgetdata").innerHTML += '<div id = wg' + design_id + ' class=" col-lg-6 realelect  ' + design_id + ' wg-thumb">\
                    <div class="card ">\
                        <div class="header p-0">\
                            <ul class="header-dropdown">\
                                <li class="dropdown">\
                                    <a href="javascript:void(0);" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">\
									<i class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i>\
									</a>\
                                    <ul class="dropdown-menu pull-top bg-dark">\
                                        <li><a href="javascript:void(0);" data-type="success" data-toggle="modal" data-target="#editwidget"><i class="icon icon-note text-warning"></i> Modify</a></li>\
                                     	<li><a href="javascript:delwidget(' + design_id + ',' + pos + ',' + kode_widget + ');" ><i class="icon icon-trash text-danger"></i> Remove</li></a>\
										</li>\
										</ul>\
										</li>\
										</ul></div>\
                        <div class="body gender-overview">\
						<div class="text-left"><h5><i class="icon icon-map text-primary"></i>' + name + '</h5></div>\
                            <h5>\
                                <span class="m-r-50"  id ="txtchrt1' + design_id + '"><i class="icon icon-bar-chart m-r-10 text-danger"></i>12</span>\
                                <span class="m-r-50" id ="txtchrt2' + design_id + '" ><i class="fa fa-bolt m-r-10 text-primary"></i>121</span>\
                                <span class="m-r-50" id ="txtchrt3' + design_id + '" ><i class="fa fa-bolt m-r-10 text-primary"></i>121</span>\
                                <span class="m-r-50" id ="txtchrt4' + design_id + '" ><i class="fa fa-bolt m-r-10 text-primary"></i>121</span>\
								<div id= "wgchart' + design_id + '" style="height: 250px;">\
							</div>\
                        </div>\
                    </div>\
                </div>\
            </div>';
}

function addwgrealtimechart(design_id) {
    if (ls_read('role') == 'SUPERADMIN') {
        document.getElementById("widgetdata").innerHTML += '<div class="' + design_id + ' row clearfix col-lg-12 col-md-12 wg-thumb" >\
                <div class="col-lg-20 realelect ' + design_id + ' ">\
                    <div class="card">\
                        <div class="header p-0">\
                            <div class="float-right">\
                                <div class="switch panel-switch-btn"> <span class="m-r-10 font-12">Realtime</span>\
                                    <label>OFF\
                                        <input type="checkbox" id="realtime' + design_id + '" checked>\
                                        <span class="lever switch-col-cyan"></span>ON</label>\
                                </div>\
                            </div>   \
							<ul class="header-dropdown">\
                                <li class="dropdown">\
                                    <a href="javascript:void(0);" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">\
									<i class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i>\
									</a>\
                                    <ul class="dropdown-menu pull-top bg-dark">\
                                        <li><a href="javascript:void(0);" data-type="success" data-toggle="modal" data-target="#editwidget"><i class="icon icon-note text-warning"></i> Modify</a></li>\
                                     	<li><a href="javascript:delwidget(' + design_id + ');" ><i class="icon icon-trash text-danger"></i> Remove</li></a>\
										</li>\
										</ul>\
										</li>\
										</ul></div>\
                        <div class="body gender-overview">\
						<div class="text-left"><h5><i class="icon icon-map text-primary"></i>Sumarry Report Electricity (AVG)</h5></div>\
                            <h5>\
                                <span class="m-r-50"><i class="icon icon-line-chart m-r-10 text-danger"></i> 2,21,598</span>\
                             </h5>\
							<div id= "rt' + design_id + '" class="flot-chart">\
							</div>\
                        </div>\
                    </div>\
                </div>\
            </div>';
    } else {
        document.getElementById("widgetdata").innerHTML += '<div class="' + design_id + ' row clearfix col-lg-12 col-md-12 wg-thumb" >\
                <div class="col-lg-20 realelect ' + design_id + ' ">\
                    <div class="card">\
                        <div class="header p-0">\
                            <div class="float-right">\
                                <div class="switch panel-switch-btn"> <span class="m-r-10 font-12">Realtime</span>\
                                    <label>OFF\
                                        <input type="checkbox" id="realtime' + design_id + '" checked>\
                                        <span class="lever switch-col-cyan"></span>ON</label>\
                                </div>\
                            </div>   \
							<ul class="header-dropdown">\
                                <li class="dropdown">\
                                    <a href="javascript:void(0);" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">\
									<i class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i>\
									</a>\
                                    </div>\
                        <div class="body gender-overview">\
						<div class="text-left"><h5><i class="icon icon-map text-primary"></i>Sumarry Report Electricity (AVG)</h5></div>\
                            <h5>\
                                <span class="m-r-50"><i class="icon icon-line-chart m-r-10 text-danger"></i> 2,21,598</span>\
                             </h5>\
							<div id= "rt' + design_id + '" class="flot-chart">\
							</div>\
                        </div>\
                    </div>\
                </div>\
            </div>';
    }
}

function addwgbar(design_id, topic, panel_name, icon) {
    if (ls_read('role') == 'SUPERADMIN') {
        document.getElementById("widgetdata").innerHTML += '<div id = wg' + design_id + ' class="' + design_id + ' col-lg-6 realelect wg-thumb">\
	<div class="card">\
		<div class="header p-0">\
			<ul class="header-dropdown ">\
					<li class="dropdown">\
					   <a href="javascript:void(0);" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">\
					<i class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i>\
					</a>\
					<ul class="dropdown-menu pull-top bg-dark">\
						<li onclick="fullwidht(' + design_id + ')"><a href="javascript:void(0)"><i class="icon icon-screen-desktop text-primary icon-fcs"></i> <span class="fcs"> Full Widht</span></a></li>\
						<li onclick="update_compare(' + design_id + ',\'' + panel_name + '\')"><a href="javascript:void(0);"><i class="icon icon-note text-warning" ></i> Modify</a></li>\
						<li><a href="javascript:delwidget(' + design_id + ');" ><i class="icon icon-trash text-danger"></i> Remove</li></a>\
					</ul>\
				</li>\
			</ul>\
		</div>\
		<div class="body gender-overview">\
		<div class="text-left"><h5><i class="icon icon-bar-chart text-primary"></i> ' + panel_name + '</h5></div>\
		<div id="' + design_id + '" style="margin-top: -20px; height: 300px;"></div>\
		<div class="row" style="margin-left: 20px">\
			<div class="col-sm-3"><i class="fa fa-square" style="color:red"></i> Current R</div>\
			<div class="col-sm-3"><i class="fa fa-square" style="color:yellow"></i> Current S</div>\
			<div class="col-sm-3"><i class="fa fa-square" style="color:blue"></i> Current T</div>\
				<p id=pos' + design_id + '></p>\
			</div>\
		</div>\
	</div>\
</div>';
    } else {
        document.getElementById("widgetdata").innerHTML += '<div id = wg' + design_id + ' class="' + design_id + ' col-lg-6 realelect wg-thumb">\
	<div class="card">\
		<div class="header p-0">\
			<ul class="header-dropdown ">\
			</ul>\
		</div>\
		<div class="body gender-overview">\
		<div class="text-left"><h5><i class="icon icon-bar-chart text-primary"></i> ' + panel_name + '</h5></div>\
		<div id="' + design_id + '" style="margin-top: -20px; height: 300px;"></div>\
		<div class="row" style="margin-left: 20px">\
			<div class="col-sm-3"><i class="fa fa-square" style="color:red"></i> Current R</div>\
			<div class="col-sm-3"><i class="fa fa-square" style="color:yellow"></i> Current S</div>\
			<div class="col-sm-3"><i class="fa fa-square" style="color:blue"></i> Current T</div>\
				<p id=pos' + design_id + '></p>\
			</div>\
		</div>\
	</div>\
</div>';
    }

}

function renderChart() {
    datajson = document.getElementById("setting_gui").innerHTML
    $.each(JSON.parse(datajson), function(idx, obj) {
        if (obj.kd_widget == "2" && obj.protocol == "MQTT") {
            label = obj.topic.split('/');
            widgetdata[idx] = Morris.Area({
                element: "wgchart" + obj.design_id,
                behaveLikeLine: true,
                xkey: 'period',
                data: [{
                    period: 0,
                    CurrentR: 0,
                    CurrentS: 0,
                    CurrentT: 0
                }],
                ykeys: ['CurrentR', 'CurrentS', 'CurrentT'],
                labels: [label[3] + ' R', label[3] + ' S', label[3] + ' T'],
                pointSize: 2,
                smoth: true,
                fillOpacity: 0.3,
                gridLineColor: '#27303e',
                lineWidth: 2,
                hideHover: 'auto',
                lineColors: ['red', 'yellow', 'blue'],
                parseTime: false,


            });
        }
    });

    realtimechart();
}

function renderCharthttp() {
    var i = 0;
    datajson = document.getElementById("setting_gui").innerHTML;
    $.each(JSON.parse(datajson), function(idx, obj) {
        if (obj.kd_widget == "2" && obj.protocol == "HTTP") {
            label = obj.topic.split('/');
            httpchart[i] = Morris.Area({
                element: "wgchart" + obj.design_id,
                behaveLikeLine: true,
                xkey: 'period',
                data: [{
                    period: 0,
                    current: 0,
                    volt: 0,
                    freq: 0,
                    temp: 0
                }],
                ykeys: ['current', 'volt', 'freq', 'temp'],
                labels: ['current', 'volt', 'freq', 'temp'],
                pointSize: 2,
                smoth: true,
                fillOpacity: 0.3,
                gridLineColor: '#27303e',
                lineWidth: 2,
                hideHover: 'auto',
                lineColors: ['#f20a0a', '#0f44f2', '#0cf523'],
                parseTime: false,


            });
            i++
        }
    });
}

function renderSummarychart() {
    try {
        datajson = document.getElementById("setting_gui").innerHTML
        $.each(JSON.parse(datajson), function(idx, obj) {
            if (obj.kd_widget == "5") {
                label = obj.topic.split('/');
                reportchart[idx] = Morris.Area({
                    element: obj.design_id + "",
                    behaveLikeLine: true,
                    xkey: 'period',
                    data: [{
                        period: 0,
                        CurrentR: 0,
                        CurrentS: 0,
                        CurrentT: 0
                    }],
                    ykeys: ['Current', 'Voltage', 'Temperature'],
                    labels: ['Current', 'Voltage', 'Temperature'],
                    pointSize: 2,
                    smoth: true,
                    fillOpacity: 0,
                    pointStrokeColors: ['#f20a0a', '#0f44f2', '#0cf523'],
                    behaveLikeLine: true,
                    gridLineColor: '#27303e',
                    lineWidth: 2,
                    hideHover: 'auto',
                    lineColors: ['#f20a0a', '#0f44f2', '#0cf523'],
                    parseTime: true,
                    resize: true

                });
            }
        });

        readsumarychart();
    } catch (err) { alert(err.message) }
}



function rendergauge() {
    datajson = document.getElementById("setting_gui").innerHTML
    $.each(JSON.parse(datajson), function(idx, obj) {
        if (obj.kd_widget == "1") {
            widgetdata[idx] = new JustGage({
                id: obj.design_id,
                value: 0,
                valueFontColor: '#5E6773',
                valueFontFamily: 'Roboto, sans-serif',
                valueMinFontSize: 12,
                symbol: 'A',
                min: parseInt(obj.payload_min),
                max: parseInt(obj.payload_max),
                minTxt: obj.payload_min,
                maxTxt: obj.payload_max,
                hideValue: true,
                donut: false,
                label: '',
                labelFontColor: '#A0AEBA',
                labelMinFontSize: 12,
                counter: true,
                pointer: true,
                pointerOptions: {
                    color: '#5E6773'
                }
            });
        }
    });
}

function newrendergauge(newwidgetnumb, design_id, pmin, pmax) {
    plotgauge[newwidgetnumb] = new JustGage({
        id: design_id,
        value: 0,
        valueFontColor: '#5E6773',
        valueFontFamily: 'Roboto, sans-serif',
        valueMinFontSize: 12,
        symbol: 'A',
        min: parseInt(pmin),
        max: parseInt(pmax),
        minTxt: pmin,
        maxTxt: pmax,
        hideValue: true,
        donut: false,
        label: '',
        labelFontColor: '#A0AEBA',
        labelMinFontSize: 12,
        counter: true,
        pointer: true,
        pointerOptions: {
            color: '#5E6773'
        }
    });
}

function renderpie() {
    datajson = document.getElementById("setting_gui").innerHTML
    $.each(JSON.parse(datajson), function(idx, obj) {
        if (obj.kd_widget == "3") {
            var data = google.visualization.arrayToDataTable([
                ['Power Usage', 'Realtime'],
                ['IR', 0],
                ['IS', 0],
                ['IT', 0],
                ['Available', 100]
            ]);
            optionspie = {
                width: 500,
                height: 300,
                backgroundColor: 'transparent',
                legend: { textStyle: { color: 'white' } },
                colors: ['red', '#ffba00', 'blue', 'green']
            };
            plotpie[idx] = new google.visualization.PieChart($('#' + obj.design_id)[0]);
            plotpie[idx].draw(data, optionspie);
        }
    });
}

function renderpiehttp() {
    datajson = document.getElementById("setting_gui").innerHTML
    $.each(JSON.parse(datajson), function(idx, obj) {
        if (obj.kd_widget == "3" && obj.protocol == "HTTP") {
            var data = google.visualization.arrayToDataTable([
                ['Power Usage', 'Realtime'],
                ['IR', 0],
                ['IS', 0],
                ['IT', 0],
                ['Available', 100]
            ]);
            optionspie = {
                width: 500,
                height: 300,
                backgroundColor: 'transparent',
                legend: { textStyle: { color: 'white' } },
                colors: ['red', '#ffba00', 'blue', 'green']
            };
            plotpie[idx] = new google.visualization.PieChart($('#' + obj.design_id)[0]);
            plotpie[idx].draw(data, optionspie);

        }
    });
}

function rendertbsum() {
    datajson = document.getElementById("setting_gui").innerHTML
    $.each(JSON.parse(datajson), function(idx, obj) {
        try {
            if (obj.kd_widget == "6") {
                $.get("main/viewsum.php")
                    .done(function(data) {
                        ($('#' + obj.design_id).html(data))
                    });
            }
        } catch (err) {
            alert(err.message)
        }
    })
}

function rendermap() {
    datajson = document.getElementById("setting_gui").innerHTML
    $.each(JSON.parse(datajson), function(idx, obj) {
        try {
            if (obj.kd_widget == "4") {
                $.get("main/maps.php")
                    .done(function(data) {
                        ($('#' + obj.design_id).html(data))
                    });
            }
        } catch (err) {
            alert(err.message)
        }
    })
}

function renderBar() {
    datajson = document.getElementById("setting_gui").innerHTML;
    $.each(JSON.parse(datajson), function(idx, obj) {
        try {
            if (obj.kd_widget == "7") {
                // $.post("process/search_kode.php", { design: obj.design_id })
                //     .done(function(data) {
                //         $.ajax({
                //             type: "POST",
                //             url: "process/compare_bar.php",
                //             dataType: "json",
                //             data: { kode: data },
                //             success: function(response) {
                //                 console.log(response);
                //                 var morrisBar = new Morris.Bar({
                //                     element: '' + obj.design_id,
                //                     data: response,
                //                     resize: true,
                //                     barColors: ['red', 'yellow', 'blue'],
                //                     xkey: 'y',
                //                     ykeys: ['a', 'b', 'c'],
                //                     labels: ['Current R', 'Current S', 'Current T'],
                //                     hideHover: 'auto'
                //                 });
                //             }
                //         });

                //     });
                var data = [];

                var morrisBar = new Morris.Bar({
                element: ''+obj.design_id,
                data: data,
                resize: true,
                barColors: ['red','yellow', 'blue'],
                xkey: 'y',
                ykeys: ['a','b', 'c'],
                labels: ['Current R','Current S','Current T'],
                hideHover: 'auto'
                });

                setInterval(function(){
                $.post( "process/search_kode.php",{design : obj.design_id} )
                .done(function( data ) {
                $.ajax({
                type: "POST",
                url: "process/compare_bar.php",
                dataType: "json",
                data:{kode:data},
                success: function(response){
                    morrisBar.setData(response);
                        }
                    });
                    });		
                },3000)	
            }

        } catch (err) {
            alert(err.message)
        }
    });
}