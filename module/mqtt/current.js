var host ="das.pt-mgi.co.id";
var port = "8083";
var user='das';
var pass='das1234';
var topic='das/sensor/phaseR';
startConnect(host,port,topic);

function startConnect(host,port,topic,user='das',pass='das1234') {
    clientID = "clientID-" + parseInt(Math.random() * 100);
     client = new Paho.MQTT.Client(host, Number(port), clientID);
    client.onConnectionLost = onConnectionLost;
    client.onMessageArrived = onMessageArrived;
	 var options = {
            useSSL: false,
            cleanSession: true,
            timeout: 3,
            onSuccess: subcribe,
			userName:user,
			password:pass
         };
    client.connect(options)
}

function subcribe(topic){
   topic = "das/sensor/phaseR";
   client.subscribe(topic);
}

function onConnectionLost(responseObject) {
    document.getElementById("messages").innerHTML = 'ERROR: Connection lost';
	console.log(responseObject.errorMessage);
    if (responseObject.errorCode !== 0) {
        document.getElementById("messages").innerHTML =  responseObject.errorMessage ;
    }
}

function onMessageArrived(message) {
	console.log("onMessageArrived: " + message.payloadString);
	
	document.getElementById("widget1").innerHTML=message.payloadString;
	activityGauge.refresh(message.payloadString);
}

function startDisconnect() {
    client.disconnect();
    document.getElementById("messages").innerHTML += '<span>Disconnected</span><br/>';
}