function notifAction(status, id, kode){
    if (status != null){
        if(status == 'N01' || status == 'N02' || status == 'N03'){
            if(status == 'N01'){
                $.ajax({
                    type: "POST",
                    url: "process/notif_detail_home.php",
                    data: "kode="+kode,
                    dataType: "json",
                    success: function(response){
                        localStorage.setItem('healty', kode);
                        localStorage.setItem('nama_healty', response.nama);
                    }
                    });
            }
            window.location.href = 'main/det_alat.php';
            deleteNotif(id);
        }else if (status == 'N04' || status == 'N05'){
                    $.ajax({
                    type: "POST",
                    url: "process/notif_detail_home.php",
                    data: "kode="+kode,
                    dataType: "json",
                    success: function(response){
                        $('#provinsi').val(response.provinsi_name);
                        $('#kota').val(response.kota_name);
                        $('#kecamatan').val(response.kecamatan_name);
                        $('#desa').val(response.desa_name);
                        $('#nama_alat').val(response.nama);
                        $('#tipe').val(response.jenis_mesin);
                        $('#device_ip').val(response.ip);
                        $('#capacity').val(response.kapasitas);
                        $('#ct_primer').val(response.ct_primer);
                        $('#ct_sekunder').val(response.ct_sekunder);
                        $('#ratio').val(response.ratio);
                        $('#set_ols').val(response.set_ols);
                        $('#max_volt').val(response.max_volt);
                        $('#max_temp').val(response.max_temp);
                        $('#keterangan').val(response.keterangan);
                        $('#latitude').val(response.latitude);
                        $('#longitude').val(response.longitude);
                        $('#kode_mesin_updt').val(response.kode_mesin);
                        $('#id_log').val(id);
                        $('#id_notif').val(status);
                        $('#id_teknisi').val(response.teknisi_id);
                        $('#realisasi_id_teknisi').val(response.realisasi_teknisi_id);
                    }
                    });
            $('#modal_notif').modal('show');
        }else{
            return false;
        }
    }else{
        return false;
    }
}

function approval(){
    var id = $('#id_log').val();
    swal({
        title: "Are you sure to approve?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: false
     },
     function(isConfirm){
       if (isConfirm){
            $.post( "process/mesin_update_flag.php", {
                    kode_mesin: $('#kode_mesin_updt').val(), 
                    nama_provinsi:$('#provinsi').val(),
                    nama_kota:$('#kota').val(),
                    nama_kecamatan:$('#kecamatan').val(),
                    nama_desa:$('#desa').val(),
                    namamesin:$('#nama_alat').val(),
                    jenismesin:$('#tipe').val(),
                    ip:$('#device_ip').val(),      
                    kapasitas:$('#capacity').val(),
                    ct_primer:$('#ct_primer').val(),
                    ct_sekunder: $('#ct_sekunder').val(),
                    ratio: $('#ratio').val(),
                    set_ols: $('#set_ols').val(),
                    max_volt: $('#max_volt').val(),
                    max_temp: $('#max_temp').val(),
                    latitude: $('#latitude').val(),
                    longitude: $('#longitude').val(),
                    keterangan:$('#keterangan').val()})
                    .done(function(data) {
                        $.post("process/insert_task_history.php",{
                            teknisi_id: $('#id_teknisi').val(),
                            kode_mesin: $('#kode_mesin_updt').val(),
                            task: $('#id_notif').val(),
                            realisasi_teknisi_id: $('#realisasi_id_teknisi').val()
                        }).done(function(data){
                            swal("Success!", "your mechine be approved", "success");
                            deleteNotif(id);
                        })
                    });
            setTimeout(function(){
                window.location.href = 'main/mesin_list.php';
            }, 1500);
        } else {
            swal("Cancelled", "your mechine cancelled approve", "error"); 
        }
     });
    
}

function modify(){
$('#provinsi, #kota, #kecamatan, #desa, #nama_alat, #tipe, #device_ip, #capacity, #ct_primer, #ct_sekunder, #set_ols, #max_volt, #max_temp, #keterangan, #latitude, #longitude').removeAttr("readonly");    
}

function deleteNotif(id){
    $.ajax({
      type: "POST",
      url: "process/notif_update.php",
      data: "id="+id,
      success: function(msg){
      }
    });
}

function hit_ratio(){
	var pri=$('#ct_primer').val()*1;
    var sek=$('#ct_sekunder').val()*1;
	if(pri!=0 && sek!=0){
        $('#ratio').val(pri/sek);
    }
}

function suggest_desa(src)
{
	var page    = 'main/suggest_desa.php';
	if(src.length>2){
		var loading = '<p align="center">Loading ...</p>';
		showStuff('suggest_desa');
		$('#suggest_desa').html(loading);
		$.ajax({
			url: page,
			data : 'nama_desa='+src,
			type: "post", 
			dataType: "html",
			timeout: 10000,
			success: function(response){
				$('#suggest_desa').html(response);
			}
		});
	}
}

//Fungsi untuk memilih provinsi dan memasukkannya pada input text
function pilih_desa(desa)
{
	$('#desa').val(desa);
}


function suggest_kec(src)
{
	var page    = 'main/suggest_kec.php';
	if(src.length>=2){
		var loading = '<p align="center">Loading ...</p>';
		showStuff('suggest_kec');
		$('#suggest_kec').html(loading);
		$.ajax({
			url: page,
			data : 'nama_kecamatan='+src,
			type: "post", 
			dataType: "html",
			timeout: 10000,
			success: function(response){
				$('#suggest_kec').html(response);
			}
		});
	} 
}

//Fungsi untuk memilih provinsi dan memasukkannya pada input text
function pilih_kecamatan(kecamatan)
{
	$('#kecamatan').val(kecamatan);
}



function suggest(src)
{
	var page    = 'main/suggest.php';
	if(src.length>=3){
		var loading = '<p align="center">Loading ...</p>';
		showStuff('suggest');
		$('#suggest').html(loading);
		$.ajax({
			url: page,
			data : 'nama_kota='+src,
			type: "post", 
			dataType: "html",
			timeout: 10000,
			success: function(response){
				$('#suggest').html(response);
			}
		});
	}
}

//Fungsi untuk memilih kota dan memasukkannya pada input text
function pilih_kota(kota)
{
	$('#kota').val(kota);
}

function suggest_prov(src)
{
	var page    = 'main/suggest_prov.php';
	if(src.length>=2){
		var loading = '<p align="center">Loading ...</p>';
		showStuff('suggest_prov');
		$('#suggest_prov').html(loading);
		$.ajax({
			url: page,
			data : 'nama_provinsi='+src,
			type: "post", 
			dataType: "html",
			timeout: 10000,
			success: function(response){
				$('#suggest_prov').html(response);
			}
		});
	}
}

//Fungsi untuk memilih provinsi dan memasukkannya pada input text
function pilih_provinsi(provinsi)
{
	$('#provinsi').val(provinsi);
}

//menampilkan form div
function showStuff(id) {
	document.getElementById(id).style.display = 'block';
}
//menyembunyikan form
function hideStuff(id) {
	document.getElementById(id).style.display = 'none';
}