startConnect()
function startConnect() {
    clientID = "clientID-" + parseInt(Math.random() * 100);
    host ="das.pt-mgi.co.id";
    port = "8083";
    client = new Paho.MQTT.Client(host, Number(port), clientID);
    // Set callback handlers
    client.onConnectionLost = onConnectionLost;
    client.onMessageArrived = onMessageArrived;
	 var options = {
            useSSL: false,
            cleanSession: true,
            timeout: 3,
            onSuccess: onConnect,
			userName:'das',
			password:'das1234'
         };
    client.connect(options)
}
function onConnect() {
   topic = "das/sensor/suhu";
   subcribe(topic);   
}

function subcribe(topic){	
   client.subscribe(topic);
}

function publish(x){	
	message= new Paho.MQTT.Message("60");
	message.destinationName='das/sensor/suhu/';
    client.send(message);
}

function sendMessage(ia,ib,ic,va,vb,vc){	

	mia = new Paho.MQTT.Message(ia);
    mia.destinationName = "das/sensor/currentR";
	if(ia<=600){
		client.send(mia);
	}
	mib = new Paho.MQTT.Message(ib);
    mib.destinationName = "das/sensor/currentS";
	if(ia<=600){
		client.send(mib);
	}
	mic = new Paho.MQTT.Message(ic);
    mic.destinationName = "das/sensor/currentT";
	if(ia<=600){
		client.send(mic);
	}
	
	mva = new Paho.MQTT.Message(va);
    mva.destinationName = "das/sensor/voltR";
    client.send(mva);
	mvb = new Paho.MQTT.Message(vb);
    mvb.destinationName = "das/sensor/voltS";
    client.send(mvb);
	mvc = new Paho.MQTT.Message(vc);
    mvc.destinationName = "das/sensor/voltT";
    client.send(mvc);
}

function onConnectionLost(responseObject) {
    document.getElementById("messages").innerHTML = 'ERROR: Connection lost';
	console.log(responseObject.errorMessage);
    if (responseObject.errorCode !== 0) {
        document.getElementById("messages").innerHTML =  responseObject.errorMessage ;
    }
}

function onMessageArrived(message) {
	console.log("onMessageArrived: " + message.payloadString);
	document.getElementById("sensor").innerHTML=message.payloadString;
}
function startDisconnect() {
    client.disconnect();
    document.getElementById("messages").innerHTML += '<span>Disconnected</span><br/>';
}