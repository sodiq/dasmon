//localStorage.clear();
var host ="das.pt-mgi.co.id";
var port = "8083";
var user='das';
var pass='das1234';
var topic = "das/sensor/phaseR";
var prefix="das/widget/";	
var datamessage;
var widgetdata=[];
var plotgauge =[];
var plotgaugehttp =[];
var httpchart=[]
var reportchart=[];
var binddata = [];
var tempdatachart=[];
var plotpie=[];
var plotpiehttp=[];
var icon;

$(document).ready(function(){
	readsqlite();
});

try{
ls_save('prefix',prefix);
}
catch (err)
{alert(err.message)}
var kapasitas = 1000;
	window.onload = function(){
	startConnect(host,port,topic);
};
function startConnect(host,port,topic,user='das',pass='das1234') {
    clientID = "clientID-" + parseInt(Math.random() * 100);
     client = new Paho.MQTT.Client(host, Number(port), clientID);
    client.onConnectionLost = onConnectionLost;
    client.onMessageArrived = onMessageArrived;
	 var options = {
            useSSL: false,
            cleanSession: true,
			timeout: 3,
            onSuccess: subcribe,
			userName:user,
			password:pass
         };
    client.connect(options)
}

function subcribe(){
	var used;
   var prefix= ls_read('prefix');   
   client.subscribe(prefix+'#');
   

}
function publish(data,topic){	
	var prefix= ls_read('prefix');
	message= new Paho.MQTT.Message(data);
	message.destinationName=prefix+topic;
    client.send(message);
}
function onConnectionLost(responseObject) {
	// console.log(responseObject.errorMessage);
    if (responseObject.errorCode !== 0) {
       // alert("MQTT is disconnected");
       startConnect(host,port,topic);
    } 
}
function onMessageArrived(message) {
	var topic=message.destinationName;
	var prefix= ls_read('prefix');
	// 	console.log(message.payloadString)
	//  if(topic==prefix+'notif/read'){
	//  	var msg=message.payloadString;
	// 		document.getElementById("status").innerHTML=msg;
	//  	if(msg=='0'){
	//  		$('.nodot').removeClass('notification-dot');
	//  		document.getElementById("notif-body").innerHTML='0';
	//  	}else{
	//  		$('.nodot').addClass('notification-dot');
	//  	}
	//  }
	// if(topic==prefix+'notifs'){
	// 	publish('1','notif/read');	
	// 	var temp_notif='<li>\
	// 						<a href="javascript:void(0);">\
 // 							<div class="media">\
	//  								<div class="media-left">\
	//  									<i class="icon-info text-warning t-icon"></i>\
	//  								</div>\
	//  								<div class="media-body" onClick="publish(\'0\',\'notif/read\');">\
	//  									<p class=\"text\" id=\"notif\"></p>\
	//  									<span class=\"timestamp\" id=\"jam\"></span>\
	//  								</div>\
	//  							</div>\
	//  						</a>\
	//  					</li>';	
	//  	document.getElementById("notif-body").innerHTML=temp_notif;
	// 	document.getElementById("notif").innerHTML=message.payloadString;
	//  	document.getElementById("jam").innerHTML=getFormattedDate();
	//  }
	// $.ajax({
	// 	type: "POST",
	// 	url: "process/notif_push.php",
	// 	success: function(response){
	// 	$('#notif-body').html(response);
	// 	var jml = $('#total_notif').val();
	// 	if(jml==0){
	// 		$('#notif-dot').hide();
	// 	}else{
	// 		$('#notif-dot').show();
	// 	}
	// 	$('#jml_notif').html(jml);
	// 	$('#status_notif').html(jml);
	// 	}
	// });
	try{
		var destination =topic.split('/')
		if(destination[3]=='phaseR' ||destination[3]=='phaseS' || destination[3]=='phaseT' || destination[3]=='tempR' ||destination[3]=='tempS' || destination[3]=='tempT' || destination[3]=='voltR' ||destination[3]=='voltS' || destination[3]=='voltT' || destination[3]=='freqR' ||destination[3]=='freqS' || destination[3]=='freqT' || destination[3]=='tempAvg' || destination[3]=='freqAvg' )
		{
        var jenis ;

		$.ajax({
		type: "POST",
		url: "module/webapi/update_report.php",
		data: {kode: destination[2], jenis: destination[3], arus: message.payloadString, volt: message.payloadString,temp: message.payloadString },
		success: function (response) {
            //alert(response);
        }
	    });

		datajson = document.getElementById("setting_gui").innerHTML;
		$.each(JSON.parse(datajson), function(idx, obj) {
			if (obj.protocol=="MQTT"){
			var parsetopic = obj.topic.split('/')
			var kodetrafo = ls_read('kode-trafo');
			var trafodestination =prefix+kodetrafo+'/'+parsetopic[3];
			var type = obj.topic.split('/')
			//alert(topic)
			if (obj.kd_widget =='1' && trafodestination ==  topic){
				// console.log(obj.kd_widget)
				// widgetdata[idx].refresh(message.payloadString)
				// $('#txt'+obj.design_id).html(message.payloadString+obj.unit); 
				var nilai = 0;
				if(destination[3]=='phaseR'){
					nilai = (message.payloadString * obj.ratio).toFixed(2);
				}else if(destination[3]=='phaseS'){
					nilai = (message.payloadString * obj.ratio).toFixed(2);
				}else if(destination[3]=='phaseT'){
					nilai = (message.payloadString * obj.ratio).toFixed(2);
				}else{
					nilai = message.payloadString;
				}
				widgetdata[idx].refresh(nilai);
				$('#txt'+obj.design_id).html(nilai+obj.unit); 
			}
			if (obj.kd_widget =='2' ){	
				datartc(message.payloadString,topic)
			}
			if (obj.kd_widget =='3'){
				datartc(message.payloadString,topic)
				// updpie(obj.topic,idx,obj.unit,obj.ratio)			
			}
			}
			});	
			}
		}
	catch(err){		
	}
}

function getFormattedDate() {
    var date = new Date();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();
    month = (month < 10 ? "0" : "") + month;
    day = (day < 10 ? "0" : "") + day;
    hour = (hour < 10 ? "0" : "") + hour;
    min = (min < 10 ? "0" : "") + min;
    sec = (sec < 10 ? "0" : "") + sec;
    var str = day+'-'+month+'-'+date.getFullYear()+' '+ hour + ":" + min + ":" + sec;
    return str;
}

function startDisconnect() {
    client.disconnect();
}

function ls_save(index,data) {		
		return localStorage.setItem(index,data);
} 

function ls_read(index) {		
	return localStorage.getItem(index);
} 
function upddatawg(){
	$.get( "view.php" )
    .done(function( data ) {
				document.getElementById("setting_gui").innerHTML = data;
});}

function readsqlite(){
var kode_trafo = ls_read('kode_trafo');
$.get( "view.php", {kode_mesin: kode_trafo})
    .done(function( data ) {
				document.getElementById("setting_gui").innerHTML = data;
				$.each(JSON.parse(data), function(idx, obj) {
					var id = "design"+obj.design_id;
					var  name = obj.panel_name;	
					binddata.push([]);
					widgetdata.push('');
					plotpie.push('');
					try{
						if (obj.kd_widget=="1"||obj.kd_widget=="2"||obj.kd_widget=="3"||obj.kd_widget=="4"){
							var parsetopic = obj.topic.split('/');
							if (parsetopic[3].match(/phase/) == 'phase'){
								icon ='icon icon-energy text-danger';
								}
							if (parsetopic[3].match(/freq/) == 'freq'){
								icon ='icon icon-energy text-danger';
								}
							else if (parsetopic[3].match(/volt/) == 'volt'){
									icon ='icon icon-energy text-primary';
								}
							else if (parsetopic[3].match(/temp/) == 'temp'){
									icon ='fa fa-thermometer-half text-success';
								}
						}
					}
					catch(err)
					{alert(err.message)
					}
					
				if(obj.protocol=="HTTP" && obj.kd_widget=="1"){		
					addwggauge(obj.design_id,obj.topic,obj.panel_name,icon,obj.unit,idx,obj.kd_widget)
				}
				else if(obj.protocol=="HTTP" && obj.kd_widget=="2"){		
					addwgreportcharthttp(obj.design_id,obj.panel_name,idx,obj.kd_widget);
				}
				else if(obj.protocol=="HTTP" && obj.kd_widget=="3"){		
					addwgpiehttp(obj.design_id,obj.panel_name,idx,obj.kd_widget);
				}
				else if (obj.protocol=="MQTT" && obj.kd_widget=="1"){
						addwggauge(obj.design_id,obj.topic,obj.panel_name,icon,obj.unit,idx,obj.kd_widget)
					}
				else if(obj.protocol=="MQTT" && obj.kd_widget=="2"){
						addwgreportchart(obj.design_id,obj.panel_name,idx,obj.kd_widget);
				} 
				else if(obj.protocol=="MQTT" && obj.kd_widget=="3"){
						icon ="fa fa-thermometer-half text-danger";
						addwgpie(obj.design_id,obj.topic,obj.panel_name,icon)
					} 
				else if(obj.kd_widget=="4"){
						icon ="fa fa-thermometer-half text-danger";
						try{
						addwgmap(obj.design_id,obj.topic,obj.panel_name,icon)
						}
						catch(err)
						{
							alert(err.message)
						}
					} 
					else if(obj.kd_widget=="5"){
						icon ="fa fa-thermometer-half text-danger";
						try{
						 addwgchartsum(obj.design_id,topic,obj.panel_name,icon);
						}
						catch(err)
						{
							alert(err.message)
						}
					}
					else if(obj.kd_widget=="6"){
						icon ="fa fa-thermometer-half text-danger";
						try{
						 addwgtbsum(obj.design_id,topic,obj.panel_name,icon,obj.kode_mesin);
						}
						catch(err)
						{
							alert(err.message)
						}
					}
					else if(obj.kd_widget=="7"){
						icon ="fa fa-thermometer-half text-danger";
						try{
							addwgbar(obj.design_id,topic,obj.panel_name,icon);
						}
						catch(err)
						{
							alert(err.message)
						}
					}
					else if(obj.kd_widget=="1a"){
						try{
							addwgOilLevel(obj.design_id, obj.topic, obj.panel_name);
						}
						catch(err)
						{
							alert(err.message)
						}
					}
					else{
						icon ="icon icon-energy text-danger ";
					} 
				});	
			
			try{
				google.charts.load('current', {'packages':['corechart']});
				google.charts.setOnLoadCallback(renderpie) 
				renderSummarychart();
				renderChart();
				realtimechart();
				rendergauge();
				renderCharthttp();
				rendertbsum();
				rendermap();
				renderBar();
				renderOilLevel();
				$('#loading').hide()
				webapi();
				}
				catch(err){
					alert(err.messages)
			}
			;
			
		});
}


function delwidget(x,y,kd) {
	$("."+x).addClass("d-none");
	$("."+x).remove;
	
 	 $.get( "delete.php", { submit_data: 1, des_id: x } )
				.done(function( data ) {
					toastr.options.timeOut = "false";
		            toastr.options.closeButton = true;
		            toastr.options.positionClass = 'toast-top-center';
		            toastr['info']('Widget successfully deleted!');
		    
		            $('.btn-toastr').on('click', function() {
		                $context = $(this).data('context');
		                $message = $(this).data('message');
		                $position = $(this).data('position');
		    
		                if ($context === '') {
		                    $context = 'info';
		                }
		    
		                if ($position === '') {
		                    $positionClass = 'toast-top-center';
		                } else {
		                    $positionClass = 'toast-' + $position;
		                }
		    
		                toastr.remove();
		                toastr[$context]($message, '', {
		                    positionClass: $positionClass
		                });
		            });	
						});  
 }
function widget_checked(index,data){
	var wg_pr= ls_read('wg_pr')
	if (wg_pr.match(data) ){
		 toastr.options.timeOut = "false";
             toastr.options.closeButton = true;
             toastr.options.positionClass = 'toast-top-center';
             toastr['info']('Widget already exist!');
	}else{
	ls_save(index,data)	
	$('.widget-checked').removeClass('fa fa-check-square-o text-success');
	var addwidget=ls_read(index);	
	switch(addwidget){
		case "1":return $('.widget-checked-wg1').addClass('fa fa-check-square-o text-success'); 
		case "2":return $('.widget-checked-wg2').addClass('fa fa-check-square-o text-success');
		case "3":return $('.widget-checked-wg3').addClass('fa fa-check-square-o text-success'); 
		case "4":return $('.widget-checked-wg4').addClass('fa fa-check-square-o text-success'); 
		case "5":return $('.widget-checked-wg5').addClass('fa fa-check-square-o text-success'); 
		case "6":return $('.widget-checked-wg6').addClass('fa fa-check-square-o text-success'); 
		case "7":return $('.widget-checked-wg7').addClass('fa fa-check-square-o text-success'); 
		case "1a":return $('.widget-checked-wg8').addClass('fa fa-check-square-o text-success');
	}	}
}
function readtime(){
			var today = new Date();
			var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
			var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			var dateTime = date+' '+time;
			return dateTime	
				}

function  datartc(data,kode){ 
		ls_save(kode,data);
};

function readsumarychart(){
	showgraph()
	function showgraph(){
		var datajson = document.getElementById("setting_gui").innerHTML
		$.each(JSON.parse(datajson), function(number,objjson) {
		if(objjson.kd_widget=="5")
		{
		$.get("module/webapi/showreport.php").done(function(data){
			var Showdata=[];
			$.each(JSON.parse(data),function(idx,obj){
				var current = parseInt(obj.current) || 0;
				var volt = parseInt(obj.volt)|| 0;
				var temp = parseInt(obj.temp) || 0;
				var freq = parseInt(obj.freq) || 0;
				var time = obj.tanggal;
				$('#txtchrt1'+objjson.design_id).html(' <span  ><i class="fa fa-bolt text-danger"></i ">Current: '+current+""+'</span>')
				$('#txtchrt2'+objjson.design_id).html(' <span ><i class="fa fa-bolt  text-primary"></i>Volt: '+volt+""+'</span>')
				$('#txtchrt3'+objjson.design_id).html(' <span  ><i class="fa fa-thermometer-half text-success"></i>Temp: '+temp+""+'</span>')
				$('#txtchrt4'+objjson.design_id).html(' <span  ><i class="fa fa-bolt text-primary"></i>Freq: '+freq+""+'</span>')
				showdata={period:time,Current:current,Voltage:volt,Temperature:temp };
				Showdata.push(showdata)		
			});	
			reportchart[number].setData(Showdata)
		});	
		}}
	);	
	}
var loopinterval = setInterval(function (){showgraph()},10000,true)
}	
function realtimechart(){
 
  setInterval(function(){
	datajson = document.getElementById("setting_gui").innerHTML
	$.each(JSON.parse(datajson), function(idx, obj) {
		
			if ( obj.kd_widget =='2'&& obj.protocol=='MQTT' ){
				
			var dttopic = obj.topic
			var parsetopic=obj.topic.split('/')
			binddata[idx].push({period:readtime(),
				CurrentR:(ls_read(dttopic+'R') * obj.ratio).toFixed(2),
				CurrentS:(ls_read(dttopic+'S') * obj.ratio).toFixed(2),
				CurrentT:(ls_read(dttopic+'T') * obj.ratio).toFixed(2)})			
				var icon;
				if (parsetopic[3].match(/temp/) == 'temp'){
					icon ='fa fa-thermometer-half';
					}
				else {
					icon ='fa fa-bolt';
					}			
				var R = ((parseFloat(ls_read(dttopic+'R')) || 0)* obj.ratio ).toFixed(2);
				var S = ((parseFloat(ls_read(dttopic+'S')) || 0)* obj.ratio ).toFixed(2);
				var T = ((parseFloat(ls_read(dttopic+'T')) || 0)* obj.ratio ).toFixed(2);
				$('#txtchrt1'+obj.design_id).html(' <span class="text" ><i class="icon-loop m-r-5 text-danger"></i>'+R +obj.unit+'</span>')
				$('#txtchrt2'+obj.design_id).html(' <span class="text" ><i class="icon-loop m-r-5 text-warning"></i>'+S+obj.unit+'</span>')
				$('#txtchrt3'+obj.design_id).html(' <span class="text " ><i class="icon-loop m-r-5 text-primary"></i>'+T+obj.unit+'</span>')
				if (binddata[idx].length >20){
					binddata[idx].shift(0);
				}
				widgetdata[idx].setData(binddata[idx])
				}
				});}, 1000);	 
}
function updpiehttp(R,S,T){
		try{
				var name = topic.split('/')
				var data = google.visualization.arrayToDataTable([
						['Power Usage', 'Realtime'],
						  [' R' , R ],
						  [' S', S ],
						  [' T', T ]
				]);
				plotpie[0].draw(data, optionspie);
		}
		catch(err){
	}
}
function updpie(topic,position,unit,ratio){
		try{
				var name = topic.split('/')
				var R =  (parseFloat(ls_read(topic+'R')) ||0) * ratio
				var S =  (parseFloat(ls_read(topic+'S')) ||0) * ratio
				var T =  (parseFloat(ls_read(topic+'T')) ||0) * ratio
				var data = google.visualization.arrayToDataTable([
						['Power Usage', 'Realtime'],
						  [name[3]+ ' R' , R ],
						  [name[3]+ ' S', S ],
						  [name[3]+ ' T', T ],
						  ['Available',kapasitas-( R+S+T)]
				]);
				plotpie[position].draw(data, optionspie);
		}
		catch(err){
		}
}		
function selecticon(x){
						var parsetopic = x.split('/');
							if (parsetopic[3].match(/phase/) == 'phase'){
								icon ='icon icon-energy text-danger';
								}
							if (parsetopic[3].match(/freq/) == 'freq'){
								icon ='icon icon-energy text-danger';
								}
							else if (parsetopic[3].match(/volt/) == 'volt'){
									icon ='icon icon-energy text-primary';
								}
							else if (parsetopic[3].match(/temp/) == 'temp'){
									icon ='fa fa-thermometer-half text-success';
								}
								
	return icon;
}		
loophttp()
function loophttp(){
	setInterval(function(){webapi()
	}, 3000);
}

function webapi(){
	datajson = document.getElementById("setting_gui").innerHTML
	$.each(JSON.parse(datajson), function(idx1, obj) {
		 if (obj.kd_widget=='2' && obj.protocol=='HTTP'){
				var i =0;
				var url= obj.url+"?code="+obj.code+"&phase="+obj.phase+"&getdata="+obj.getdata
					$.get(url)
					.done(function( data ) {		
							$.each(JSON.parse(data), function(idx2, objson) {
							var datajson =JSON.parse(JSON.stringify(objson.phaseR));
								currents =parseInt(datajson.current) ;
								volts =parseInt(datajson.volt) ;
								freqs =parseInt(datajson.freq) ;
								temps =parseInt(datajson.temp) ;
								tempdatachart.push({period:readtime(),current:currents,volt:volts,freq:freqs,temp:temps})
								if (tempdatachart.length >20){
									tempdatachart.shift(0);
								}
								ls_save('wghttpchart'+obj.design_id,JSON.stringify(tempdatachart))
								$('#txtchrt1'+obj.design_id).html(' <span  ><i class="fa fa-bolt text-danger"></i ">Current: '+datajson.current+""+'</span>')
								$('#txtchrt2'+obj.design_id).html(' <span ><i class="fa fa-bolt  text-primary"></i>Volt: '+datajson.volt+""+'</span>')
								$('#txtchrt3'+obj.design_id).html(' <span  ><i class="fa fa-bolt text-warning"></i>Freq: '+datajson.temp+""+'</span>')
								$('#txtchrt4'+obj.design_id).html(' <span  ><i class="fa fa-thermometer-half text-success"></i>Temp: '+datajson.freq+""+'</span>')
							});
							if (tempdatachart.length >20){
								tempdatachart.shift(0);
								}
								httpchart[i].setData( tempdatachart)				
							i++
						});  
				}
		else if (obj.kd_widget=='1'&& obj.protocol =='HTTP'){
					var url= obj.url+"?code="+obj.code+"&phase="+obj.phase+"&getdata="+obj.getdata
					$.get(url)
					.done(function( data ) {
					widgetdata[idx1].refresh(data)
					$('#txt'+obj.design_id).html(data); 
					})
			}
		else if (obj.kd_widget=='3'&& obj.protocol =='HTTP'){
			var i =0;
					var url= obj.url+"?code="+obj.code+"&phase="+obj.phase+"&getdata="+obj.getdata
					$.get(url)					
				.done(function( data ) {		
							$.each(JSON.parse(data), function(idx2, objson) {
							var datajsonR =JSON.parse(JSON.stringify(objson.phaseR));
							var datajsonS =JSON.parse(JSON.stringify(objson.phaseS));
							var datajsonT =JSON.parse(JSON.stringify(objson.phaseT));
							IR =parseInt(datajsonR.current);
							VR =parseInt(datajsonR.volt);
							var R = IR*VR/1000;
							IS =parseInt(datajsonS.current);
							VS =parseInt(datajsonS.volt);
							var S = IS*VS/1000;
							IT =parseInt(datajsonT.current);
							VT =parseInt(datajsonT.volt);
							var T = IT*VT/1000;
							updpiehttp(R,S,T)
							});					
							i++
						});  		
	
		
		}
		
		
		
			
	});
	
} 
	
	
	


