<?php
error_reporting(0);
header('Access-Control-Allow-Origin: *'); 
include 'koneksi.php';

$code = $_GET['code'];
$phase = $_GET['phase'];
$current = $_GET['current'];
$volt = $_GET['volt'];
$temp =$_GET['temp'];
$getdata = $_GET['getdata'];

if($phase=='all'){
		$query_allphase = "SELECT 
		(ia.arus*m_mesin.ratio) AS currentR,
		(ib.arus*m_mesin.ratio) AS currentS,
		(ic.arus*m_mesin.ratio) AS currentT,
		va.volt AS voltR,
		vb.volt AS voltS,
		vc.volt AS voltT,
		freqa.freq AS freqR,
		freqb.freq AS freqS,
		freqc.freq AS freqT,
		ta.temp AS tempR,
		tb.temp AS tempS,
		tc.temp AS tempT
		FROM ia
		LEFT JOIN ib ON ia.kode = ib.kode
		LEFT JOIN ic ON ic.kode = ic.kode
		LEFT JOIN m_mesin ON ia.kode = m_mesin.kode_mesin
		LEFT JOIN va ON ia.kode = va.kode
		LEFT JOIN vb ON ia.kode = vb.kode
		LEFT JOIN vc ON ia.kode = vc.kode  
		LEFT JOIN freqa ON ia.kode = freqa.kode
		LEFT JOIN freqb ON ia.kode = freqb.kode
		LEFT JOIN freqc ON ia.kode = freqc.kode
		LEFT JOIN ta ON ia.kode = ta.kode
		LEFT JOIN tb ON ia.kode = tb.kode
		LEFT JOIN tc ON ia.kode = tc.kode
		WHERE
			ia.kode='".$code."' 
		ORDER BY ia.tanggal DESC, ib.tanggal DESC, ic.tanggal DESC, 
		va.tanggal DESC, vb.tanggal DESC, vc.tanggal DESC,
		freqa.tanggal DESC, freqb.tanggal DESC, freqc.tanggal DESC,
		ta.tanggal DESC, tb.tanggal DESC, tc.tanggal DESC
		LIMIT 1";

		$result = mysqli_query($connection,$query_allphase);
		$array_data = array();
		while($baris = mysqli_fetch_assoc($result))
		{
		  $array_data[]=$baris;
		}

		$data_all=json_encode($array_data);

		$hasil_all=json_decode($data_all, true);

		// print_r($hasil_all);

		$json='[
		{
			"phaseR":
				{
				"current": '. $hasil_all[0]['currentR'].',
				"volt": '.$hasil_all[0]['voltR'].',
				"freq": '.$hasil_all[0]['freqR'].',
				"temp": '.$hasil_all[0]['tempR'].'
			},
			"phaseS":
			{
				"current":'.$hasil_all[0]['currentS'].',
				"volt": '.$hasil_all[0]['voltS'].',
				"freq": '.$hasil_all[0]['freqS'].',
				"temp": '.$hasil_all[0]['tempS'].'
			},
			"phaseT":
			{
				"current":'.$hasil_all[0]['currentT'].',
				"volt": '.$hasil_all[0]['voltT'].',
				"freq":'.$hasil_all[0]['freqT'].',
				"temp":'.$hasil_all[0]['tempT'].'
			}
		}
	    ]';

echo $json;

}elseif ($phase=='R') {
    $query = "SELECT 
	round((ia.arus*m_mesin.ratio),2) AS current, 
	va.volt AS volt,
	freqa.freq,
	ta.temp
	FROM ia
	LEFT JOIN m_mesin ON ia.kode = m_mesin.kode_mesin
	LEFT JOIN va ON ia.kode = va.kode  
	LEFT JOIN freqa ON ia.kode = freqa.kode
	LEFT JOIN ta ON ia.kode = ta.kode
	WHERE
		ia.kode='".$code."' 
	ORDER BY ia.tanggal 
	DESC, va.tanggal DESC, freqa.tanggal DESC, ta.tanggal DESC
	LIMIT 1";

	$result = mysqli_query($connection,$query);
	$array_data = array();
	while($baris = mysqli_fetch_assoc($result))
	{
	  $array_data[]=$baris;
	}
	$data_phaseR= json_encode($array_data);
	$hasil_phaseR= json_decode($data_phaseR, true);

	

    if($getdata=='current'){
    	echo $hasil_phaseR[0]['current'];
    }elseif($getdata=='volt'){
    	echo $hasil_phaseR[0]['volt'];
    }elseif ($getdata=='freq'){
    	echo $hasil_phaseR[0]['freq'];
    }elseif ($getdata=='temp'){
    	echo $hasil_phaseR[0]['temp'];
    }else{
    	$json='[
		{
			"phaseR":
			 {
				"current": '. $hasil_phaseR[0]['current'].',
				"volt": '.$hasil_phaseR[0]['volt'].',
				"freq": '.$hasil_phaseR[0]['freq'].',
				"temp": '.$hasil_phaseR[0]['temp'].'
		     }
		}
	    ]';
    echo $json;
    }


}elseif ($phase=='S') {
	$query_phaseS = "SELECT 
	(ib.arus*m_mesin.ratio) AS current, 
	vb.volt AS volt,
	freqb.freq,
	tb.temp
	FROM ib
	LEFT JOIN m_mesin ON ib.kode = m_mesin.kode_mesin
	LEFT JOIN vb ON ib.kode = vb.kode  
	LEFT JOIN freqb ON ib.kode = freqb.kode
	LEFT JOIN tb ON ib.kode = tb.kode
	WHERE
		ib.kode='".$code."' 
	ORDER BY ib.tanggal 
	DESC, vb.tanggal DESC, freqb.tanggal DESC, tb.tanggal DESC
	LIMIT 1";

	$result = mysqli_query($connection,$query_phaseS);
	$array_data = array();
	while($baris = mysqli_fetch_assoc($result))
	{
	  $array_data[]=$baris;
	}
	
	$data_phaseS= json_encode($array_data);
	$hasil_phaseS= json_decode($data_phaseS, true);

    if($getdata=='current'){
    	echo $hasil_phaseS[0]['current'];
    }elseif($getdata=='volt'){
    	echo $hasil_phaseS[0]['volt'];
    }elseif ($getdata=='freq'){
    	echo $hasil_phaseS[0]['freq'];
    }elseif ($getdata=='temp'){
    	echo $hasil_phaseS[0]['temp'];
    }else{
    	$json='[
		{
			"phaseS":
			{
				"current": '. $hasil_phaseS[0]['current'].',
				"volt":  '.$hasil_phaseS[0]['volt'].', 
				"freq": '.$hasil_phaseS[0]['freq'].', 
				"temp": '.$hasil_phaseS[0]['temp'].'
		    }
		}
	    ]';
    echo $json;
    }

}elseif ($phase=='T'){
  $query_phaseT = "SELECT 
	(ic.arus*m_mesin.ratio) AS current, 
	vc.volt AS volt,
	freqc.freq,
	tc.temp
	FROM ic
	LEFT JOIN m_mesin ON ic.kode = m_mesin.kode_mesin
	LEFT JOIN vc ON ic.kode = vc.kode  
	LEFT JOIN freqc ON ic.kode = freqc.kode
	LEFT JOIN tc ON ic.kode = tc.kode
	WHERE
		ic.kode='".$code."' 
	ORDER BY ic.tanggal 
	DESC, vc.tanggal DESC, freqc.tanggal DESC, tc.tanggal DESC
	LIMIT 1";

	$result = mysqli_query($connection,$query_phaseT);
	$array_data = array();
	while($baris = mysqli_fetch_assoc($result))
	{
	  $array_data[]=$baris;
	}
	
	$data_phaseT= json_encode($array_data);
	$hasil_phaseT= json_decode($data_phaseT, true);

    if($getdata=='current'){
    	echo $hasil_phaseT[0]['current'];
    }elseif($getdata=='volt'){
    	echo $hasil_phaseT[0]['volt'];
    }elseif ($getdata=='freq'){
    	echo $hasil_phaseT[0]['freq'];
    }elseif ($getdata=='temp'){
    	echo $hasil_phaseT[0]['temp'];
    }else{
    	$json='[
		{ 
			"phaseT":
			{
				"current": '. $hasil_phaseT[0]['current'].', 
				"volt": '.$hasil_phaseT[0]['volt'].', 
				"freq": '.$hasil_phaseT[0]['freq'].', 
				"temp": '.$hasil_phaseT[0]['temp'].'
		    }
		}
	    ]';
    echo $json;
    }

}else{
	echo "API Format Not Found";
}
?>