<!DOCTYPE html>
<html>

<?php
include '../session.php';
include '../class/class.select.php';
$select=new select;
$userID=$_SESSION['userSession'];
//$max_kode=$select->zf(($select->max_kode_m_mesin()+1),3);
//$max_kode_gi=$select->zf(($select->max_kode_mgi()+1),3);
//$max_kode_unit=$select->zf(($select->max_kode_ols_unit($max_kode)+1),2);

if(isset($_COOKIE['addols'])){
	$add_ols=$_COOKIE['addols'];
}else{
	$add_ols='f';
} 

if(!empty($_REQUEST['kode'])){
	$kode=$_REQUEST['kode'];
}else{
	$kode='0';
}
$qry=$select->det_mesin($kode);
$row=mysql_fetch_array($qry);
$prov = $row['provinsi_name'];
$kota = $row['kota_name'];
$kec = $row['kecamatan_name'];
$desa = $row['desa_name'];
$place = $prov.', '.$kota.', '.$kec.', '.$desa; 
?>
<body>


    <table class="table table-striped" width="30px">
            <tr>
                <th scope="row">Kode Mesin</th>
                 <td><?php echo $row['kode_mesin'];?></td>
                <th scope="row">Trafo Name</th>
                 <td><?php echo $row['trafo_name']; ?></td>
            </tr>
            <tr>
                <th scope="row">Device Type</th>
                  <td><?php echo $row['trafo_name'];?></td>
                 <th scope="row">IP</th>
                  <td><?php echo $row['ip'];?></td>
            </tr>
            <tr>
                <th scope="row">Description</th>
                  <td><?php echo $row['keterangan'];?></td>
                <th scope="row">Serial Number</th>
                  <td><?php echo $row['serial_number'];?></td>
            </tr>
            <tr>
                <th scope="row">Location</th>
                  <td><?php echo $place;?></td>
                <th scope="row">Capacity</th>
                  <td><?php echo $row['kapasitas'];?></td>
            </tr>
            <tr>
                <th scope="row">CT Primer</th>
                  <td><?php echo $row['ct_primer'];?></td>
                 <th scope="row">CT Secondary</th>
                  <td><?php echo $row['ct_sekunder'];?></td>
            </tr>
            <tr>
                <th scope="row">Ratio</th>
                  <td><?php echo $row['ratio'];?></td>
                 <th scope="row">OLS</th>
                  <td><?php echo $row['set_ols'];?></td>
            </tr>
            <tr>
                <th scope="row">Max Volt</th>
                  <td><?php echo $row['max_volt'];?></td>
                <th scope="row">Max Temp</th>
                  <td><?php echo $row['max_temp'];?></td>
            </tr>
            <tr>
                <th scope="row">Latitude</th>
                  <td><?php echo $row['latitude'];?></td>
                <th scope="row">Longitude</th>
                  <td><?php echo $row['longitude'];?></td>
            </tr>
    </table>

</body>
</html>