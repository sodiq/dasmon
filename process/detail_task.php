<?php

include '../session.php';
include '../class/class.select.php';
$select=new select;
$userID=$_SESSION['userSession'];
$area = $select->user($userID, 'area');

 if($area == 'PUSAT'){
     $area_login = '%';
 }else{
    $area_login = $area;
 }

if(isset($_GET['id'])){
    $id = $_GET['id'];
}else{
    $id = null;
}
?>

<div class="table-responsive">
<table id="table_detail" class="table">
    <thead>
        <tr>
			<td><b>Date</b></td>
            <td><b>Code</b></td>
            <td><b>Name</b></td>
            <td><b>Type</b></td>
            <td><b>Address</b></td>
            <td><b>Status</b></td>
        </tr>
    </thead>
    <tbody>
<?php 
$result = $select->get_detail_list_task($id, $area_login);
while($row = mysql_fetch_array($result)){ ?>
            <tr>	
				<td><?php echo date('Y-m-d h:i:sa', strtotime($row['tanggal'])) ?></td>
                <td><?php echo $row['kode_mesin']?></td>
                <td><?php echo $row['nama']?></td>
                <td><?php echo $row['jenis_mesin']?></td>
                <td><?php echo $row['provinsi_name'].', '.$row['kota_name']?></td>
                <td><?php echo ($row['active'] == 'N' && $row['flag_reg'] == 'Y' ? '<span class="badge badge-warning">on progress</span>' : 
				($row['active'] == 'N' && $row['flag_reg'] == 'N' ? '<span class="badge badge-primary">waiting for activation</span>' : 
				($row['active'] == 'Y' && $row['flag_reg'] == 'N' ? '<span class="badge badge-success">complete</span>' : 
				'<span class="badge badge-warning">null</span>')))
				?> </td>
            </tr>
<?php } ?>
        </tbody>
    </table>
</div>

