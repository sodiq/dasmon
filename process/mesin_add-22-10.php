<!DOCTYPE html>
<html>

<?php
include '../session.php';
include '../class/class.select.php';
$select=new select;
$userID=$_SESSION['userSession'];
$max_kode=$select->zf(($select->max_kode_m_mesin()+1),3);
$max_kode_gi=$select->zf(($select->max_kode_mgi()+1),3);
//$max_kode_unit=$select->zf(($select->max_kode_ols_unit($max_kode)+1),2);

if(isset($_COOKIE['addols'])){
	$add_ols=$_COOKIE['addols'];
}else{
	$add_ols='f'; 
} 

if(!empty($_REQUEST['kode'])){
	$kode=$_REQUEST['kode'];
}else{
	$kode='0';
}
$qry=$select->det_mesin($kode);
$row=mysql_fetch_array($qry);
$prov = $row['provinsi_name'];
$kota = $row['kota_name'];
$kec = $row['kecamatan_name'];
$desa = $row['desa_name'];
$place = strtr($prov, ' ','_').'/'.strtr($kota, ' ','_').'/'.strtr($kec, ' ','_').'/'.strtr($desa, ' ','_'); 
?>
<body>

<form role="form" method="POST" id="bd-form">
	
	<input type="hidden" name="kode_mesin" value="<?php echo $row['kode_mesin'];?>" id="kode_mesin" class="form-control input-sm" placeholder="Kode Mesin">

		<div class="row">
		
			<div class="col-md-4">
				<div class="input-group">
				<span class="input-group-addon" id="">Select Province </span>
				<div class="row">
				<input class="form-control" style="margin-left: 15px;" id="nama_provinsi" name="nama_provinsi" type="text" onkeypress="suggest_prov(this.value);"  value="<?php echo $prov; ?>"><div id="suggest_prov"></div>
			     </div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="input-group">
				<span class="input-group-addon" id="">Select City </span>
				<div class="row">
				<input class="form-control" style="margin-left: 15px;" id="nama_kota" name="nama_kota" type="text" 
				onkeypress="suggest(this.value);" value="<?php echo $kota; ?>"><div id="suggest"></div>
			     </div>
				</div>
			</div> 
		
			<div class="col-md-4">
				<div class="input-group">
				<span class="input-group-addon" id="">Select District </span>
				<div class="row">
				<input class="form-control" style="margin-left: 15px;" id="nama_kecamatan" name="nama_kecamatan" type="text" onkeypress="suggest_kec(this.value);" value="<?php echo $kec; ?>"><div id="suggest_kec"></div>
			     </div>
				</div>
			</div>


			</div> <!-- tutup row -->
		<br/>

				<div class="row">
				<div class="col-md-4">
				<div class="input-group">
				<span class="input-group-addon" id="" style="margin-top: -10px;">Select Village </span>
				<div class="row">
				<input class="form-control" style="margin-left: 15px;" id="nama_desa" name="nama_desa" type="text" onkeypress="suggest_desa(this.value);"  value="<?php echo $desa; ?>"><div id="suggest_desa"></div>
			     </div>
				</div>
			</div> <br/>
		
		


	<div class="input-group col-md-4 form-dev" style="margin-top: 10px; height: 20px;">
		<div class="input-group-prepend">
			<span class="input-group-text" id="">Device Name</span>
		</div>
			<input type="text" name="nama_mesin" id="nama_mesin" class="form-control" value="<?php echo $row['nama'];?>">
	</div> 


	<div class="input-group col-md-4 form-dev" style="margin-top: 10px; height: 20px;">
		<div class="input-group-prepend">
			<span class="input-group-text" id="">Device Type</span>
		</div>
			<input type="text" name="jenis_mesin" id="jenis_mesin" class="form-control" value="<?php echo $row['jenis_mesin'];?>">
	</div> 


	</div><br/>	 <!--tutup row -->

	<div class="row">
	<div class="input-group col-md-4 form-dev">
		<div class="input-group-prepend">
			<span class="input-group-text" id="">Device IP </span>
		</div>
			<input type="text" name="ip" id="ip" class="form-control" value="<?php echo $row['ip'];?>">
	</div>


	<div class="input-group col-md-4 form-dev" style="height: 20px;">
		<div class="input-group-prepend">
			<span class="input-group-text" id="">Capacity </span>
		</div>
			<input type="text" name="kapasitas" id="kapasitas" class="form-control" value="<?php echo $row['kapasitas'];?>">
		<div class="input-group-prepend">
			<span class="input-group-text"><select><option>A</option><option>mA</option></select></span>
		</div>
	</div>
	
  

	<div class="input-group col-md-4 form-dev" style="height: 20px;">
		<div class="input-group-prepend">
			<span class="input-group-text" id="">CT Primer </span>
		</div>
			<input onchange="hit_ratio()" type="text" name="ct_primer" id="ct_primer" class="form-control" value="<?php echo $row['ct_primer'];?>">
		<div class="input-group-prepend">
			<span class="input-group-text">A</span>
		</div>
	</div>
	

	 </div><br/><br/><!-- tutup row -->


	<div class="row">	
	<div class="input-group col-md-4 form-dev" style="margin-top: -10px;">
		<div class="input-group-prepend">
			<span class="input-group-text" id="">CT Sekunder</span>
		</div>
			<input onchange="hit_ratio()" type="text" name="ct_sekunder" id="ct_sekunder" class="form-control" value="<?php echo $row['ct_sekunder'];?>">
		<div class="input-group-prepend">
			<span class="input-group-text">A</span>
		</div>
	</div>
	

	<div class="input-group col-md-4 form-dev" style="margin-top: -10px;">
		<div class="input-group-prepend">
			<span class="input-group-text" id="">Ratio </span>
		</div>
			<input type="text" name="ratio" id="ratio" class="form-control" value="<?php echo $row['ratio'];?>">
	</div>


	

	<div class="input-group col-md-4 form-dev" style="margin-top: -10px;">
		<div class="input-group-prepend">
			<span class="input-group-text" id="">Set OLS </span>
		</div>
			<input onchange="hit_ratio()" type="text" name="set_ols" id="set_ols" class="form-control" value="<?php echo $row['set_ols'];?>">
	</div>

    </div><br/><br/>


    <div class="row">

	<div class="input-group col-md-4 form-dev" style="margin-top: -10px;">
		<div class="input-group-prepend">
			<span class="input-group-text" id="">Max Volt </span>
		</div>
			<input type="text" name="max_volt" id="max_volt" class="form-control" value="<?php echo $row['max_volt'];?>">
	</div>

    

    <div class="input-group col-md-4 form-dev" style="margin-top: -10px;">
		<div class="input-group-prepend">
			<span class="input-group-text" id="">Max Temp </span>
		</div>
			<input type="text" name="max_temp" id="max_temp" class="form-control" value="<?php echo $row['max_temp'];?>">
	</div>


	<div class="input-group col-md-4 form-dev" style="margin-top: -10px;">
		<div class="input-group-prepend">
			<span class="input-group-text" id="">Keterangan </span>
		</div>
			<input type="text" name="keterangan" id="keterangan" class="form-control" value="<?php echo $row['keterangan'];?>">
	</div>

	   </div><br/><br/><!--tutup row -->

  <div class="row">
  <div class="col-md-4 form-dev" style="margin-top: -10px;">
			<label>Active</label>
			<div class="row" style="padding-left: 15px;">
				<input type="radio" onClick="" id="mesin_on" name="active" <?php if($row['active']=='Y'){echo 'checked';}?> value="Y">Y 
				<input style="margin-left: 20px;" type="radio" onClick="" id="mesin_off" name="active" <?php if($row['active']=='N'){echo 'checked';}?> value="N">N
			</div>
	</div>
	

	<div class="input-group col-md-4 form-dev" style="height: 20px; margin-top: -10px;" >
		<div class="input-group-prepend">
			<span class="input-group-text" id="">Latitude </span>
		</div>
			<input type="text" name="latitude" id="latitude" class="form-control" value="<?php echo $row['latitude'];?>">
	</div>

<div class="input-group col-md-4 form-dev" style="height: 20px; margin-top: -10px;">
		<div class="input-group-prepend">
			<span class="input-group-text" id="">Longitude </span>
		</div>
			<input type="text" name="longitude" id="longitude" class="form-control" value="<?php echo $row['longitude'];?>">
</div>


</div><br/>		
</form>

<script type="text/javascript">
function hit_ratio(){
	var pri=$('#ct_primer').val()*1;
	var sek=$('#ct_sekunder').val()*1;

	if(pri!='0' && sek!='0'){
		$('#ratio').val(pri/sek);
		$('#set_ols').val(pri*0.8);
	}
}
</script>

  </body>
</html>