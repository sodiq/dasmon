<!DOCTYPE html>
<head>
<title>Realtime Electricity</title>
<meta charset="utf-8">

<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.min.css">


<!-- MAIN CSS -->
<link rel="stylesheet" href="assets/css/main.css">
</head>
<body>
<!---------------------------------------- MAP --------------------------------------------->

 <?php
    include '../class/koneksi.php';
    $data = mysql_query ("
        SELECT ia.kode AS kodeia, ia.tanggal, ia.arus AS arusia,
               ib.kode AS kodeib, ib.arus AS arusib,
               ic.kode AS kodeic, ic.arus AS arusic,
               va.kode AS kodeva, va.volt AS vola,
               vb.kode AS kodevb, vb.volt AS volb,
               vc.kode AS kodevc, vc.volt AS volc,
               ta.kode AS kodeta, ta.temp AS tempa,
               tb.kode AS kodetb, tb.temp AS tempb,
               tc.kode AS kodetc, tc.temp AS tempc, 
               m_mesin.kode_mesin, m_mesin.latitude, m_mesin.longitude, m_mesin.nama, m_mesin.ratio 
        FROM ia 
        LEFT JOIN m_mesin
        ON ia.kode = m_mesin.kode_mesin
        LEFT JOIN ib
        ON ib.kode = m_mesin.kode_mesin
        LEFT JOIN ic
        ON ic.kode = m_mesin.kode_mesin
        LEFT JOIN va
        ON va.kode = m_mesin.kode_mesin
        LEFT JOIN vb
        ON vb.kode = m_mesin.kode_mesin
        LEFT JOIN vc
        ON vc.kode = m_mesin.kode_mesin
        LEFT JOIN ta
        ON ta.kode = m_mesin.kode_mesin
        LEFT JOIN tb
        ON tb.kode = m_mesin.kode_mesin            
        LEFT JOIN tc
        ON tc.kode = m_mesin.kode_mesin

    ");
    $array_data = array();
    while ($row = mysql_fetch_assoc($data))
    {
        $array_data[] = $row;
    }
    $data_lokasi = json_encode($array_data);
    ?>

    <title></title>
    <meta charset="utf-8" />
    <script>
    var hs = '<?php echo ($data_lokasi); ?>';
    var data = JSON.parse(hs);
    console.log(data);
    var infobox;

    function GetMap() {
        // alert(data);
        var map = new Microsoft.Maps.Map('#myMap', {
            credentials: 'AklReKlg6U-7ie54NEcZ3yAuP4WzeZfeMjDWf_jR4ARCbJZXE_bCubuInqzP0E-Q',
        });

        map.setView({
            mapTypeId: Microsoft.Maps.MapTypeId.canvasDark,
            center: new Microsoft.Maps.Location(-7.005145, 110.438126),
            zoom: 11
        });

        infobox = new Microsoft.Maps.Infobox(map.getCenter(), {
            visible: false
        });

        infobox.setMap(map);

        // var Location = Microsoft.Maps.getLocations();

        for (var i = data.length - 1; i >= 0; i--) {
             var location = new Microsoft.Maps.Location(parseFloat(data[i].latitude), parseFloat(data[i].longitude))
            // var location = new Microsoft.Maps.Location(-7.127540,110.404480)
            var pin = new Microsoft.Maps.Pushpin(location,{
                icon: '../assets/images/trf_green2.png',
                anchor: new Microsoft.Maps.Point(13,15)
            });

            pin.metadata = {
                title: data[i].nama,
                description: 
                '<table>'+
                '<tr>'+
                '<td>'+'IR '+ '<br>' + (data[i].arusia*data[i].ratio) + 'A' + '</td>'+ 
                '<td>'+'IS '+ '<br>' + (data[i].arusib*data[i].ratio) + 'A' + '</td>' +
                '<td>'+'IT '+ '<br>' + (data[i].arusic*data[i].ratio) + 'A' + '</td>' +
                    
                '<td>'+'RN '+ '<br>' + data[i].vola + 'V' +  '</td>'+ 
                '<td>'+'SN '+ '<br>' + data[i].volb + 'V' + '</td>'+ 
                '<td>'+'TN '+ '<br>' + data[i].volc + 'V'+ '</td>'+
                '<tr/>'+ 

                '<tr>'+
                '<td>'+'TA '+'<br>' + data[i].tempa + 'C'  + '</td>'+ 
                '<td>'+'TB '+ '<br>' + data[i].tempb + 'C' + '</td>'+ 
                '<td>'+'TC '+ '<br>' + data[i].tempc + 'C' + '</td>'+ 

                '<td>'+'FR '+ '<br>' + data[i].vola  +  '</td>'+ 
                '<td>'+'FS '+ '<br>' + data[i].volb  + '</td>'+ 
                '<td>'+'FT '+ '<br>' + data[i].volc + '</td>'+
                '<tr/>'+
                '</table>'+
                '<hr>'
               
                // icon: 'trf_green2.png',
                // anchor: new Microsoft.Maps.Point(12, 39),
            };

             Microsoft.Maps.Events.addHandler(pin, 'click', pushpinClicked);

            map.entities.push(pin);
        }
    }

    function pushpinClicked(e) {
        //Make sure the infobox has metadata to display.
        if (e.target.metadata) {
            //Set the infobox options with the metadata of the pushpin.
            infobox.setOptions({
                location: e.target.getLocation(),
                title: e.target.metadata.title,
                description: e.target.metadata.description,
                visible: true
            });
        }
    }

    </script>
    <script type='text/javascript' src='http://www.bing.com/api/maps/mapcontrol?callback=GetMap' async defer></script>
    <div id="myMap" style="position:relative;width:600px;height:300px;"></div>

<br/> 

<?php include "realtime.php"  ?>

</body>

<!-- Javascript -->
<script src="assets/bundles/libscripts.bundle.js"></script>    
<script src="assets/bundles/vendorscripts.bundle.js"></script>
<script src="assets/bundles/flotscripts.bundle.js"></script> <!-- flot charts Plugin Js --> 
<script src="assets/js/pages/charts/el_realtime.js"></script> 
</html>