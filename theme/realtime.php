<div class="row clearfix">
                <div class="col-lg-6 col-md-6">
                    <div class="card">
                        <div class="header">
                            <h2>Realtime Electricity</h2>
                            <div class="float-right">
                                <div class="switch panel-switch-btn"> <span class="m-r-10 font-12">REAL TIME</span>
                                    <label>OFF
                                        <input type="checkbox" id="realtime" checked>
                                        <span class="lever switch-col-cyan"></span>ON</label>
                                </div>
                            </div>                        
                        </div>
                        <div class="body">
                            <div id="real_time_chart" class="flot-chart"></div>
                        </div>
                    </div>
                </div>      
</div>

<!-- Javascript -->
<!-- <script src="assets/js/pages/charts/el_realtime.js"></script> -->