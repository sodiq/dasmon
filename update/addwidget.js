function addwgmap(design_id,topic,panel_name,icon){
	document.getElementById("widgetdata").innerHTML+='<div id = wg'+design_id+' class="row  col-lg-12 clearfix">\
														<div class="col-md-12 covarea">\
															<div class="card visitors-map">\
																<div class="header p-0">\
																	<ul class="header-dropdown">\
																		<li class="dropdown">\
																			<a href="javascript:void(0);" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">\
																			<i class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i>\
																			</a>\
																			<ul class="dropdown-menu pull-top bg-dark">\
																				<li><a href="javascript:void(0);" data-type="success" data-toggle="modal" data-target="#editwidget"><i class="icon icon-note text-warning"></i> Modify</a></li>\
																				<li><a href="javascript:$(.covarea).addClass(d-none);"><i class="icon icon-trash text-danger"></i> Remove</a></li>\
																			</ul>\
																		</li>\
																	</ul>\
																</div>\
																<div class="body">\
																	<div class="text-left"><h5><i class="icon icon-map text-primary"></i> Coverage Area</h5></div>\
																	<div class="row">\
																		<div class="col-lg-12 col-md-12">\
																			 <div id="'+design_id+'" style="width:100%; height:320px;"><?php include "/main/maps.php" ?></div>\
																		</div>\
																		</div>\
																</div>\
															</div>\
														</div>\
													</div>';
}

function addwgpie(design_id,topic,panel_name,icon){
					document.getElementById("widgetdata").innerHTML+='<div id = wg'+  design_id+' class=" col-lg-6 realelect">\
																		<div class="card">\
																			<div class="header p-0">\
																				<ul class="header-dropdown ">\
																						<li class="dropdown">\
																						   <a href="javascript:void(0);" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">\
																						<i class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i>\
																						</a>\
																						<ul class="dropdown-menu pull-top bg-dark">\
																							<li><a href="javascript:void(0);" data-type="success" data-toggle="modal" data-target="#editwidget" onclick="modify_widget("'+design_id+'")"><i class="icon icon-note text-warning"></i> Modify</a></li>\
																							<li><a href="javascript:delwidget('+design_id+');" ><i class="icon icon-trash text-danger"></i> Remove</li></a>\
																						</ul>\
																					</li>\
																				</ul>\
																			</div>	\
																			<div class="body gender-overview">\
																				 <div class="text-left"><h5><i class="icon icon-pie-chart text-primary"></i>'+panel_name+'</h5></div>\
																					<div id="'+design_id+'" class="flot-chart" style="margin-top: -20px; height: 300px;"></div>\
																					<p hidden>'+topic+'</p>\
																				</div>\
																			</div>\
																		</div>\
																	</div>';
}
function addwggauge(design_id,topic,panel_name,icon,unit){
					document.getElementById("widgetdata").innerHTML+='\<div id = wg'+  design_id+' class="col-lg-4 col-md-6 '+design_id+' body team_list">\
																		<div class="card info-box-2 dd" data-plugin="nestable">\
																			<div class="header p-0">\
																				<ul class="header-dropdown m-r--5">\
																					<li class="dropdown"> <a href="javascript:void(0);" class="" data-toggle="dropdown"\
																							role="button" aria-haspopup="true" aria-expanded="false"> <i\
																								class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i> </a>\
																						<ul class="dropdown-menu pull-top bg-dark">\
																							<li><a href="javascript:void(0);" data-type="success" data-toggle="modal" data-target="#editwidget" onclick="modify_widget('+design_id+')"><i class="icon icon-note text-warning"></i> Modify</a></li>\
																							<li><a href="javascript:delwidget('+design_id+');" ><i class="icon icon-trash text-danger"></i> Remove</li></a>\
																						</ul>\
																					</li>\
																				</ul>\
																			</div>	\
																			<div class="body">\
																				<div class="text-center dd"><h5><i class="'+icon+'"></i> '+panel_name+'</h5></div>\
																				<div class="icon">\
																					<div id="'+design_id+'" class="" style="width:100px; height:55px;"></div>\
																					<p hidden>'+topic+'</p>\
																				</div>\
																				<div class="content">\
																					<div class="text"> Average Value</div>\
																					<div id ="txt'+design_id+'" class="number"><span >0</span>'+unit+'</div>\
																				</div>\
																			</div>\
																		</div>\
																	</div>';
}
function addwgreportchart(design_id,name){
				document.getElementById("widgetdata").innerHTML+='<div id = wg'+  design_id+' class=" col-lg-6 realelect">\
                    <div class="card ">\
                        <div class="header p-0">\
                            <ul class="header-dropdown">\
                                <li class="dropdown">\
                                    <a href="javascript:void(0);" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">\
									<i class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i>\
									</a>\
                                    <ul class="dropdown-menu pull-top bg-dark">\
                                        <li><a href="javascript:void(0);" data-type="success" data-toggle="modal" data-target="#editwidget"><i class="icon icon-note text-warning"></i> Modify</a></li>\
                                     	<li><a href="javascript:delwidget('+design_id+');" ><i class="icon icon-trash text-danger"></i> Remove</li></a>\
										</li>\
										</ul>\
										</li>\
										</ul></div>\
                        <div class="body gender-overview">\
						<div class="text-left"><h5><i class="icon icon-map text-primary"></i>'+name+'</h5></div>\
                            <h5>\
                                <span class="m-r-50"  id ="txtchrt1'+design_id+'"><i class="icon icon-bar-chart m-r-10 text-danger"></i>12</span>\
                                <span class="m-r-50" id ="txtchrt2'+design_id+'" ><i class="fa fa-bolt m-r-10 text-primary"></i>121</span>\
                                <span class="m-r-50" id ="txtchrt3'+design_id+'" ><i class="fa fa-bolt m-r-10 text-primary"></i>121</span>\
								<div id= "wgchart'+design_id+'" style="height: 250px;">\
							</div>\
                        </div>\
                    </div>\
                </div>\
            </div>';
}
function addwgrealtimechart(design_id){
	document.getElementById("widgetdata").innerHTML+='<div class="row clearfix col-lg-12 col-md-12" style="width:500px">\
                <div class="col-lg-20 realelect '+design_id+' ">\
                    <div class="card">\
                        <div class="header p-0">\
                            <div class="float-right">\
                                <div class="switch panel-switch-btn"> <span class="m-r-10 font-12">REAL TIME</span>\
                                    <label>OFF\
                                        <input type="checkbox" id="realtime'+design_id+'" checked>\
                                        <span class="lever switch-col-cyan"></span>ON</label>\
                                </div>\
                            </div>   \
							<ul class="header-dropdown">\
                                <li class="dropdown">\
                                    <a href="javascript:void(0);" class="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">\
									<i class="zmdi zmdi-more-vert fa fa-ellipsis-v"></i>\
									</a>\
                                    <ul class="dropdown-menu pull-top bg-dark">\
                                        <li><a href="javascript:void(0);" data-type="success" data-toggle="modal" data-target="#editwidget"><i class="icon icon-note text-warning"></i> Modify</a></li>\
                                     	<li><a href="javascript:delwidget('+design_id+');" ><i class="icon icon-trash text-danger"></i> Remove</li></a>\
										</li>\
										</ul>\
										</li>\
										</ul></div>\
                        <div class="body gender-overview">\
						<div class="text-left"><h5><i class="icon icon-map text-primary"></i>Sumarry Report Electricity (AVG)</h5></div>\
                            <h5>\
                                <span class="m-r-50"><i class="icon icon-line-chart m-r-10 text-danger"></i> 2,21,598</span>\
                             </h5>\
							<div id= "rt'+design_id+'" class="flot-chart">\
							</div>\
                        </div>\
                    </div>\
                </div>\
            </div>';
}	
function renderChart() {
		datajson = document.getElementById("setting_gui").innerHTML	
		$.each(JSON.parse(datajson), function(idx, obj) 
		{if (obj.kd_widget=="2"){
			label  = obj.topic.split('/');
			reportchart[idx]= Morris.Area({
													element:"wgchart"+obj.design_id,
													behaveLikeLine: true,
													xkey: 'period',
													data:[{period:0, 
															CurrentR:0,
															CurrentS:0,
															CurrentT:0}
													],
													ykeys: ['CurrentR', 'CurrentS', 'CurrentT'],
													ymax:parseInt(obj.payload_max),
													labels: [label[3]+' R', label[3]+' S', label[3]+' T'],
													pointSize: 2,
													smoth:true,
													fillOpacity: 0.5,
													pointStrokeColors: ['#f20a0a', '#0f44f2', '#0cf523'],
													behaveLikeLine: false,
													gridLineColor: '#27303e',
													lineWidth: 0,
													hideHover: 'auto',
													lineColors: ['#f20a0a', '#0f44f2', '#0cf523'],
													parseTime: false
												
												});
		}
	});
	
		realtimechart();
}
function rendergauge(){	
	var x = 0 ;
	datajson = document.getElementById("setting_gui").innerHTML
		$.each(JSON.parse(datajson), function(idx, obj) {	
		if (obj.kd_widget=="1"){
		plotgauge[idx]	= new JustGage(
							{
								id: obj.design_id,
								value: 0,
								valueFontColor: '#5E6773',
								valueFontFamily: 'Roboto, sans-serif',
								valueMinFontSize: 12,
								symbol: 'A',
								min: parseInt(obj.payload_min),
								max: parseInt(obj.payload_max),
								minTxt: obj.payload_min,
								maxTxt: obj.payload_max,
								hideValue:true,
								donut:false,
								label: '',
								labelFontColor: '#A0AEBA',
								labelMinFontSize: 12,
								counter: true,
								pointer: true,
								pointerOptions:
								{
									color: '#5E6773'	
								}	
			});
		}});	
}
function renderpie(){
		datajson = document.getElementById("setting_gui").innerHTML
			$.each(JSON.parse(datajson), function(idx, obj) {	
			if(obj.kd_widget=="3"){
			
		
			var data = google.visualization.arrayToDataTable([
							['Power Usage', 'Realtime'],
							  ['IR', 0],
							  ['IS', 0],
							   ['IT', 0],
							  ['Available', 100]
							]);
			optionspie = {
							  width: 500, height: 300,
							  backgroundColor: 'transparent',
							  legend: {textStyle: {color: 'white'}},
							  colors: ['red','#ffba00', 'blue', 'green']
							};								
			plotpie[idx] = new google.visualization.PieChart($('#'+obj.design_id)[0]);
			plotpie[idx].draw(data, optionspie);
			}
			});
			
}				